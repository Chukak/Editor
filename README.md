# Editor
Simple text editor, with syntax highlighting, run script and run SQL queries. Created with Python 3.5.2.

## Documentation
[Русский](https://github.com/Chukak/Editor/blob/master/readme_ru.md)

## Getting Started
### Installation 
Download this project into folder with command. You needs to install git.

#### On Linux
```
apt-get install git
git clone https://github.com/Chukak/Editor.git
```

### Requirements
You needs to install python 3.3.2 or latest version and pip 9.0.1. Run command in project folder.
```
pip install -r requirements.txt
```

### Start project
```
python window.py
```

## Test
At present the tests are not complete. Run the tests if you want.

## Built with
[PyQt5.9](http://doc.qt.io/qt-5.9/index.html) 

[SqlAlchemy1.1.15](https://pypi.python.org/pypi/SQLAlchemy/1.1.15) and [docs](http://docs.sqlalchemy.org/en/latest/) - Sql queries

[pandas](https://pypi.org/project/pandas/0.21.0/#files) - Used for DataFrame only

## Autors
[chukak](https://github.com/Chukak)
