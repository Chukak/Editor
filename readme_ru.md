# Editor
Простой текстовый редактор с подсветкой кода, запуском скриптов и запуской запросов SQL. Создан с помощью Python 3.5.2

## Начало
### Установка 
Загрузите этот проект в папку с помощью команды. Вам необходимо установить git.

#### Linux
```
apt-get install git
git clone https://github.com/Chukak/Editor.git
```
### Требования
Вам необходимо установить python 3.3.2 или позднюю версию и pip 9.0.1. Запустите команду в папке с проектом.
```
pip install -r requirements.txt
```
### Запуск проекта
```
python window.py
```
## Тесты
В настоящий момент тесты не закончены. Запустите тесты по своему желанию.

## Библиотеки
[PyQt5.9](http://doc.qt.io/qt-5.9/index.html) 

[SqlAlchemy1.1.15](https://pypi.python.org/pypi/SQLAlchemy/1.1.15) and [docs](http://docs.sqlalchemy.org/en/latest/) - Sql queries

[pandas](https://pypi.org/project/pandas/0.21.0/#files) - Used for DataFrame only

## Авторы
[chukak](https://github.com/Chukak)
