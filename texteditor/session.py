from configparser import ConfigParser
from PyQt5.QtCore import QRect
#from PyQt5.QtGui import QFont, QColor, QTextBlockFormat, QTextCharFormat
from extraoption.default.runners import RUNNERS

from envs.environments import STYLES_ENVS, ENVIRONMENT
from envs.options import format_text

class Session:
    def __init__(self, obj):
        super().__init__()
        self.object = obj
        
    def get_session(self):
        config = ConfigParser()
        files = []
        try:
            config.read('settings.ini', encoding='utf-8')
            if config.has_section('USERSETTINGS'):
                key = 'USERSETTINGS'
            else:
                key = 'DEFAULT'
            session = config.get(key, 'currentsession')
            self.object.option['font'] = config.get(key, 'font')
            self.object.option['session'] = int(config.get(key, 'session'))
            self.object.option['tabulation'] = int(config.get(key, 'tabulation'))
            self.object.option['saverunners'] = int(config.get(key, 'saverunners'))
            self.object.default_environ = config.get(key, 'defaultenv')
            for path in session.split(','):
                if path:
                    try:
                        with open(path, 'r', encoding='utf-8') as f:
                            data = f.read()
                            self.object.file_data[path] = data
                            files.append(path)
                        self.object.path_file.append(path)
                    except FileNotFoundError:
                        print('error')
                        continue
            for f in files:
                if config.has_option('FILES', f):
                    self.object.environs[f] = config.get('FILES', f)
                else:
                    #print(f)
                    self.object.environs[f] = 'none'
            proj = config.get('PROJECTS', 'opens')
            for p in proj.split(','):
                if p:
                    self.object.projects.append(str(p))
            #check = self.get_environments()
            if not self.get_environments(): 
                raise FileNotFoundError
            if not self.get_runners_settings(): 
                raise FileNotFoundError
            if self.object.option['saverunners']:
                if not self.get_runners_file_path():
                    raise FileNotFoundError
            return 1
        except FileNotFoundError:
            return 0
        
    def save_session(self, save=False):
        self.save_display_settings()
        config = ConfigParser()
        config.optionxform = str
        string = ''
        proj = ''
        try:
            config.read('settings.ini', encoding='utf-8')
            if not config.has_section('USERSETTINGS'):
                config['USERSETTINGS'] = {}
            config['USERSETTINGS']['font'] = self.object.option['font']
            config['USERSETTINGS']['session'] = str(self.object.option['session'])
            config['USERSETTINGS']['defaultenv'] = str(self.object.default_environ)
            config['USERSETTINGS']['tabulation'] = str(self.object.option['tabulation'])
            config['USERSETTINGS']['saverunners'] = str(self.object.option['saverunners'])
            if not save:
                config['USERSETTINGS']['currentsession'] =  ''
            elif save:
                config['FILES'].clear()
                for f in self.object.path_file:
                    string += (f + ',') 
                if len(string) >0:
                    config['USERSETTINGS']['currentsession'] =  string[:-1]
                for f, run in self.object.environs.items():
                    if run != 'none':
                        config['FILES'][str(f)] = run 
                for p in self.object.projects:
                    proj += (p + ',')
                if len(proj) >0:
                    config['PROJECTS']['opens'] = proj[:-1] 
            with open('settings.ini', 'w', encoding='utf-8') as f:
                config.write(f)
            if not self.save_environments():
                raise FileNotFoundError
            if not self.save_runners_settings():
                raise FileNotFoundError
            if self.object.option['saverunners']:
                if not self.save_runners_file_path():
                    raise FileNotFoundError
            else:
                if not self.save_runners_file_path(attr='clear'):
                    raise FileNotFoundError
            return 1
        except FileNotFoundError:
            return 0
            
    def get_environments(self):
        config = ConfigParser()  
        try:
            config.read('environments.ini', encoding='utf-8')
            for name, dict_ in STYLES_ENVS.items():
                if not dict_ is None:
                    if config.has_section(name):
                        for word, format in config[name].items():
                            color, font = format.split(';')
                            bold = True if 'bold' in font else False
                            italic = True if 'italic' in font else False
                            format = format_text(color, bold, italic)
                            STYLES_ENVS[name][word] = format
            return 1
        except FileNotFoundError:
            return 0
              
    def save_environments(self):
        config = ConfigParser()
        try:
            config.read('environments.ini', encoding='utf-8')
            for name, dict_ in STYLES_ENVS.items():
                if not dict_ is None:
                    if not config.has_section(name):
                        config[name] = {}
                    for word, format in dict_.items():
                        config[name][word] = str(format.foreground().color().name()) + ';'
                        font, f = format.font(), ''
                        if font.bold():
                            f += 'bold,'
                        if font.italic():
                            f += 'italic,'
                        config[name][word] += f
            with open('environments.ini', 'w', encoding='utf-8') as f:
                config.write(f)
            return 1        
        except FileNotFoundError:
            return 0
    
    def get_display_settings(self):
        config = ConfigParser()
        try:
            config.read('settings.ini', encoding='utf-8')
            if config.has_section('DISPLAY'):
                x = int(config['DISPLAY']['x'])
                y = int(config['DISPLAY']['y'])
                width = int(config['DISPLAY']['width'])
                height = int(config['DISPLAY']['height'])
                return QRect(x, y, width, height)
            else:
                return False
        except FileNotFoundError:
            pass
        
    def save_display_settings(self):
        config = ConfigParser()
        rect = self.object.geometry()
        x,y, width, height = rect.x(), rect.y(), rect.width(), rect.height()
        try:
            config.read('settings.ini', encoding='utf-8')
            if not config.has_section('DISPLAY'):
                config['DISPLAY'] = {}
            config['DISPLAY']['x'] = str(x)
            config['DISPLAY']['y'] = str(y)
            config['DISPLAY']['width'] = str(width)
            config['DISPLAY']['height'] = str(height)
            with open('settings.ini', 'w', encoding='utf-8') as f:
                config.write(f)
        except FileNotFoundError:
            pass 
    
    def save_runners_settings(self):
        config = ConfigParser()
        try:
            config.read('runners.ini', encoding='utf-8')
            if not config.has_section('RUNNERS'):
                config['RUNNERS'] = {}
            for key in RUNNERS:
                runners = ''
                for l in RUNNERS[key]:
                    runners += str(l) + ';'
                print(runners)
                config['RUNNERS'][key] = runners
                with open('runners.ini', 'w', encoding='utf-8') as f:
                    config.write(f)
            return 1
        except FileNotFoundError:
            return 0
            
    def get_runners_settings(self):
        config = ConfigParser()
        try:
            config.read('runners.ini', encoding='utf-8')
            if config.has_section('RUNNERS'):
                for key in config['RUNNERS']:
                    all = []
                    for i in config['RUNNERS'][key].split(';'):
                        if i:
                            all.append(str(i))
                    RUNNERS[key] = all
            return 1
        except FileNotFoundError:
            return 0
    
    def save_runners_file_path(self, attr=None):
        config = ConfigParser()
        if attr == 'clear':
            try:
                config.read('runners.ini', encoding='utf-8')
                if not config.has_section('FILES_WITH_RUNNERS'):
                    config['FILES_WITH_RUNNERS'] = {}
                config['FILES_WITH_RUNNERS'].clear()
                with open('runners.ini', 'w', encoding='utf-8') as f:
                    config.write(f) 
                return 1
            except FileNotFoundError:
                return 0
                
        try:
            config.read('runners.ini', encoding='utf-8')
            if not config.has_section('FILES_WITH_RUNNERS'):
                config['FILES_WITH_RUNNERS'] = {}
            config['FILES_WITH_RUNNERS'].clear()
            for index in range(self.object.tabs.count()):
                widget = self.object.tabs.widget(index)
                attr = getattr(widget, 'code_runner', False)
                if attr:
                    path_runner = attr.path
                    path_file = self.object.file_in_tabs[index]
                    config['FILES_WITH_RUNNERS'][path_file] = path_runner
            with open('runners.ini', 'w', encoding='utf-8') as f:
                    config.write(f)   
            return 1
        except FileNotFoundError:
            return 0
    
    def get_runners_file_path(self):
        config = ConfigParser()
        try:
            config.read('runners.ini', encoding='utf-8')
            for path, runner in config['FILES_WITH_RUNNERS'].items():
                self.object.file_run[str(path)] = str(runner)
            return 1
        except FileNotFoundError:
            return 0
        
    
