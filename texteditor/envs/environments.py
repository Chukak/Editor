from envs.syntax.highlighters.python import PythonHighlighter
from envs.syntax.highlighters.cpp import CppHighlighter
from envs.syntax.highlighters.html import HtmlHighlighter
from envs.syntax.highlighters.css import CssHighlighter
from envs.syntax.highlighters.javascript import JavaScriptHighlighter
from envs.syntax.highlighters.mysql import MysqlHighlighter, MysqlALLHighlighter
from envs.syntax.highlighters.sql2003 import Sql2003Highlighter
from envs.syntax.styles import python, cpp, html, javascript, css, mysql, sql2003

from collections import OrderedDict


ENVIRONMENT = OrderedDict(sorted({
    'none' : None,
    'python3' : PythonHighlighter,
    'cpp': CppHighlighter,
    'mysql': MysqlHighlighter,
    'mysqlALL': MysqlALLHighlighter,
    'sql2003': Sql2003Highlighter,
    'html5': HtmlHighlighter,
    'css3': CssHighlighter,
    'javascript': JavaScriptHighlighter,
    }.items(), key=lambda x: x[0]))
##########
ENVIRONMENT.move_to_end('none', last=False)


ENVIRONMENT_VALUES = {
    'none': 'Стандартный документ',
    'python3': 'Python 3',
    'cpp': 'C++ 11v (Неполный)',
    'html5': 'HTML 5',
    'css3': 'CSS 3',
    'javascript': 'JavaScript ECMAScript 5/6',
    'mysql': 'MySQL v5 (only sql:2003 keywords)',
    'mysqlALL': 'MySQL v5 (ALL KEYWORDS) (low performance)',
    'sql2003': 'SQL 2003',
    }

STYLES_ENVS = {
    'none': None, 
    'python3': python.CUSTOM_STYLES,
    'cpp': cpp.CUSTOM_STYLES,
    'html5': html.CUSTOM_STYLES,
    'css3': css.CUSTOM_STYLES,
    'javascript': javascript.CUSTOM_STYLES,
    'mysql': mysql.CUSTOM_STYLES,
    'mysqlALL': mysql.CUSTOM_STYLES,
    'sql2003': sql2003.CUSTOM_STYLES,
    }

DEFAULT_STYLES_ENVS = {
    'none': None,
    'python3': python.DEFAULT_STYLES,
    'cpp': cpp.DEFAULT_STYLES,
    'html5': html.DEFAULT_STYLES,
    'css3': css.DEFAULT_STYLES,
    'javascript': javascript.DEFAULT_STYLES,
    'mysql': mysql.DEFAULT_STYLES,
    'mysqlALL': mysql.DEFAULT_STYLES,
    'sql2003': sql2003.DEFAULT_STYLES,
    }

STYLES_VALUES_ENVS = {
    'none': None,
    'python3': python.STYLES_VALUES,
    'cpp': cpp.STYLES_VALUES,
    'html5': html.STYLES_VALUES,
    'css3': css.STYLES_VALUES,
    'javascript': javascript.STYLES_VALUES,
    'mysql': mysql.STYLES_VALUES,
    'mysqlALL': mysql.STYLES_VALUES,
    'sql2003': sql2003.STYLES_VALUES,
    }

STATUS_ENVIRONMENT = {
    'none': 'Нет среды разработки',
    'python3': 'Python3',
    'cpp': 'C++11',
    'html5': 'HTML5',
    'css3': 'CSS3',
    'javascript': 'JavaScript ECMAS 5/6',
    'mysql': 'MySQL',
    'mysqlALL': 'MySQL v5 (ALL KEYWORDS)',
    'sql2003': 'SQL standart 2003',
    }

