from envs.options import format_text



STYLES_VALUES = {
    'keyword' : 'Keywords',
    'operator': 'Operators(=,+,/,etc.)',
    'braces': '()',
    'number': 'Numbers',
    'comment': 'Comment',
    'string': 'Double and single strings',
    'docstring': 'Multi-line comment',
    }

CUSTOM_STYLES = {
    }

DEFAULT_STYLES = {
    'keyword': format_text('#0000ff', bold=True),
    'operator': format_text('#ff0000'),
    'braces': format_text('#919191'),
    'number': format_text('#000000'),
    'string': format_text('#008f00'),
    'comment': format_text('darkgray', bold=True),
    'docstring': format_text('#008d67', bold=True),
    }