
from envs.options import format_text



STYLES_VALUES = {
    'keyword' : 'Keywords',
    'operator': 'Operators(=,+,not,etc.)',
    'braces': '() , [] , {}',
    'preprocessor': 'Pre-processor',
    'comment': 'Comments',
    'this': 'this',
    'number': 'Numbers',
    'builtins': 'Built-In functions',
    'string': 'Double and single strings',
    'docstring': 'Multiline strings',
    }


CUSTOM_STYLES = {
    }

DEFAULT_STYLES = {
    'keyword': format_text('#ff0000'),
    'preprocessor': format_text('#7a7a00', bold=True),
    'operator': format_text('#0000ff', bold=True),
    'braces': format_text('#919191'),
    'comment': format_text('darkgray', bold=True),
    'this': format_text('#b10085', bold=True, italic=True),
    'number': format_text('#0b0b0b'),
    'builtins': format_text('#55007f'),
    'string': format_text('#00aa00'),
    'docstring': format_text('#ff0000', bold=True),
    }

