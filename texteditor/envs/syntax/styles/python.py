from envs.options import format_text



STYLES_VALUES = {
    'keyword' : 'Keywords',
    'psevdokeyword': 'None, True, False',
    'operator': 'Operators(=,+,not,etc.)',
    'braces': '() , [] , {}',
    'fcname': 'Function name, class name',
    'defclass': 'def, class',
    'comment': 'Comments',
    'self': 'self',
    'decorator': 'Decorators',
    'number': 'Numbers',
    'builtins': 'Built-In functions',
    'regex': 'Regular expressions',
    'bytes': "Bytes b' '",
    'unicode': "Unicode u' '",
    'string': 'Double and single strings',
    'docstring': 'Multiline strings',
    'exception': 'Error and Exceptions',
    }

CUSTOM_STYLES = {
    }

DEFAULT_STYLES = {
    'keyword': format_text('#0000ff', bold=True),
    'psevdokeyword': format_text('#5100f5', italic=True),
    'operator': format_text('#ff0000'),
    'braces': format_text('#919191'),
    'defclass': format_text('#0000ff', bold=True),
    'fcname': format_text('#0f0f0f', bold=True),
    'comment': format_text('darkgray', bold=True),
    'self': format_text('#910000', bold=True, italic=True),
    'number': format_text('#000000'),
    'decorator': format_text('#555500', italic=True),
    'builtins': format_text('#7b00b8', bold=True),
    'regex': format_text('#008f00', italic=True),
    'bytes': format_text('#787800', italic=True),
    'unicode': format_text('#4343ca', italic=True),
    'string': format_text('#008f00'),
    'docstring': format_text('#008d67', bold=True),
    'exception': format_text('#0000ff'),
    }