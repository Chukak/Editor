from envs.options import format_text



STYLES_VALUES = {
    'keyword' : 'Keywords',
    'attributes' : 'Attributes (class, id, rel, src, etc.)',
    'braces': '{}',
    'comment': 'Comments',
    'string': 'Double and single strings',
    'operator': '=,+,-'
    }

CUSTOM_STYLES = {
    }

DEFAULT_STYLES = {
    'keyword': format_text('#55aaff', bold=True),
    'attributes': format_text('#aa007f', bold=True),
    'braces': format_text('#000000'),
    'comment': format_text('#aaaa7f', italic=True),
    'string': format_text('#0000ff', bold=True),
    'operator': format_text('#000000'),
    }