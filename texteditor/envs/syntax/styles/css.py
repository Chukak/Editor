from envs.options import format_text



STYLES_VALUES = {
    'keyword' : 'Keywords',
    'attributes' : 'Attributes in {...} (padding, margin, etc.)',
    'values': 'Values in {...} (1px, 0, etc)',
    'braces': '[]',
    'comment': 'Comments',
    'operator': '=,+,-',
    'extra': 'Extra symbols (;, :, etc.)',
    }

CUSTOM_STYLES = {
    }

DEFAULT_STYLES = {
    'keyword': format_text('#00aa7f', bold=True),
    'attributes': format_text('#aa557f', bold=True),
    'values': format_text('#0000ff'),
    'braces': format_text('#000000'),
    'comment': format_text('#00557f', italic=True),
    'operator': format_text('#aa0000'),
    'extra': format_text('#000000'),
    }