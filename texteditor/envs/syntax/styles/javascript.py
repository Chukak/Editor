from envs.options import format_text



STYLES_VALUES = {
    'keyword' : 'Keywords',
    'psevdokeyword': 'True, False',
    'operator': 'Operators(=,+,not,etc.)',
    'properties': 'Object, Properties ans methods',
    'braces': '() , [] , {}',
    'comment': 'Comments',
    'self': 'self',
    'this': 'this',
    'number': 'Numbers',
    'string': 'Double and single strings',
    'regex': 'JS regexp (Only splash)',
    'docstring': 'Multiline strings',
    }

CUSTOM_STYLES = {
    }

DEFAULT_STYLES = {
    'keyword': format_text('#aa007f', bold=True),
    'psevdokeyword': format_text('#aa007f', italic=True),
    'operator': format_text('#ff0000'),
    'braces': format_text('#aaaa7f'),
    'comment': format_text('#55aa7f', bold=True),
    'self': format_text('#000000', bold=True, italic=True),
    'this': format_text('#910000', bold=True, italic=True),
    'number': format_text('#000000'),
    'properties': format_text('#aa00ff', bold=True),
    'string': format_text('#0000ff'),
    'regex': format_text('#0000ff'),
    'docstring': format_text('#55aa7f', bold=True),
    }