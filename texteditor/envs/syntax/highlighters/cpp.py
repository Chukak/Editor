from envs.lexers.lexers import RegexLexer
from envs.syntax.styles.cpp import CUSTOM_STYLES
from envs.options import format_words

from collections import OrderedDict

class CppHighlighter(RegexLexer):
    '''version c++11 '''
    def __init__(self):
        self.tokens = OrderedDict()
        
        self.tokens['keywords'] = (format_words([
                    'asm','dynamic_cast','namespace','reinterpret_cast','try',
                    'explicit','new','static_cast','typeid',
                    'catch','false','operator','template','typename',
                    'class','friend','private','using',
                    'const_cast','inline','public','throw','virtual',
                    'delete','mutable','protected','true',
                    'auto','const','short','struct','unsigned',
                    'break','continue','else','for','long','signed','switch',
                    'case','default','enum','goto','register','sizeof','typedef','volatile',
                    'do','extern','if','return','static','union','while',#'this',
                    ]), CUSTOM_STYLES['keyword'])
        
        self.tokens['operators'] = (
                    r'!=|==|<<|>>|[-~+/*%=<>&^]',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['operator_word'] = (
                    r'\b(and|not|xor|or|bitand|and_eq|bitor|compl|not_eq|or_eq|xor_eq)\b',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['braces'] = (
                    r'\(|\)|\[|\]|\{|\}',
                    CUSTOM_STYLES['braces'])
        
        self.tokens['number'] = (
                        r'\b([0-9]|[0-9]+[0-9]*|[+-]?0[xX][0-9A-Fa-f]+[IL]?)\b',
                    CUSTOM_STYLES['number'])
        
        self.tokens['preprocessor'] = ( 
                    r'^#.*\b',
                    CUSTOM_STYLES['preprocessor'])
        
        self.tokens['builtins'] = (format_words(['bool', 'char', 'void', 'double', 'float',
                                           'int', 'wchar_t',
                    ]), CUSTOM_STYLES['builtins'])
        
        self.tokens['this'] = (r'\bthis\b', CUSTOM_STYLES['this'])
        
        self.comment = (r'//', CUSTOM_STYLES['comment'])
        self.multiline = ([(r"/\*", r"\*/", 1)] , CUSTOM_STYLES['docstring'])
        self.str_format = CUSTOM_STYLES['string']
        self.escape_symbol = '\\\\'
    
    @classmethod
    def get_highlight_processing(cls):
        return 'default'
