from envs.lexers.lexers import RegexLexer
from envs.syntax.styles.javascript import CUSTOM_STYLES
from envs.options import format_words

from collections import OrderedDict
"""NOT ALL"""
class JavaScriptHighlighter(RegexLexer):
    '''JAVASCRIPT ECMAScript5/6'''
    def __init__(self):
        self.tokens = OrderedDict()
        
        self.tokens['keywords'] = (format_words([
                    'arguments', 'await', 'break', 'case', 'catch', 
                    'class', 'const', 'continue', 'debugger', 'default', 'delete',
                    'do', 'else', 'enum', 'eval', 'export', 'extends', 
                    'finally', 'for', 'function',  
                    'if', 'implements', 'import', 'in', 'instanceof', 
                    'interface', 'let', 'new', 'null',
                    'package', 'private', 'protected', 'public', 'return', 
                    'static', 'super', 'switch', 'throw', 
                    'try' , 'typeof', 'var', 'void', 'while',
                    'with', 'yield', 'alert', 'all', 'anchor', 'anchors', 'area', 'assign', 'blur',
                    'button', 'checkbox', 'clearInterval', 'clearTimeout', 'clientInformation',
                    'close', 'closed', 'confirm', 'constructor', 'crypto', 'decodeURI',
                    'decodeURIComponent', 'defaultStatus', 'document', 'element', 'elements',
                    'embed', 'embeds', 'encodeURI', 'encodeURIComponent', 'escape', 'event',
                    'fileUpload', 'focus', 'form', 'forms', 'frame', 'innerHeight', 
                    'innerWidth', 'layer', 'layers', 'link', 'location', 'mimeTypes', 
                    'navigate', 'navigator', 'frames', 'frameRate', 'hidden', 'history', 
                    'image', 'images', 'offscreenBuffering', 'open', 'opener', 'option', 
                    'outerHeight', 'outerWidth', 'packages', 'pageXOffset', 'pageYOffset', 
                    'parent', 'parseFloat', 'parseInt', 'password', 'pkcs11', 'plugin', 'prompt', 
                    'propertyIsEnum', 'radio', 'reset', 'screenX', 'screenY', 'scroll', 'secure', 
                    'select', 'self', 'setInterval', 'setTimeout', 'status', 'submit', 'taint', 'text', 
                    'textarea', 'top', 'unescape', 'untaint', 'window',]), CUSTOM_STYLES['keyword'])
        
        self.tokens['psevdo_keyords'] = (
                    r'\b(true|false)\b',
                    CUSTOM_STYLES['psevdokeyword'])
        
        self.tokens['operators'] = (
                    r'!=|==|<<|>>|[-~+/*%=<>&^]',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['breaces'] = (
                    r'\(|\)|\[|\]|\{|\}',
                    CUSTOM_STYLES['braces'])
        
        self.tokens['properties'] = (format_words([
                    'Array', 'Date', 'eval', 'function', 'hasOwnProperty', 'Infinity', 'isFinite',
                    'isNaN', 'isPrototypeOf', 'length', 'Math', 'NaN', 'name', 'Number', 
                    'Object', 'prototype', 'String', 'toString', 'undefined', 'valueOf',
                ]), CUSTOM_STYLES['properties'])
        
        self.tokens['number'] = (
                    r'\b([0-9]|[0-9]+[0-9]*|[+-]?0[xX][0-9A-Fa-f]+[IL]?)\b',
                    CUSTOM_STYLES['number'])
        
        self.tokens['regex'] = (
                r'/',
                CUSTOM_STYLES['regex'])
        
        self.tokens['self'] = (
                    r'\bself\b',
                    CUSTOM_STYLES['self'])
        
        self.tokens['this'] = (
                    r'\bthis\b',
                    CUSTOM_STYLES['this'])
        
        self.comment = (r'//', CUSTOM_STYLES['comment'])
        self.multiline = ([(r"/\*", r"\*/", 1)], CUSTOM_STYLES['docstring'])
        self.str_format = CUSTOM_STYLES['string']
        self.escape_symbol = '\\\\'
        
        
    @classmethod
    def get_highlight_processing(cls):
        return 'default'   
    
