from envs.lexers.lexers import RegexLexer
from envs.syntax.styles.css import CUSTOM_STYLES
from envs.options import format_words

from collections import OrderedDict

class CssHighlighter(RegexLexer):
    '''CSS3'''
    def __init__(self):
        self.tokens = OrderedDict()
        
        """self.tokens['keywords'] = (format_words([
                    'a', 'abbr', 'address', 'article', 'aside',
                    'audio', 'b', 'bdi', 'bdo', 'blockquote', 'body',
                    'button', 'canvas', 'caption', 'cite', 'code',
                    'colgroup', 'datalist', 'dd', 'del', 'details', 
                    'dfn', 'dialog', 'div', 'dl', 'dt', 'em', 
                    'fieldset', 'figcaption', 'figure', 'form', 'footer',
                    'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header',
                    'html', 'i', 'iframe', 'input', 'ins', 'kbd', 'label',
                    'legend', 'li', 'map', 'main', 'menu', 'menuitem', 'meter',
                    'nav', 'mark', 'noscript', 'object', 'ol', 'optiongroup',
                    'option', 'p', 'output', 'pre', 'progress', 'rp', 'q',
                    'rt', 'ruby', 's', 'samp', 'script', 'section', 'span',
                    'small', 'select', 'strong', 'style', 'sub', 'summary',
                    'sup', 'table', 'tbody', 'td', 'thead', 'th', 'tfoot',
                    'textarea', 'time', 'tr', 'title', 'u', 'ul', 'var',
                    'video', 'area', 'base', 'br', 'col', 'embed',
                    'hr', 'input', 'li', 'meta', 'param', 
                    'source', 'track', 'wbr', 'img', 'command',
                    'link', ]), CUSTOM_STYLES['keyword']) """
        
        self.tokens['custom_keywords'] = (
                    r'\b\S+\b|\.\S+\b|\#\S+\b',
                    CUSTOM_STYLES['keyword'])
        
        self.in_braces = OrderedDict()
        
        self.in_braces['word_in_braces'] = (
                    r'\b\S+:',
                    CUSTOM_STYLES['attributes'])
        
        self.in_braces['extra'] = (
                    r':|;|,',
                    CUSTOM_STYLES['extra'])
        
        self.in_braces['operators'] = (
                    r'\+|\-',
                    CUSTOM_STYLES['operator'])
        
        self.css_braces = ([(r'\{', r'\}', 2)], CUSTOM_STYLES['values'])
        self.multiline = ([(r"/\*", r"\*/", 1)], CUSTOM_STYLES['comment'])
        
    
    @classmethod
    def get_highlight_processing(cls):
        return 'css'   