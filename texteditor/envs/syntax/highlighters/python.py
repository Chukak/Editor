from envs.lexers.lexers import RegexLexer
from envs.syntax.styles.python import CUSTOM_STYLES
from envs.options import format_words

from collections import OrderedDict

#from envs.environments import STYLES_ENVS
#from envs.environments import STYLES_ENVS


#STYLES = STYLES_ENVS['python3']

class PythonHighlighter(RegexLexer):
    '''version 3 '''
    def __init__(self):
        self.tokens = OrderedDict()
        self.tokens['keyword'] = (format_words([
                    'assert', 'async', 'await', 'break', 'continue', 'del', 'elif',
                    'else', 'except', 'finally', 'for', 'global', 'if', 'lambda', 'pass',
                    'raise', 'nonlocal', 'return', 'try', 'while', 'yield', 
                    'as', 'with', 'import', 'def', 'class', 'from', 'print',
                    ]), CUSTOM_STYLES['keyword'])
        
        self.tokens['psevdo_keywords'] = (
                    r'\b(None|True|False)\b',
                    CUSTOM_STYLES['psevdokeyword'])
        
        self.tokens['operators'] = (
                    r'!=|==|<<|>>|[-~+/*%=<>&^]',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['operator_word'] = (
                    r'\b(in|is|not|or|and)\b',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['braces'] = (
                    r'\(|\)|\[|\]|\{|\}',
                    CUSTOM_STYLES['braces'])
        
        self.tokens['fcname'] = (
                    r'\bdef\b\s*(\w+)|\bclass\b\s*(\w+)',
                    CUSTOM_STYLES['fcname'])
        
        self.tokens['defclass'] = (
                    r'\b(def|class)\b', 
                    CUSTOM_STYLES['defclass'])
        
        self.tokens['number'] = (
                    r'\b([0-9]|[0-9]+[0-9]*|[+-]?0[xX][0-9A-Fa-f]+[IL]?)\b',
                    CUSTOM_STYLES['number'])
        
        self.tokens['decorators'] = (r'(@\w*)',
                    CUSTOM_STYLES['decorator'])
        
        self.tokens['builtins'] = (format_words([
                    '__import__', 'abs', 'all', 'any', 'bin', 'bool', 'bytearray', 'bytes',
                    'chr', 'classmethod', 'cmp', 'compile', 'complex', 'delattr', 'dict',
                    'dir', 'divmod', 'enumerate', 'eval', 'filter', 'float', 'format',
                    'frozenset', 'getattr', 'globals', 'hasattr', 'hash', 'hex', 'id',
                    'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'list',
                    'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct',
                    'open', 'ord', 'pow', 'print', 'property', 'range', 'repr', 'reversed',
                    'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str',
                    'sum', 'super', 'tuple', 'type', 'vars', 'zip',
                    ]), CUSTOM_STYLES['builtins'])
        
        self.tokens['exceptions'] = (
                    r'\b[A-Z].*(Error|Warning)\b',
                    CUSTOM_STYLES['exception']) 
        
        self.tokens['self'] = (
                    r'\bself\b',
                    CUSTOM_STYLES['self'])
        
        self.comment = (r'#', CUSTOM_STYLES['comment'])
        self.regex = (r'r', CUSTOM_STYLES['regex'])
        self.bytes_ = (r'b', CUSTOM_STYLES['bytes'])
        self.unicode = (r'u', CUSTOM_STYLES['unicode'])
        self.multiline = ([(r'"""', r'"""', 1), (r"'''", r"'''", 2)], CUSTOM_STYLES['docstring'])
        self.str_format = CUSTOM_STYLES['string']
        self.escape_symbol = '\\\\'
   
    @classmethod
    def get_highlight_processing(cls):
        return 'default'   

