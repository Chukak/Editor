from envs.lexers.lexers import RegexLexer
from envs.syntax.styles.mysql import CUSTOM_STYLES
from envs.options import format_words

from collections import OrderedDict
#from envs.environments import STYLES_ENVS
#from envs.environments import STYLES_ENVS


#STYLES = STYLES_ENVS['python3']

class MysqlHighlighter(RegexLexer):
    '''version 5 '''
    def __init__(self):
        self.tokens = OrderedDict()
        
        self.tokens['only-sql-keyword'] = (format_words([
                    'date', 'day', 'deallocate', 'dec', 'declare', 'default', 'delete', 'deref', 'desc', 
                    'describe', 'deterministic', 'disconnect', 'distinct', 'do', 'drop', 
                    'dynamic', 'each', 'element', 'else', 'elseif', 'end', 'except', 'exec', 
                    'execute', 'exists', 'exit', 'external', 'false', 'fetch', 'filter',
                    'for', 'foreign', 'free', 'from', 'full', 'function', 'get', 'global',
                    'grant', 'group', 'grouping', 'handler', 'having', 'hold', 'hour',
                    'identity', 'if', 'immediate', 'in', 'indicator', 'inner', 'inout'
                    'input', 'insensitive', 'insert', 'intersect', 'interval', 'into',
                    'is', 'iterate', 'join', 'language', 'large', 'lateral', 'leading', 'leave',
                    'left', 'like', 'local', 'localtime', 'localtimestamp', 'loop', 'match',
                    'member', 'merge', 'method', 'minute', 'modifies', 'module', 'month', 'multiset', 
                    'no', 'none', 'not', 'null', 'of', 'old', 'on', 'only', 'open', 'or', 'order', 
                    'out', 'outer', 'output', 'over', 'overlaps', 'parameter', 'partition', 
                    'precision', 'prepare', 'primary', 'procedure', 'range', 'reads', 'real', 
                    'recursive', 'ref', 'references', 'referencing', 'release', 'repeat', 
                    'resignal', 'result', 'return', 'returns', 'revoke', 'right', 'rollback', 
                    'rollup', 'row', 'rows', 'savepoint', 'scope', 'scroll', 'search', 'second', 
                    'select', 'sensitive', 'session_user', 'set', 'signal', 'similar', 'some', 'specific', 
                    'specifictype', 'sql', 'sqlexception', 'sqlstate', 'sqlwarning', 'start', 
                    'static', 'submultiset', 'symmetric', 'system', 'system_user', 'table', 
                    'tablesample', 'then', 'time', 'timestamp', 'timezone_hour', 'timezone_minute', 
                    'to', 'trailing', 'translation', 'treat', 'trigger', 'true', 'undo', 'union', 
                    'unique', 'unknown', 'unnest', 'until', 'update', 'user', 'using', 'value', 'values', 
                    'varying', 'when', 'whenever', 'where', 'while', 'window', 'with', 'within', 
                    'without', 'year']), CUSTOM_STYLES['keyword']) 
        
        self.tokens['psevdo_keywords'] = (
                    r'\b(null|NULL)\b',
                    CUSTOM_STYLES['keyword'])
        
        self.tokens['operators'] = (
                    r'!=|=|<|>|[-+/*%&]',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['braces'] = (
                    r'\(|\)',
                    CUSTOM_STYLES['braces'])
        
        self.tokens['number'] = (
                    r'\b([0-9]|[0-9]+[0-9]*|[+-]?0[xX][0-9A-Fa-f]+[IL]?)\b',
                    CUSTOM_STYLES['number'])
        
        self.comment = (r'--', CUSTOM_STYLES['comment'])
        self.multiline = ([(r"/\*", r"\*/", 1)], CUSTOM_STYLES['docstring'])
        self.str_format = CUSTOM_STYLES['string']
        
    @classmethod
    def get_highlight_processing(cls):
        return 'sql'  


class MysqlALLHighlighter(RegexLexer):
    '''version 5 '''
    def __init__(self):
        self.tokens = OrderedDict()
        
        self.tokens['all-mysql-keywords'] = (format_words([
                    'accessible', 'account', 'action', 'after', 'against', 'aggregate', 'algorithm', 'all', 
                    'alter', 'always', 'analyse', 'analyze', 'and', 'any', 'as', 'asc', 'ascii', 'asensitive', 
                    'at', 'autoextend_size', 'auto_increment', 'avg', 'avg_row_length', 'backup', 'before', 
                    'begin', 'between', 'binlog', 'bit', 'block', 'btree', 'by', 'cache', 'call', 'cascade', 
                    'cascaded', 'case', 'catalog_name', 'chain', 'change', 'changed', 'channel', 'check', 'checksum', 
                    'cipher', 'class_origin', 'client', 'close', 'coalesce', 'code', 'collate', 'collation', 
                    'column', 'columns', 'column_format', 'column_name', 'comment', 'commit', 'committed', 'compact', 
                    'completion', 'compressed', 'compression', 'concurrent', 'condition', 'connection', 
                    'consistent', 'constraint', 'constraint_catalog', 'constraint_name', 'constraint_schema', 
                    'contains', 'context', 'continue', 'convert', 'cpu', 'create', 'cross', 'current', 
                    'current_date', 'current_time', 'current_timestamp', 'current_user', 'cursor', 'cursor_name', 
                    'database', 'databases', 'datafile', 'date', 'datetime', 'day', 'day_hour', 'day_microsecond', 
                    'day_minute', 'day_second', 'deallocate', 'declare', 'default', 'default_auth', 'definer', 
                    'delayed', 'delay_key_write', 'delete', 'desc', 'describe', 'des_key_file', 'deterministic', 
                    'diagnostics', 'directory', 'disable', 'discard', 'disk', 'distinct', 'distinctrow', 'div', 
                    'do', 'drop', 'dumpfile', 'duplicate', 'each', 'else', 'elseif', 'enable', 'enclosed', 
                    'encryption', 'end', 'ends', 'engine', 'engines', 'enum', 'error', 'errors', 'escape', 
                    'escaped', 'event', 'events', 'every', 'exchange', 'execute', 'exists', 'exit', 'expansion', 
                    'expire', 'explain', 'export', 'extended', 'extent_size', 'false', 'fast', 'faults', 
                    'fetch', 'fields', 'file', 'file_block_size', 'filter', 'first', 'fixed', 'flush', 'follows', 
                    'for', 'force', 'foreign', 'format', 'found', 'from', 'full', 'function', 'general', 
                    'generated', 'geometry', 'geometrycollection', 'get', 'get_format', 'global', 'grant', 
                    'grants', 'group', 'group_replication', 'handler', 'hash', 'having', 'help', 'high_priority', 
                    'host', 'hosts', 'hour', 'hour_microsecond', 'hour_minute', 'hour_second', 'identified', 
                    'if', 'ignore', 'ignore_server_ids', 'import', 'index', 'indexes', 'infile', 'initial_size', 
                    'inner', 'inout', 'insensitive', 'insert', 'insert_method', 'install', 'instance', 'interval', 
                    'into', 'invoker', 'io', 'io_after_gtids', 'io_before_gtids', 'io_thread', 'ipc', 'is', 
                    'isolation', 'issuer', 'iterate', 'join', 'key', 'keys', 'key_block_size', 'kill', 'language', 
                    'last', 'leading', 'leave', 'leaves', 'left', 'less', 'level', 'like', 'limit', 'lines', 
                    'linestring', 'list', 'load', 'local', 'localtime', 'localtimestamp', 'lock', 'locks', 'logfile', 
                    'logs', 'long', 'loop', 'low_priority', 'master', 'master_auto_position', 'master_bind', 
                    'master_connect_retry', 'master_delay', 'master_heartbeat_period', 'master_host', 'master_log_file', 
                    'master_log_pos', 'master_password', 'master_port', 'master_retry_count', 'master_server_id', 
                    'master_ssl', 'master_ssl_ca', 'master_ssl_capath', 'master_ssl_cert', 'master_ssl_cipher', 
                    'master_ssl_crl', 'master_ssl_crlpath', 'master_ssl_key', 'master_ssl_verify_server_cert', 
                    'master_tls_version', 'master_user', 'match', 'max_connections_per_hour', 'max_queries_per_hour', 
                    'max_rows', 'max_size', 'max_statement_time', 'max_updates_per_hour', 'max_user_connections', 
                    'medium', 'memory', 'merge', 'message_text', 'microsecond', 'middleint', 'migrate', 'minute', 
                    'minute_microsecond', 'minute_second', 'min_rows', 'mod', 'mode', 'modifies', 'modify', 'month', 
                    'multilinestring', 'multipoint', 'multipolygon', 'mutex', 'mysql_errno', 'name', 'names', 'ndb', 
                    'ndbcluster', 'never', 'new', 'next', 'nodegroup', 'nonblocking', 'none', 'not', 'no_wait', 
                    'no_write_to_binlog', 'null', 'offset', 'old_password', 'one', 'only', 'open', 'optimize', 
                    'optimizer_costs', 'option', 'optionally', 'options', 'or', 'order', 'out', 'outer', 'outfile', 
                    'owner', 'pack_keys', 'page', 'parser', 'parse_gcol_expr', 'partial', 'partition', 'partitioning', 
                    'partitions', 'password', 'phase', 'plugin', 'plugins', 'plugin_dir', 'point', 'polygon', 'port', 
                    'precedes', 'precision', 'prepare', 'preserve', 'prev', 'primary', 'privileges', 'procedure', 
                    'processlist', 'profile', 'profiles', 'proxy', 'purge', 'quarter', 'query', 'quick', 'range', 
                    'read', 'reads', 'read_only', 'read_write', 'real', 'rebuild', 'recover', 'redofile', 
                    'redo_buffer_size', 'redundant', 'references', 'regexp', 'relay', 'relaylog', 'relay_log_file', 
                    'relay_log_pos', 'relay_thread', 'release', 'reload', 'rename', 'reorganize', 'repair', 
                    'repeat', 'repeatable', 'replace', 'replicate_do_db', 'replicate_do_table', 'replicate_ignore_db',
                    'replicate_ignore_table', 'replicate_rewrite_db', 'replicate_wild_do_table', 
                    'replicate_wild_ignore_table', 'replication', 'require', 'reset', 'resignal', 'restore', 
                    'restrict', 'resume', 'return', 'returned_sqlstate', 'returns', 'reverse', 'revoke', 'right', 
                    'rlike', 'rollback', 'rollup', 'rotate', 'routine', 'row', 'rows', 'row_count', 'row_format', 
                    'rtree', 'savepoint', 'schedule', 'schema', 'schemas', 'schema_name', 'second', 
                    'second_microsecond', 'security', 'select', 'sensitive', 'separator', 'serial', 
                    'serializable', 'server', 'session', 'set', 'share', 'show', 'shutdown', 'signal', 
                    'signed', 'simple', 'slave', 'slow', 'snapshot', 'socket', 'some', 'soname', 'sounds', 
                    'source', 'spatial', 'specific', 'sql', 'sqlexception', 'sqlstate', 'sqlwarning', 
                    'ssl', 'stacked', 'start', 'starting', 
                    'starts', 'stats_auto_recalc', 'stats_persistent', 'stats_sample_pages', 'status', 'stop', 
                    'storage', 'stored', 'straight_join', 'string', 'subclass_origin', 'subject', 'subpartition', 
                    'subpartitions', 'super', 'suspend', 'swaps', 'switches', 'table', 'tables', 'tablespace', 
                    'table_checksum', 'table_name', 'temporary', 'temptable', 'terminated', 'than', 'then', 'time', 
                    'timestamp', 'timestampadd', 'timestampdiff', 'tinyblob', 'to', 'trailing', 'transaction', 
                    'trigger', 'triggers', 'true', 'truncate', 'type', 'types', 'uncommitted', 'undefined', 
                    'undo', 'undofile', 'undo_buffer_size', 'uninstall', 'union', 'unique', 'unknown', 'unlock', 
                    'unsigned', 'until', 'update', 'upgrade', 'usage', 'use', 'user', 'user_resources', 
                    'use_frm', 'using', 'utc_date', 'utc_time', 'utc_timestamp', 'validation', 'value', 
                    'values', 'view', 'virtual', 'wait', 'warnings', 'week', 'weight_string', 'when', 
                    'where', 'while', 'with', 'without', 'work', 'wrapper', 'write', 'x509', 'xa', 'xid',
                    'xml', 'xor', 'year', 'year_month', 'zerofill', 'account', 'always', 'channel', 
                    'compression', 'encryption', 'file_block_size', 'filter', 'follows', 'generated', 
                    'group_replication', 'instance', 'master_tls_version', 'never', 'optimizer_costs', 
                    'parse_gcol_expr', 'precedes', 'replicate_do_db', 'replicate_do_table', 'replicate_ignore_db', 
                    'replicate_ignore_table', 'replicate_rewrite_db', 'replicate_wild_do_table', 
                    'replicate_wild_ignore_table', 'rotate', 'stacked', 'stored', 'validation', 'virtual', 
                    'without', 'xid'
                    ]), CUSTOM_STYLES['keyword'])  
        
        self.tokens['psevdo_keywords'] = (
                    r'\b(null|NULL)\b',
                    CUSTOM_STYLES['keyword'])
        
        self.tokens['operators'] = (
                    r'!=|=|<|>|[-+/*%&]',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['braces'] = (
                    r'\(|\)',
                    CUSTOM_STYLES['braces'])
        
        self.tokens['number'] = (
                    r'\b([0-9]|[0-9]+[0-9]*|[+-]?0[xX][0-9A-Fa-f]+[IL]?)\b',
                    CUSTOM_STYLES['number'])
        
        self.comment = (r'--', CUSTOM_STYLES['comment'])
        self.multiline = ([(r"/\*", r"\*/", 1)], CUSTOM_STYLES['docstring'])
        self.str_format = CUSTOM_STYLES['string']
        
    @classmethod
    def get_highlight_processing(cls):
        return 'sql' 
    
     