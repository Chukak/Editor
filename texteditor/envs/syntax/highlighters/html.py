from envs.lexers.lexers import RegexLexer
from envs.syntax.styles.html import CUSTOM_STYLES
from envs.options import format_html_nonclosed_tags_2, format_html_closed_tags_2

from collections import OrderedDict

class HtmlHighlighter(RegexLexer):
    '''HTML5'''
    def __init__(self):
        self.tokens = OrderedDict()
        
        self.tokens['keywords'] = (
                    format_html_closed_tags_2() + format_html_nonclosed_tags_2(), 
                    CUSTOM_STYLES['keyword'])
        
        self.tokens['braces'] = (
                    r'\{\{|\}\}|\{\%|\%\}|\{\{\.*\}\}|\{\%.*\%\}',
                    CUSTOM_STYLES['braces'])
        
        self.tokens['operators'] = (
                    r'\=|\+|\-',
                    CUSTOM_STYLES['operator'])
        
        self.html_tags = ([(r'<[^(>)(\s+)]+', r'/>|>', 2)], CUSTOM_STYLES['attributes'])
        self.multiline = ([(r"<!--", r"-->", 1)], CUSTOM_STYLES['comment'])
        self.str_format = CUSTOM_STYLES['string']
        
        
    @classmethod
    def get_highlight_processing(cls):
        return 'html'   



