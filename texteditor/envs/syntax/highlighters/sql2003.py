from envs.lexers.lexers import RegexLexer
from envs.syntax.styles.sql2003 import CUSTOM_STYLES
from envs.options import format_words

from collections import OrderedDict


class Sql2003Highlighter(RegexLexer):
    def __init__(self):
        self.tokens = OrderedDict()
        
        self.tokens['sql_keyword'] = (format_words([
                    'date', 'day', 'deallocate', 'dec', 'declare', 'default', 'delete', 'deref', 'desc', 
                    'describe', 'deterministic', 'disconnect', 'distinct', 'do', 'drop', 
                    'dynamic', 'each', 'element', 'else', 'elseif', 'end', 'except', 'exec', 
                    'execute', 'exists', 'exit', 'external', 'false', 'fetch', 'filter',
                    'for', 'foreign', 'free', 'from', 'full', 'function', 'get', 'global',
                    'grant', 'group', 'grouping', 'handler', 'having', 'hold', 'hour',
                    'identity', 'if', 'immediate', 'in', 'indicator', 'inner', 'inout'
                    'input', 'insensitive', 'insert', 'intersect', 'interval', 'into',
                    'is', 'iterate', 'join', 'language', 'large', 'lateral', 'leading', 'leave',
                    'left', 'like', 'local', 'localtime', 'localtimestamp', 'loop', 'match',
                    'member', 'merge', 'method', 'minute', 'modifies', 'module', 'month', 'multiset', 
                    'no', 'none', 'not', 'null', 'of', 'old', 'on', 'only', 'open', 'or', 'order', 
                    'out', 'outer', 'output', 'over', 'overlaps', 'parameter', 'partition', 
                    'precision', 'prepare', 'primary', 'procedure', 'range', 'reads', 'real', 
                    'recursive', 'ref', 'references', 'referencing', 'release', 'repeat', 
                    'resignal', 'result', 'return', 'returns', 'revoke', 'right', 'rollback', 
                    'rollup', 'row', 'rows', 'savepoint', 'scope', 'scroll', 'search', 'second', 
                    'select', 'sensitive', 'session_user', 'set', 'signal', 'similar', 'some', 'specific', 
                    'specifictype', 'sql', 'sqlexception', 'sqlstate', 'sqlwarning', 'start', 
                    'static', 'submultiset', 'symmetric', 'system', 'system_user', 'table', 
                    'tablesample', 'then', 'time', 'timestamp', 'timezone_hour', 'timezone_minute', 
                    'to', 'trailing', 'translation', 'treat', 'trigger', 'true', 'undo', 'union', 
                    'unique', 'unknown', 'unnest', 'until', 'update', 'user', 'using', 'value', 'values', 
                    'varying', 'when', 'whenever', 'where', 'while', 'window', 'with', 'within', 
                    'without', 'year'  
                    ]), CUSTOM_STYLES['keyword'])
        
        self.tokens['keyword'] = (format_words([
                    ]), CUSTOM_STYLES['keyword'])
        
        self.tokens['psevdo_keywords'] = (
                    r'\b(null|NULL)\b',
                    CUSTOM_STYLES['keyword'])
        
        self.tokens['operators'] = (
                    r'!=|=|<|>|[-+/*%&]',
                    CUSTOM_STYLES['operator'])
        
        self.tokens['braces'] = (
                    r'\(|\)',
                    CUSTOM_STYLES['braces'])
        
        self.tokens['number'] = (
                    r'\b([0-9]|[0-9]+[0-9]*|[+-]?0[xX][0-9A-Fa-f]+[IL]?)\b',
                    CUSTOM_STYLES['number'])
        
        self.comment = (r'--', CUSTOM_STYLES['comment'])
        self.multiline = ([(r"/\*", r"\*/", 1)], CUSTOM_STYLES['docstring'])
        self.str_format = CUSTOM_STYLES['string']
        
    @classmethod
    def get_highlight_processing(cls):
        return 'sql'  
    
     