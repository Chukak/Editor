import re 

from PyQt5.QtGui import QSyntaxHighlighter
from PyQt5.QtCore import QRegExp


class SyntaxHighlighter(QSyntaxHighlighter):
    def __init__(self, document, highlighter, processing='default'):
        super().__init__(document)
        if not highlighter is None:
            highlight = highlighter()
            self.rules = highlight.get_tokens()
            self.comment = highlight.get_comment()
            self.extra_string = highlight.get_extra_string()
            self.multiline = highlight.get_multiline()
            self.str_format = highlight.get_string_format()
            self.escape_symbol = highlight.get_escape_symbol()
            if processing == 'html':
                self.html_tags = highlight.get_html_tags()
                self.highlight_function = self.highlightBlockHtml
            elif processing == 'css':
                self.css_braces = highlight.get_css_braces()
                self.css_in_braces = highlight.get_css_in_braces()
                self.highlight_function = self.highlightBlockCss
            elif processing == 'sql':
                self.highlight_function = self.highlightBlockSql
            else:
                self.highlight_function = self.highlightBlockDefault
            
            self.multi = {}
            self.string = []
            self.block_comments = -1
            self.block = -1
            
            self.passed_multi = set()
        else:
            self.setDocument(None)
    
    def highlightBlockHtml(self):
        self.prev_state = self.previousBlockState()
        self.block = self.currentBlock().blockNumber()
        self.setCurrentBlockState(-1)
        
        temp_copy_multiline = self.multiline
        self.multiline = self.html_tags
        self.highlight_multiline()
        self.multiline = temp_copy_multiline
        
        self.string.clear()
        #for regex, format in self.rules:
        #    self.formatting(regex, format)
        [self.formatting(regex, format) for regex, format in self.rules]
        
        if not self.str_format is None:
            self.highlight_string(checkin=False)
        self.highlight_multiline()
    
    def highlightBlockCss(self):
        self.prev_state = self.previousBlockState()
        self.block = self.currentBlock().blockNumber()
        self.setCurrentBlockState(-1)
        
        #for regex, format in self.rules:
        #    self.formatting(regex, format)
        [self.formatting(regex, format) for regex, format in self.rules]
        
        temp_copy_multiline = self.multiline
        self.multiline = self.css_braces
        self.highlight_multiline(checkin=False)
        self.multiline = temp_copy_multiline
        
        if not self.css_in_braces is None:
            #for regex, format in self.css_in_braces:
            #    self.formatting(regex, format)
            [self.formatting(regex, format) for regex, format in self.css_in_braces]
                        
        self.highlight_multiline(checkin=False)    
    
    def highlightBlockSql(self):
        self.string.clear()
        self.prev_state = self.previousBlockState()
        self.block = self.currentBlock().blockNumber()
        self.setCurrentBlockState(-1)
        
        #for regex, format in self.rules:
        #    self.formatting(regex, format)
        [self.formatting(regex, format, flags=re.IGNORECASE) for regex, format in self.rules]
        
        if not self.str_format is None:
            self.highlight_string(checkin=False)
        
        if not self.comment is None:
            self.highlight_comment()
        
        self.highlight_multiline()
        
    def highlightBlockDefault(self):
        self.string.clear()
        self.passed_multi.clear()
        #for regex, format in self.rules:
        #    self.formatting(regex, format)
        [self.formatting(regex, format) for regex, format in self.rules]
                
        self.prev_state = self.previousBlockState()
        self.block = self.currentBlock().blockNumber()
        self.block_comments = -1
        self.setCurrentBlockState(-1)
        
        if not self.str_format is None:
            self.highlight_string()
        
        self.highlight_extra_string()
        
        if not self.comment is None:
            self.highlight_comment()
            
        self.highlight_multiline()
    
    def formatting(self, regex, format, flags=0):
        for n in re.finditer(regex, self.text, flags):
            start, length = n.start(), n.end() - n.start()
            self.setFormat(start, length, format)
        
    def highlightBlock(self, text):
        if not self.parent() is None:
            self.text = text
            self.highlight_function()
    
    def format_string_block(self, start, end):
        format = self.str_format
        end_ = end if end >= 0 else len(self.text)
        self.setFormat(start, end_ - start +1, format)
    
    def get_backsplash(self, pos, exp):
        escape = self.escape_symbol if not self.escape_symbol is None else '\n'
        regex = QRegExp('[^' + escape + ']' + exp.pattern())
        sr = regex.indexIn(self.text, pos)
        return sr +1 if sr >=0 else sr
    
    #@profile
    #############################################   
    def highlight_string(self, checkin=True):
        regex  = [QRegExp(r"'"), QRegExp(r'"')]
        temp = []
        for r in regex:
            res = []
            pos = r.indexIn(self.text)
            while pos >=0:
                if pos >=0:
                    res.append(pos)
                    if len(res) % 2 == 0:
                        pos = r.indexIn(self.text, pos +1)
                    else:
                        pos = self.get_backsplash(pos, r)
                
            temp.append(res)
        result = sorted([r for lst in temp for r in lst])       
        
        if result and checkin:
            multi = self.highlight_multiline(checkin=False, extra=True)
            result = self.check_str_in_multi(result.copy(), result[0], result, multi)
            
        maxi = -1
        passed = []
        for i in result:
            if i >=0 and not i in passed and i > maxi:
                for ls in temp:
                    if ls.count(i):
                        current_list = ls
                        break
                try:
                    end = maxi = ls[current_list.index(i) +1]
                    self.string.append((i, end))
                    passed.append(end)
                except IndexError:
                    self.string.append((i, -1))
                    break
        
        #for s,e in self.string:
        #    self.format_string_block(s, e)
        [self.format_string_block(s, e) for s,e in self.string]
        
    def highlight_comment(self):
        regex, format = self.comment[0], self.comment[1]
        comment = re.match(regex, self.text)
        length = len(self.text)
        if comment:
            self.block_comments = comment.start()
            self.setFormat(0, length, format)
        else:
            for n in re.finditer(regex, self.text):
                in_string = False
                start = n.start()
                for s,e in self.string:
                    bools = [e > 0 and s < start < e, e == -1 and s < start]
                    
                    in_string = set(filter(lambda b: b is not False, bools))
                    if in_string:
                        break
                    #if e > 0 and s < start < e:
                        #if s < start < e:
                    #        in_string = True
                    #        break
                    #elif e == -1 and s < start:
                        #if s < start:
                    #        in_string = True
                    #        break
                if not in_string:
                    self.block_comments = start
                    self.setFormat(start, length, format)
                    break
    
    def highlight_extra_string(self):
        for attribute in self.extra_string:
            pattern, format = attribute
            for r in re.finditer(pattern, self.text):
                for s,e in self.string:
                    if r.start() == s -1:
                        end = e if e > 0 else len(self.text)
                        self.setFormat(r.start(), end - s +1 +1, format)
                            
    ##################################################    
    def highlight_multiline(self, checkin=True, extra=False):
        if not self.multiline is None:
            if extra:
                return self.get_multi_pos(self.multiline[0], checkin=checkin)
            else:
                self.multi.clear()
                format = self.multiline[1]
                lst = self.get_multi_pos(self.multiline[0], checkin=checkin)
                for s, e, state in lst:
                    if e == -1:
                        state, end = state, len(self.text) - s
                    else:
                        state, end = -1, e -s
                    self.setCurrentBlockState(state)
                    self.setFormat(s, end, format)
                self.multi[self.block] = lst
       
    ###############################################
    def get_multi_pos(self, regexs, checkin=True):
        temp, result_pos = [], []
        more_multi = len(regexs)
        for ss, ee, state in regexs:
            ss, ee = QRegExp(ss), QRegExp(ee)
            res = []
            if self.prev_state >0 and self.prev_state == state:
                pos, cap, multi = 0, 0, True
            else:
                pos, cap, multi = ss.indexIn(self.text), ss.matchedLength(), False
            if self.block_comments >= 0:
                if pos > self.block_comments:
                    continue
            while pos >=0:
                end, ecap = ee.indexIn(self.text, pos + cap), ee.matchedLength()
                find = pos + cap +1
                if end >=0:
                    res.append((pos, end + ecap, state))
                    if multi:
                        find = end + ecap +1
                else:
                    res.append((pos, -1, state))
                    
                pos = ss.indexIn(self.text, find)
                cap = ss.matchedLength() 
            
            temp.append(res)
        
        result = sorted([(s,e,state) for ls in temp for s,e,state in ls])
        
        if checkin and result:
            result = self.check_multi_in_str(result.copy(), result[0], result)
            if result and more_multi:
                result= self.check_multi_in_multi(result.copy(), result[0], result)
        maxi = -1
        passed = []
        for s, e, state in result:
            if s >=0 and not (s,e, state) in passed and s > maxi:
                for ls in temp:
                    if ls.count((s, e, state)):
                        current_list = ls
                        break
                try:
                    end = ls[current_list.index((s, e, state))]
                    maxi = end[1]
                    result_pos.append((s, e, state))
                    passed.append(end)
                except IndexError:
                    result_pos.append((s, -1, state))
                    break
        return result_pos
    
    #############################################
    def get_multiline_pos(self, pos):
        for ss, ee, state in self.multiline[0]:
            rexp = QRegExp(ss)
            res = rexp.indexIn(self.text, pos)
            if res >=0:
                return res + rexp.matchedLength()
        return -1
    
    def check_str_in_multi(self, copie, pos, result, multi):
        if pos >=0 and multi:
            for s,e,state in multi:
                if e == -1 and s <= pos:
                    ch = self.get_multiline_pos(pos)
                    if s <= pos <= ch and ch >=0:
                        result.remove(pos)
                        break
                else:
                    if s <= pos < e and not (s, e) in self.passed_multi:
                        result.remove(pos)
                        break
                    elif pos < s:
                        self.passed_multi.add((s, e, state))
        try:
            pos = copie[copie.index(pos) +1]
            return self.check_str_in_multi(copie, pos, result, multi)
        except IndexError:
            return result
        
    #################################################
    def get_state_multi(self, pos):
        for ss, ee, state in self.multiline[0]:
            rexp = QRegExp(ss)
            res = rexp.indexIn(self.text, pos)
            if res >=0:
                return state
        return -1
    #################################################
    def map_multi_in_multi(self, s, e, state):
        if not pos and not s:
            stt = self.get_state_multi(pos)
            if state_item == stt:
                result.remove(item)
                #break
                    
        if e == -1 and s < pos:
            result.remove(item)
            #break
        elif e >=0 and s < pos < e:
            result.remove(item)
            #break
        
    ##############################################
    def check_multi_in_multi(self, copie, item, result):
        pos, end, state_item = item
        index = result.index(item)
        copie.remove(item)
        if pos >=0:
            #map()
            for s,e,state in copie:
                if not pos and not s:
                    stt = self.get_state_multi(pos)
                    if state_item == stt:
                        result.remove(item)
                        break
                if e == -1 and s < pos:
                    result.remove(item)
                    break
                elif e >=0 and s < pos < e:
                    result.remove(item)
                    break
        try:
            copie = result.copy()
            new = copie[index +1]
            return self.check_multi_in_multi(copie, new, result)
        except IndexError:
            return result
        
    ####################################
    def check_multi_in_str(self, copie, item, result):
        start, end, state = item
        if end >= 0 and item in self.passed_multi:
            result.remove(item)
        else:
            for s,e in self.string:
                if e > 0 and s <= start < e:
                    result.remove(item)
                elif e == -1 and s <= start:
                    result.remove(item)
        try:
            new = copie[copie.index(item) +1]
            return self.check_multi_in_str(copie, new, result)
        except IndexError:
            return result            
       






            
    
    

