from PyQt5.QtWidgets import QDialog, QListWidget, QCheckBox, QPushButton, QGridLayout,\
    QLabel, QListWidgetItem, QColorDialog, QMessageBox, QDialogButtonBox
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QPixmap
from functools import partial

from envs.environments import ENVIRONMENT, STYLES_ENVS, STYLES_VALUES_ENVS, ENVIRONMENT_VALUES, DEFAULT_STYLES_ENVS
from envs.icons import ENVS_ICON

from PyQt5.Qt import QWidget

class DialogEnvs(QDialog):
    environ = ''
    checks = {}
    indexes_words = []
    env_styles = {}
    find_environ = 'none'
    
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        self.setParent(parent, Qt.Dialog)
        self.setMaximumWidth(700)
        
        layout = QGridLayout(self)
        left = self.set_left()
        right = self.set_right()
        mbox = self.set_button_box()
        
        layout.addWidget(left, 0, 0)
        layout.addWidget(right, 0, 1)
        layout.addWidget(mbox, 1, 1)
        
        self.setLayout(layout)
        self.setWindowTitle(self.tr('Selecting the environ development'))
    
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        self.save = bbox.addButton(self.tr('Save'), QDialogButtonBox.ApplyRole)
        close = bbox.addButton(self.tr('Close'), QDialogButtonBox.NoRole)
        #cancel = bbox.addButton('Отмена', QDialogButtonBox.NoRole)
        self.save.clicked.connect(self.save_environ)
        close.clicked.connect(self.close)
        #cancel.clicked.connect(self.close)
        self.save.setDisabled(True)
        return bbox
    
    def set_left(self):
        left = QWidget(self)
        left.setMaximumWidth(300)
        layout = QGridLayout(left)
        
        self.environ_list = QListWidget(self)
        self.environ_list.mouseDoubleClickEvent = self.mouseDoubleClickEventForListEnviron
        for name in ENVIRONMENT:
            item = QListWidgetItem(ENVIRONMENT_VALUES[name], self.environ_list)
            if self.object.default_environ == name:
                self.find_environ = text = self.tr(ENVIRONMENT_VALUES[name] + ' (Default)')
                item.setText(text)
            item.env = name
        self.environ_list.itemSelectionChanged.connect(self.change_environ)
        
        bbox = QDialogButtonBox(self)
        self.bchoice = bbox.addButton(self.tr('Select'), QDialogButtonBox.AcceptRole)
        self.default_env = bbox.addButton(self.tr('Default'), QDialogButtonBox.AcceptRole)
        self.save_env = bbox.addButton(self.tr('Save'), QDialogButtonBox.AcceptRole)
        #clear_choice = bbox.addButton('Очистить', QDialogButtonBox.ResetRole)
        self.bchoice.clicked.connect(self.choice_env)
        self.bchoice.setDisabled(True)
        self.default_env.clicked.connect(self.set_default_env)
        self.default_env.setDisabled(True)
        self.save_env.clicked.connect(self.save_environ_without_file)
        #clear_choice.clicked.connect(partial(self.clear_env, self.environ_list))
        
        layout.addWidget(self.environ_list, 0, 0)
        layout.addWidget(bbox, 1, 0)
        
        left.setLayout(layout)
        return left
    
    def set_right(self):
        right = QWidget(self)
        layout = QGridLayout(right)
        
        self.list_keyword = QListWidget(right)
        self.list_keyword.itemSelectionChanged.connect(self.change_item)
        
        option_widget = QWidget(right)
        #option_widget.setMaximumWidth(100)
        option_layout = QGridLayout(option_widget)
        
        self.bcolor = QPushButton(self.tr('Color'), right)
        self.bcolor.clicked.connect(self.color_dialog)
        self.bcolor.setDisabled(True)
        
        self.bold_check = QCheckBox(self.tr('Bold'), right)
        self.bold_check.stateChanged.connect(self.state_checkbox_bold)
        self.bold_check.setDisabled(True)
        self.italic_check = QCheckBox(self.tr('Italic'), right)
        self.italic_check.stateChanged.connect(self.state_checkbox_italic)
        self.italic_check.setDisabled(True)
        
        self.default_format = QPushButton(self.tr('Reset'), right)
        self.default_format.clicked.connect(self.set_default_format)
        self.default_format.setDisabled(True)
        
        self.env_label = QLabel(self.tr('Not selected'), self)
        
        option_layout.addWidget(self.env_label, 0, 0)
        option_layout.addWidget(self.bcolor, 1, 0)
        option_layout.addWidget(self.bold_check, 2, 0)
        option_layout.addWidget(self.italic_check, 3, 0)
        option_layout.addWidget(self.default_format, 4, 0)
        option_widget.setLayout(option_layout)
        
        layout.addWidget(self.list_keyword, 0, 0)
        layout.addWidget(option_widget, 0, 1)
        right.setLayout(layout)
        return right
    
    def get_env_items(self, environ):
        self.list_keyword.clear()
        if isinstance(STYLES_ENVS[environ], dict):
            env = self.env_styles = STYLES_ENVS[environ].copy()
        else:
            env = self.env_styles = None
        if not env is None:
            for word, format in env.items():
                name = STYLES_VALUES_ENVS[environ][word]
                item = QListWidgetItem(name, self.list_keyword)
                item.setFont(format.font())
                item.setForeground(format.foreground())
                item.name = word
                self.list_keyword.addItem(item)
        else:
            self.bcolor.setDisabled(True)
            self.bold_check.setChecked(False)
            self.bold_check.setDisabled(True)
            self.italic_check.setChecked(False)
            self.italic_check.setDisabled(True)
        
    def color_dialog(self, *args):
        cdialog = QColorDialog(self)
        cdialog.show()
        cdialog.colorSelected.connect(self.set_color_item)  
        
    def set_color_item(self, color):
        item = self.list_keyword.currentItem()
        name = item.name
        item.setForeground(color) 
        self.env_styles[name].setForeground(color)
    
    def change_item(self):
        item = self.list_keyword.currentItem()
        name = getattr(item, 'name', None)
        if not name is None and name in self.env_styles:
            font = item.font()
            if font.bold():
                self.bold_check.setChecked(True)
            else:
                self.bold_check.setChecked(False)
            if font.italic():
                self.italic_check.setChecked(True)
            else:
                self.italic_check.setChecked(False)
                
            self.bcolor.setDisabled(False)
            self.bold_check.setDisabled(False)
            self.italic_check.setDisabled(False)
        else:
            self.bcolor.setDisabled(True)
            self.bold_check.setChecked(False)
            self.bold_check.setDisabled(True)
            self.italic_check.setChecked(False)
            self.italic_check.setDisabled(True)
            
    def change_environ(self):
        item = self.environ_list.currentItem()
        if not item is None:
            self.bchoice.setDisabled(False)
            self.default_env.setDisabled(False)
        else:
            self.bchoice.setDisabled(True)
            self.default_env.setDisabled(True)
        
    def state_checkbox_bold(self, state):
        item = self.list_keyword.currentItem()
        if not item is None:
            name, font = item.name, item.font()
            if state:
                font.setBold(True)
                self.env_styles[name].setFontWeight(QFont.Bold)
            else:
                font.setBold(False)
                self.env_styles[name].setFontWeight(QFont.Normal)
            font_ = self.env_styles[name].font()
            item.setFont(font_)
    
    def state_checkbox_italic(self, state):
        item = self.list_keyword.currentItem()
        if not item is None:
            name, font = item.name, item.font()
            if state:
                font.setItalic(True)
                self.env_styles[name].setFontItalic(True)
            else:
                font.setItalic(False)
                self.env_styles[name].setFontItalic(False)
            font_ = self.env_styles[name].font()
            item.setFont(font_)
    
    def set_default_format(self):
        default = DEFAULT_STYLES_ENVS[self.environ]
        if not default is None:
            for word, format in default.items():
                STYLES_ENVS[self.environ][word] = format
            self.env_styles = STYLES_ENVS[self.environ]
            self.list_keyword.clear()
            for word, format in self.env_styles.items():
                    name = STYLES_VALUES_ENVS[self.environ][word]
                    item = QListWidgetItem(name, self.list_keyword)
                    item.setFont(format.font())
                    item.setForeground(format.foreground())
                    item.name = word
                    self.list_keyword.addItem(item)
        
    def set_default_env(self, *args):
        next = self.environ_list.currentItem()
        found = self.environ_list.findItems(self.find_environ, Qt.MatchContains)
        if found:
            prev = found[0]
            prev.setText(ENVIRONMENT_VALUES[prev.env])
            self.find_environ = text = self.tr(ENVIRONMENT_VALUES[next.env] + ' (Default)')
            next.setText(text)
            self.object.default_environ = next.env
       
    def choice_env(self, *args):
        self.environ = env = self.environ_list.currentItem().env
        if env in ENVIRONMENT:
            if not isinstance(ENVIRONMENT[env], type(None)):
                icon = ENVS_ICON.get(env, None)
                if not icon is None:
                    pix = QPixmap(icon).scaled(80, 80, Qt.KeepAspectRatio)
                    self.env_label.setPixmap(pix)
                self.get_env_items(env)
                self.save.setDisabled(False)
                self.default_format.setDisabled(False)
                self.change_item()
            else:
                pix = QPixmap(ENVS_ICON['none']).scaled(80, 80, Qt.KeepAspectRatio)
                self.env_label.setPixmap(pix)
                self.list_keyword.clear()
                self.get_env_items(env)
                self.save.setDisabled(False)
                self.default_format.setDisabled(True)
        else:
            self.save.setDisabled(True)
            self.default_format.setDisabled(True)
    
    def save_environ_without_file(self, *args):
        env = self.environ
        if env in ENVIRONMENT:
            STYLES_ENVS[env] = self.env_styles
        
    def save_environ(self, *args):
        env = self.environ
        if env in ENVIRONMENT:
            index = self.object.tabs.currentIndex()
            if index > -1:
                file = self.object.file_in_tabs[index]
                self.object.tabs.widget(index).set_highlighter(env)
                self.object.environs[file] = env
                self.object.status_bar()
                #self.close()
    
    """EVENTS"""
    def mouseDoubleClickEventForListEnviron(self, event):
        self.choice_env()
    
    def closeEvent(self, event):
        self.object.tabs.setEnabled(True)
        super().closeEvent(event)
    
    #def close(self):
    #    self.object.tabs.setEnabled(True)
    #    super().close()
    #    self.object.tabs.setEnabled(True)
    
    
        
        
    
    #def clear_env(self, *args, environ):
    #    item = environ.currentItem()
    #    env = item.text()
    #    if env in ENVIRONMENT:
    #        environ.clearSelection()
    #        self.list_keyword.clear()
    #        self.env_label.setText('Не выбрано')
    #        self.environ = ''
    #        self.save.setDisabled(True)
    #        self.bchoice.setDisabled(True)
            
        
            

    
    
