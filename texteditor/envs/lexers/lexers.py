
class RegexLexer(object):
    def get_tokens(self):
        return [token for token in self.tokens.values()]
    
    def get_comment(self):
        try:
            return self.comment
        except AttributeError:
            return None
    
    def get_braces(self):
        try:
            return self.braces
        except AttributeError:
            return None
    
    def get_regex(self):
        try:
            return self.regex
        except AttributeError:
            return None
    
    def get_bytes(self):
        try:
            return self.bytes_
        except AttributeError:
            return None
    
    def get_unicode(self):
        try:
            return self.unicode
        except AttributeError:
            return None
        
    def get_string_format(self):
        try:
            return self.str_format
        except AttributeError:
            return None
    
    def get_multiline(self):
        try:
            return self.multiline
        except AttributeError:
            return None
    def get_escape_symbol(self):
        try:
            return self.escape_symbol
        except AttributeError:
            return None
            #return '\\\\'  
    
    def get_html_tags(self):
        try:
            return self.html_tags
        except AttributeError:
            return None  
    
    def get_css_braces(self):
        try:
            return self.css_braces
        except AttributeError:
            return None
    
    def get_css_in_braces(self):
        try:
            return [in_braces for in_braces in self.in_braces.values()]
        except AttributeError:
            return None
        
    def get_extra_string(self):
        return [r for r in [self.get_regex(), self.get_unicode(), 
                            self.get_bytes()
                            ] if not r is None]
           
           
    