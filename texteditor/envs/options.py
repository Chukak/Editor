from PyQt5.QtGui import QTextCharFormat, QFont, QColor

import re

def format_text(color, bold=False, italic=False):
    format = QTextCharFormat()
    format.setForeground(QColor(color))
    if bold:
        format.setFontWeight(QFont.Bold)
    if italic:
        format.setFontItalic(True)
    return format

#def format_words_sql(words):
#    return re.compile(r'' + str('\\b(' + '|'.join(words) + ')\\b'), flags.re.IGNORECASE)
    

def format_words(words):
    return r''+ str('\\b(' + '|'.join(words) + ')\\b')

#def format_words(words):
#    return [r'\b%s\b' %word for word in words]
    
def format_html_closed_tags_2():
    #return [r'<\S+>|<\S+|<\/\S+>|<\/\S+|<|>']
    return r'<\w+(?!=[^>])>|<\w+|<\/\w+>|<\/\w+|<|>'

def format_html_nonclosed_tags_2():
    #return [r'<\S+>|<\S+/>|<\S+|<|>|/>']
    return r'<\w+>|<\w+/>|<\w+|<|>|/>'






