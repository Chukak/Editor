import time
from PyQt5.QtWidgets import QSplashScreen, QLabel, QProgressBar
from PyQt5.QtGui import QPixmap, QPalette, QColor
from PyQt5.QtCore import Qt


class SplashScreen(QSplashScreen):
    def __init__(self, application, main, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = application
        self.main = main
        self.loop = 0
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus)
        self.setEnabled(False)
        self.set_gui()
        self.create_progress_bar()
        self.create_label()
    
    def create_label(self):
        self.label = QLabel(self)
        self.label.setText(self.tr('Run'))
        self.label.setAlignment(Qt.AlignCenter | Qt.AlignTop)
    
    def create_progress_bar(self):
        self.progress = QProgressBar(self)
        self.progress.setMaximum(10)
        self.progress.setAlignment(Qt.AlignCenter)
        palette = self.progress.palette()
        palette.setColor(QPalette.Text, QColor('black'))
        self.progress.setPalette(palette)
    
    def set_geometry(self, rect):
        x, y = rect.x(), rect.y()
        self.setGeometry(x, y, 550, 344)
        self.progress.setGeometry(8, self.height() - 50, self.width() - 100, 18)
        self.label.setGeometry(8, self.height() - 120, self.width() - 100, 50)
    
    def set_gui(self):
        pix = QPixmap('icon/main/python3.png')
        pixmap = pix.scaled(550, 344)
        self.setPixmap(pixmap)
    
    def go_cycle(self):
        for i in range(1, 11):
            self.progress.setValue(i)
            t = time.time()
            while time.time() < t +0.1:
                self.label.setText(self.tr('<font color="black">Loading Qt components.</font>'))
                self.set_palette_cycle(index=i)
                #self.app.processEvents()
        time.sleep(0.001)
        self.set_palette_cycle_default()
        self.progress.setRange(0, 100)
        
    def waiting_syntax_highlights(self, file):
        self.loop += 100 // len(self.main.path_file) - 0.1
        self.progress.setValue(self.loop)
        message = str(self.tr('<font color="black">Loading files.<br>The execution time depends on the file size.<br>'
                    'Current file: <font color="red" face="roboto">"%s"</font></font>' %file))
        self.label.setText(message)
        self.set_palette_cycle()
        self.app.processEvents()
    
    def set_palette_cycle(self, index=-1):
        loop = self.loop if index == -1 else index
        sep = 50 if index == -1 else 5 
        if loop >= sep:
            palette = self.progress.palette()
            palette.setColor(QPalette.Text, QColor('white'))
            self.progress.setPalette(palette)
    
    def set_palette_cycle_default(self):
        palette = self.progress.palette()
        palette.setColor(QPalette.Text, QColor('black'))
        self.progress.setPalette(palette)
                                   
    def show(self):
        super().show()
        self.go_cycle()
    
    def finish(self, win):
        self.progress.setValue(10)
        return super().finish(win)
        