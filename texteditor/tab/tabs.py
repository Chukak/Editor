import os
from functools import partial
from PyQt5.QtWidgets import QTabWidget
from texteditor.editor import Editor
from utils.utils import get_env_for_editor
from PyQt5.QtCore import Qt



class Tabs(QTabWidget): 
    prev_index = 0
    
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        self.setParent(parent) 
        self.icons = self.object.icons
    
    def create_tabs(self): 
        splash = self.object.sp
        for f in self.object.path_file:
            splash.waiting_syntax_highlights(f)
            editor = Editor(parent=self, font=self.object.option['font'], 
                            insert=self.object.file_data[f], env=self.object.environs[f])
            self.object.func.check_file_with_runner(f, editor)
            icon = self.icons.get_icon_file(os.path.basename(f))
            index = self.addTab(editor, icon, os.path.basename(f))
            self.object.file_in_tabs[index] = f
            
        self.setCurrentIndex(0)
        self.object.status_bar()
        widget = self.widget(0)
        self.object.func.refresh_code_buttons(widget)
        self.object.func.refresh_sql_code_buttons(widget)
        self.setTabsClosable(True)
        self.setMovable(True)
        self.tabCloseRequested.connect(self.close_tab)
        self.tabBar().tabMoved.connect(self.moved_tab)
        
        if self.count():
            self.object.setWindowTitle(self.object.file_in_tabs[0])
        self.currentChanged.connect(self.change_tabs)
        
        
    def create_new_tab(self, path, data=''):
        font = self.object.option['font']
        get = get_env_for_editor(path) 
        env_ = get if not get is None else self.object.default_environ
        editor = Editor(parent=self, font=font, insert=data, env=env_)
        if not self.count():
            self.object.file_in_tabs[0] = path
        icon = self.icons.get_icon_file(os.path.basename(path))
        index = self.addTab(editor, icon, os.path.basename(path))
        self.object.file_in_tabs[index] = path
        self.object.environs[path] = env_
        self.setCurrentIndex(index)
        self.object.status_bar()
        
    def close_tab(self, index):
        widget = self.widget(index)
        if widget is not None:
            widget.deleteLater()
        self.removeTab(index)
        self.object.path_file.remove(self.object.file_in_tabs[index])
        self.object.func.close_tab_with_search(index)
        count = self.count()
        if count:
            file_in_tabs = self.object.file_in_tabs.copy()
            for key in file_in_tabs:
                if key > index:
                    self.object.file_in_tabs[key -1] = file_in_tabs[key]
            del self.object.environs[file_in_tabs[index]]
            del self.object.file_in_tabs[count]
        else:
            del self.object.file_in_tabs[0]
            self.object.status_bar()
        
    def moved_tab(self, from_, to):
        copy = self.object.file_in_tabs.copy()
        self.object.file_in_tabs[from_] = copy[to]
        self.object.file_in_tabs[to] = copy[from_]
    
    def change_tabs(self, index):
        if index != -1:
            self.object.func.clear_text_line(index=self.prev_index)
            self.prev_index = index
            name = self.object.file_in_tabs[index]
            widget = self.widget(index)
            self.object.func.refresh_code_buttons(widget)
            self.object.func.refresh_sql_code_buttons(widget)
            self.object.setWindowTitle(name)
            self.object.status_bar()
    
    
    
    