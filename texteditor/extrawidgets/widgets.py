from PyQt5.QtWidgets import QPushButton, QLineEdit, QWidget, QGridLayout, QFrame,\
    QAction
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize
from functools import partial

from utils.utils import EditorFuncForMenu




class TopWidget(QWidget):
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        self.extra_func = EditorFuncForMenu(parent)
        self.setParent(parent)
        self.setFixedHeight(40)
        
        left_widget = self.create_left_top()
        right_widget = self.create_right_top()
        
        topgrid = QGridLayout(self)
        topgrid.addWidget(left_widget, 0, 0)
        topgrid.addWidget(right_widget, 0, 1)
        
        self.setMaximumWidth(700)
        self.setLayout(topgrid)
        self.layout().setContentsMargins(0, 1, 0, 1)
        self.setContentsMargins(0, 1, 0, 1)
    
    def create_left_top(self):
        left_widget = QWidget(self)
        left_widget.setFixedHeight(35)
        #left_widget.setFixedWidth(325)
        left_widget.setMaximumWidth(330)
        
        self.line_text = QLineEdit(left_widget)
        self.line_text.setPlaceholderText(self.tr('Search'))
        self.line_text.setFixedWidth(180)
        self.line_text.setFixedHeight(25)
        self.line_text.setObjectName('linesearch')
        
        self.search = QPushButton(left_widget)
        self.search.setIcon(QIcon('icon/menu/search/search.png'))
        self.search.setIconSize(QSize(15,15))
        self.search.setFixedHeight(25) 
        self.search.setFixedWidth(28)
        self.search.setObjectName('butsearch')
        self.search.clicked.connect(self.line_text.returnPressed)
        
        self.next = QPushButton(left_widget)
        self.next.setIcon(QIcon('icon/menu/search/next.png'))
        self.next.setIconSize(QSize(25, 12))
        self.next.setFixedHeight(25) 
        self.next.setFixedWidth(50)
        self.next.clicked.connect(partial(self.object.func.search_next_prev, step=1))
        
        self.previous = QPushButton(left_widget)
        self.previous.setIcon(QIcon('icon/menu/search/prev.png'))
        self.previous.setIconSize(QSize(25, 12))
        self.previous.setFixedHeight(25) 
        self.previous.setFixedWidth(50)
        self.previous.clicked.connect(partial(self.object.func.search_next_prev, step=-1))
        
        self.clear = QAction(QIcon('icon/menu/search/clear.png'), '',  self.line_text)
        self.clear.triggered.connect(self.line_text.clear)
        self.clear.triggered.connect(partial(self.object.func.clear_text_line))
        self.clear.setDisabled(True)
        self.line_text.addAction(self.clear, 1)
        
        self.line_text.returnPressed.connect(partial(self.object.func.find_text, 
                                                     line=self.line_text))
        self.line_text.textChanged.connect(self.clear_button_update)
        
        self.prev_next_button_update()
        
        frame = QFrame(left_widget)
        frame.setFixedHeight(25)
        frame.setFixedWidth(22)
        
        topgrid = QGridLayout(left_widget)
        topgrid.setHorizontalSpacing(0)
        topgrid.addWidget(self.line_text, 0, 0)
        topgrid.addWidget(self.search, 0, 1)
        topgrid.addWidget(frame, 0, 2)
        topgrid.addWidget(self.previous, 0, 3)
        topgrid.addWidget(self.next, 0, 4)
        
        left_widget.setLayout(topgrid)
        left_widget.layout().setContentsMargins(0, 1, 0, 1)
        left_widget.setContentsMargins(0, 1, 0, 1)
        return left_widget
    
    def create_right_top(self):
        right_widget = QWidget(self)
        right_widget.setFixedHeight(35)
        #right_widget.setFixedWidth(235)
        right_widget.setMaximumWidth(350)
        
        bsave = QPushButton(right_widget)
        bsave.setIcon(QIcon('icon/menu/save.png'))
        bsave.setToolTip(self.tr('Save'))
        bsave.setFixedHeight(25)
        bsave.setFixedWidth(35)
        bsave.clicked.connect(self.object.func.save_file)
        
        bsaveall = QPushButton(right_widget)
        bsaveall.setIcon(QIcon('icon/menu/saveall.png'))
        bsaveall.setToolTip(self.tr('Save all'))
        bsaveall.setFixedHeight(25)
        bsaveall.setFixedWidth(35)
        bsaveall.clicked.connect(self.object.func.save_all_file)
        
        bcreate = QPushButton(right_widget)
        bcreate.setIcon(QIcon('icon/menu/create.png'))
        bcreate.setToolTip(self.tr('Create'))
        bcreate.setFixedHeight(25)
        bcreate.setFixedWidth(35)
        bcreate.clicked.connect(self.object.func.create_file)
        
        bopen = QPushButton(right_widget)
        bopen.setIcon(QIcon('icon/menu/open.png'))
        bopen.setToolTip(self.tr('Open'))
        bopen.setFixedHeight(25)
        bopen.setFixedWidth(35)
        bopen.clicked.connect(self.object.func.open_file)
        
        bundo = QPushButton(right_widget)
        bundo.setIcon(QIcon('icon/editor/menu/undo.png'))
        bundo.setToolTip(self.tr('Undo'))
        bundo.setFixedHeight(25)
        bundo.setFixedWidth(35)
        bundo.clicked.connect(self.extra_func.undo)
        
        bredo = QPushButton(right_widget)
        bredo.setIcon(QIcon('icon/editor/menu/redo.png'))
        bredo.setToolTip(self.tr('Redo'))
        bredo.setFixedHeight(25)
        bredo.setFixedWidth(35)
        bredo.clicked.connect(self.extra_func.redo)
        
        self.bcode = QPushButton(right_widget)
        self.bcode.setToolTip(self.tr('Run script'))
        self.bcode.setIcon(QIcon('icon/menu/script.png'))
        self.bcode.setFixedHeight(25)
        self.bcode.setFixedWidth(35)
        self.bcode.clicked.connect(self.object.func.run_code)
        self.bcode.setEnabled(False)
        
        self.bcodesql = QPushButton(right_widget)
        self.bcodesql.setToolTip(self.tr('Run sql script'))
        self.bcodesql.setIcon(QIcon('icon/menu/scriptsql.png'))
        self.bcodesql.setFixedHeight(25)
        self.bcodesql.setFixedWidth(35)
        self.bcodesql.clicked.connect(self.object.func.run_sql_code)
        self.bcodesql.setEnabled(False)
        
        frame = QFrame(right_widget)
        frame.setFixedHeight(25)
        frame.setFixedWidth(20)
        
        topgrid = QGridLayout(right_widget)
        topgrid.setHorizontalSpacing(2)
        topgrid.addWidget(bcreate, 0, 0)
        topgrid.addWidget(bopen, 0, 1)
        topgrid.addWidget(bsave, 0, 2)
        topgrid.addWidget(bsaveall, 0, 3)
        topgrid.addWidget(frame, 0, 4)
        topgrid.addWidget(bundo, 0, 5)
        topgrid.addWidget(bredo, 0, 6)
        topgrid.addWidget(frame, 0, 7)
        topgrid.addWidget(self.bcode, 0, 8)
        topgrid.addWidget(self.bcodesql, 0, 9)
       
        right_widget.setLayout(topgrid)
        right_widget.layout().setContentsMargins(0, 1, 0, 1)
        right_widget.setContentsMargins(20, 1, 0, 1)
        return right_widget

    def clear_button_update(self, text):
        if text:
            self.clear.setEnabled(True)
        else:
            self.clear.setDisabled(True)
    
    def prev_next_button_update(self, option=False):
        self.previous.setEnabled(option)
        self.next.setEnabled(option)
        
        