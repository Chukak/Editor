"""
ВРЕМЕННО НЕРАБОТАЕТ
"""


import unittest 
import sys

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QTabWidget
from unittests.fileicons import TestFileIcons
from unittests.explorer import TestExplorer
from unittests.highlights import TestHighlights
from unittests.options import TestBaseOptions

from files.icons import Icons
from options.base import EFunc
from tab.tabs import Tabs 
from explorer.explorer import Explorer

ALL_TESTS = [TestExplorer, TestHighlights, TestBaseOptions]


def create_suite():
    loader = unittest.TestLoader() 
    suite = unittest.TestSuite()
    for test in ALL_TESTS:
        result = loader.loadTestsFromTestCase(test)
        suite.addTest(result)
    return suite
    
def set_application_test(app):
    for test in ALL_TESTS:
        test.application = app
        test.main = MainWindow()     
      
class TestApplication(QApplication):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def event(self, event):
        print(event)


class TestEFunc(EFunc):
    def __init__(self, parent):
        super().__init__(parent)
    
        
class TestTabs(QTabWidget):
    def __init__(self, parent):
        super().__init__()
        self.object = parent
                
    def create_new_tab(self, path, data=''):
        pass
    
    def create_tabs(self):
        pass
    
    def close_tab(self, index):
        pass
    
    def moved_tab(self, from_, to):
        pass
    
    def change_tabs(self, index):
        pass
        
class TestTopWidget(QWidget):
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        
    def create_left_top(self):
        pass
    
    def create_right_top(self):
        pass
    
    def clear_button_update(self, text):
        pass
    
    def prev_next_button_update(self, option=False):
        pass





class MainWindow(QMainWindow):
    projects = []
    file_in_tabs = {}
    path_file = []
            
    def __init__(self):
        super().__init__()
        self.icons = Icons()
        self.func = TestEFunc(self)
        self.tabs = TestTabs(self)
        #self.explorer = Explorer(self)
        self.top_widget = TestTopWidget(self)
        
        

        
                
            


if __name__ == '__main__':
    #app = TestApplication(sys.argv)
    QApplication.setDesktopSettingsAware(False)
    app = QApplication(sys.argv)
    set_application_test(app)
    runner = unittest.TextTestRunner()
    runner.run(create_suite())
    exit()
    sys.exit(app.exec_())
    

        

