


RUNNERS = {
        'python3': [],
        }


FILE_WITH_RUNNERS = {
    }

def get_key(key):
    try:
        return RUNNERS[key]
    except KeyError:
        return None

def get_list_runners():
    for key, val in RUNNERS.items():
        yield (key, val)