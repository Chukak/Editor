""" 
НЕ ИСПОЛЬЗУЙТЕ input() В СКРИПТАХ
ВРЕМЕННО
\\\\\\\\\\\\\
DONT USE input() IN SCRIPT
TEMPORARILY
"""
from PyQt5.QtWidgets import QWidget, QLabel, QGridLayout, QPlainTextEdit, QPushButton, QHBoxLayout,\
    QVBoxLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from subprocess import Popen, PIPE, TimeoutExpired


class ConsoleWidget(QWidget):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setParent(parent)
        self.setMinimumHeight(30)
        self.setMaximumHeight(120)
        self.obj = parent
        
        self.header = self.set_header()
        self.text_result = ResultText(self.obj)
        self.return_widget()
        self.turn_toggle = False
        
        layout = QGridLayout(self)
        layout.setSpacing(0)
        layout.addWidget(self.header, 0, 0)
        layout.addWidget(self.text_result, 1, 0)
        layout.addWidget(self.rturn, 2, 0)
        
        self.setLayout(layout)
        self.setContentsMargins(0, 0, 0, 0)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.show()
    
    def return_widget(self, *args):
        self.rturn = QWidget(self)
        self.rturn.setFixedWidth(30)
        self.rturn.setFixedHeight(30)
        
        button = QPushButton(self.rturn)
        button.setFixedWidth(30)
        button.setMaximumHeight(30)
        button.setObjectName('console_rturn')
        button.setStyleSheet('QPushButton#console_rturn {'
                                'image: url(icon/menu/turn_console_open.png);'
                                'height: 30px;'
                                'border:1px solid #C4C4C4; '
                                'line-height:1.9;'
                                'margin-right: 6px;'
                                'margin-top: 1px;'
                                'margin-bottom: 1px;'
                                'border-radius: 2px;'
                                '}'
                            'QPushButton#console_rturn:hover {'
                            'image: url(icon/menu/turn_console_open_hover.png); '
                            'background: #ffffff;'
                            '}')
        button.clicked.connect(self.turn)
        
        layout = QHBoxLayout(self.rturn)
        layout.addWidget(button)
        
        self.rturn.setLayout(layout)
        self.rturn.setContentsMargins(0, 0, 0, 0)
        self.rturn.layout().setContentsMargins(0, 0, 0, 0)
        self.rturn.setVisible(False)
    
    def turn(self):
        if not self.turn_toggle:
            self.header.setVisible(False)
            self.text_result.setVisible(False)
            self.return_widget()
            #self.setFixedWidth(50)
            self.rturn.setVisible(True)
            self.turn_toggle = True
        else:
            self.rturn.setVisible(False)
            self.header.setVisible(True)
            self.text_result.setVisible(True)
            #self.setMaximumWidth(130)
            self.turn_toggle = False
        
    def set_header(self):
        header = QWidget(self)
        header.setFixedHeight(25)
        header.setMaximumWidth(130)
        
        label = QLabel(self.tr('Run script'), header)
        label.setFixedWidth(130)
        label.setObjectName('console')
        label.setAlignment(Qt.AlignLeft)
        label.setStyleSheet('QLabel#console {height:20px;' 
                            'padding-top:2px;'
                            'text-align:center;'
                            'border:1px solid #C4C4C4;' 
                            'border-bottom:none;'
                            'background:#f2f2f2;'
                            'line-height:1.9;'
                            'border-top-left-radius: 7px;'
                            'border-top-right-radius:7px;}')
        
        button = QPushButton(header)
        button.setFixedWidth(28)
        button.setMaximumHeight(18)
        button.setObjectName('console_turn')
        button.setStyleSheet('QPushButton#console_turn {'
                                'image: url(icon/menu/turn_console.png);'
                                'height: 18px;'
                                'border:1px solid #C4C4C4; '
                                'line-height:1.9;'
                                'margin-right: 6px;'
                                'margin-top: 1px;'
                                'margin-bottom: 1px;'
                                'border-radius: 2px;'
                                '}'
                                'QPushButton#console_turn:hover {'
                                'image: url(icon/menu/turn_console_hover.png); '
                                'background: #ffffff;'
                                '}')
        button.clicked.connect(self.turn)
        
        layout = QHBoxLayout(header)
        layout.addWidget(label)
        layout.addWidget(button)
        
        header.setLayout(layout)
        header.setContentsMargins(0, 0, 0, 0)
        header.layout().setContentsMargins(0, 0, 0, 0)
        return header
    
class ResultText(QPlainTextEdit):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setParent(parent)
        self.setReadOnly(True)
        self.setFont(QFont('Monospace', 11))
    
    def print_result(self, text):
        # text - byte string
        self.clear()
        self.insertPlainText(text.decode())
        
class Result(object):        
    def __init__(self, widget, path, file=None, session=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.widget = widget
        widget.code_runner = self
        self.object = widget.object
        self.path = path
        self.file = self.object.file_in_tabs[self.object.tabs.currentIndex()] if file is None else file
    
    def exec_code(self):
        text = self.object.file_in_tabs[self.object.tabs.currentIndex()]
        arguments = [self.path, self.file]
        print(arguments)
        self.process = Popen(arguments, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        try:
            self.process.wait(timeout=24)
        except TimeoutExpired:
            print('error timeout')
            self.object.func.alert.timeout_code_error()
        self.print_result()

    def print_result(self):
        result, error = self.process.communicate()
        text = error if error else result
        self.object.console.print_result(text)
        
       
            
