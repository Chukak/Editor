from PyQt5.QtWidgets import QDialog, QGridLayout, QHBoxLayout, QVBoxLayout, QDialogButtonBox, QPushButton,\
    QTreeWidget, QTreeWidgetItem, QFileDialog, QListWidgetItem, QListWidget, QWidget
from PyQt5.QtCore import Qt

from extraoption.default.run import Result
from extraoption.default.runners import RUNNERS, FILE_WITH_RUNNERS, get_key, get_list_runners

class ListInterpreters(QDialog):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setParent(parent, Qt.Dialog)
        self.obj = parent
        self.create_list_widget()
        self.set_buttons()
        bbox = self.set_button_box()
        
        layout = QGridLayout(self)
        layout.addWidget(self.list, 0, 0)
        layout.addWidget(self.but_widget, 1, 0)
        layout.addWidget(bbox, 1, 1)
        self.setLayout(layout)
    
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        close = bbox.addButton(self.tr('Close'), QDialogButtonBox.NoRole)
        close.clicked.connect(self.close)
        return bbox
    
    def set_buttons(self):
        self.but_widget = QWidget(self)
        
        add = QPushButton(self.tr('Add'), self.but_widget)
        add.clicked.connect(self.file_dialog)
        
        delete = QPushButton(self.tr('Delete'), self.but_widget)
        delete.clicked.connect(self.delete_runner)
        
        layout = QHBoxLayout(self.but_widget)
        layout.addWidget(add)
        layout.addWidget(delete)
        self.but_widget.setLayout(layout)
    
    def create_list_widget(self):
        self.list = QTreeWidget(self)
        self.list.header().setMaximumHeight(0)
        for key, val in get_list_runners():
            item = QTreeWidgetItem(self.list, [key])
            item.env = key
            self.set_widget_item(val, item)
    
    def set_widget_item(self, lst, parent):
        for l in lst:
            item = QTreeWidgetItem(parent, [l])
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            item.path = l
            
    def add_new_runner(self, path):
        temp = self.list.currentItem()
        if self.list.indexOfTopLevelItem(temp) >= 0:
            parent = temp
        else:
            parent = temp.parent()
        item = QTreeWidgetItem(parent, [path])
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        item.path = path
        lst = get_key(parent.env)
        lst.append(path)
        
    def file_dialog(self, *args):
        path, get = QFileDialog.getOpenFileName(self, self.tr('Select'))
        if get:
            self.add_new_runner(path)
    
    def delete_runner(self, *args):
        temp = self.list.currentItem()
        if not self.list.indexOfTopLevelItem(temp) >= 0:
            item = self.list.currentItem()
            parent = item.parent()
            lst = get_key(parent.env)
            lst.remove(item.path)
            parent.takeChildren()
            self.set_widget_item(lst, parent)
        
    
class Interpreters(QDialog):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setParent(parent, Qt.Dialog)
        self.obj = parent
        self.widget = self.obj.tabs.currentWidget()
        self.create_list_widget()
        bbox = self.set_button_box()
        
        layout = QGridLayout(self)
        layout.addWidget(self.list, 0, 0)
        layout.addWidget(bbox, 1, 1)
        self.setLayout(layout)
    
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        self.choice = bbox.addButton(self.tr('Attach to file'), QDialogButtonBox.ApplyRole)
        self.choice.clicked.connect(self.choice_interpreter)
        close = bbox.addButton(self.tr('Close'), QDialogButtonBox.NoRole)
        close.clicked.connect(self.close)
        return bbox
    
    def choice_interpreter(self, *args):
        path = self.list.currentItem().path
        if getattr(self.widget, 'code_runner', False):
            del self.widget.code_runner
        Result(self.widget, path)
        self.obj.func.refresh_code_buttons(self.widget)
        self.close()
        
    def create_list_widget(self):
        self.list = QListWidget(self)
        self.create_items()
        
    def create_items(self, default=None):
        runners = get_key(self.widget.environ) if default is None else default
        if not runners is None:
            print(runners)
            for l in runners:
                item = QListWidgetItem('{0}'.format(l), self.list)
                item.path = l
    
    def closeEvent(self, event):
        self.obj.tabs.setEnabled(True)
        super().closeEvent(event)
