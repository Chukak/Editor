

from PyQt5.QtWidgets import QTableView, QDialog, QVBoxLayout, QWidget, QHeaderView, QDesktopWidget
from PyQt5.QtCore import QAbstractTableModel, Qt, QVariant, QModelIndex
from pandas import DataFrame


class FormatDataFrame(object):
    pass
        
class TableSqlResult(QAbstractTableModel):
    def __init__(self, dataset, *args):
        super().__init__()
        self.data_set = dataset
    
    def rowCount(self, parent=None):
        return len(self.data_set.index)
    
    def columnCount(self, parent=None):
        return len(self.data_set.columns.values)
    
    def data(self, index, role):
        if role == Qt.DisplayRole:
            return QVariant(str(self.data_set.values[index.row(), index.column()]))
        else:
            return QVariant()
    
    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.data_set.columns[col]
        return None
    
    def flags(self, index):
        return Qt.ItemIsEnabled
    
    

class TableDialog(QDialog):
    def __init__(self, parent, arguments):
        super().__init__()
        self.setParent(parent, Qt.Dialog)
        self.parent = parent
        self.arguments = arguments
        self.create_model()
        
        layout = QVBoxLayout(self)
        layout.addWidget(self.view)
        
        self.setLayout(layout)
    
    def create_model(self):
        df = self.get_data_frame()
        model = TableSqlResult(df, self)
        
        self.view = QTableView(self)
        
        #self.view.horizontalHeader().setStretchLastSection(True)
        self.view.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.view.setModel(model)
        
    def get_data_frame(self):
        df = DataFrame(self.arguments[0])
        df.columns = self.arguments[1]
        return df
    
    
        
        
        
    
        
        
        
        
        
        
    
    