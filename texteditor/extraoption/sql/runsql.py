import re
from datetime import datetime


from sqlalchemy import create_engine
from sqlalchemy import exc
from sqlalchemy import inspect

from PyQt5.QtWidgets import QDialog, QLabel, QLineEdit, QDialogButtonBox, \
    QGridLayout, QListWidget, QListWidgetItem, QComboBox, QMessageBox
from PyQt5.QtCore import Qt
from extraoption.sql.table import TableDialog

ALL_CONNECTIONS = []

class LoginSql(QDialog):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setParent(parent, Qt.Dialog)
        self.object = parent
        self.setMaximumWidth(500)
        
        layout = QGridLayout(self)
        self.login_view()
        self.labels_view()
        self.combobox_view()
        
        layout.addWidget(self.error_label,0, 0)
        layout.addWidget(self.error_view_label,0, 1)
        layout.addWidget(self.username_label, 1, 0)
        layout.addWidget(self.line_user, 1, 1)
        layout.addWidget(self.password_label, 2, 0)
        layout.addWidget(self.line_paswd, 2, 1)
        layout.addWidget(self.host_label, 3, 0)
        layout.addWidget(self.line_host, 3, 1)
        layout.addWidget(self.database_label, 4, 0)
        layout.addWidget(self.line_database, 4, 1)
        layout.addWidget(self.combo, 5, 1)
        layout.addWidget(self.db_label, 6, 0)
        layout.addWidget(self.set_button_box(), 6, 1)
        
        self.setLayout(layout)
        self.setWindowTitle(self.tr('Enter the data'))
    
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        self.sign = bbox.addButton(self.tr('Log in'), QDialogButtonBox.ApplyRole)
        close = bbox.addButton(self.tr('Close'), QDialogButtonBox.NoRole)
        self.sign.clicked.connect(self.login)
        close.clicked.connect(self.close)
        return bbox
    
    def login_view(self):
        self.line_user = QLineEdit(self)
        self.line_user.setFixedWidth(200)
        self.line_user.setFixedHeight(25)
        
        self.line_paswd = QLineEdit(self)
        self.line_paswd.setFixedWidth(200)
        self.line_paswd.setFixedHeight(25)
        self.line_paswd.setEchoMode(QLineEdit.Password)
        
        self.line_host = QLineEdit(self)
        self.line_host.setFixedWidth(200)
        self.line_host.setFixedHeight(25)
        self.line_host.setText('localhost')
        
        self.line_database = QLineEdit(self)
        self.line_database.setFixedWidth(200)
        self.line_database.setFixedHeight(25)
        
    def labels_view(self):
        self.username_label = QLabel(self.tr('Name'))
        self.password_label = QLabel(self.tr('Password'))
        self.host_label = QLabel(self.tr('Host'))
        self.database_label = QLabel(self.tr('Database'))
        self.db_label = QLabel(self.tr('DBMS'))
        self.error_label = QLabel(self.tr('Error:'))
        self.error_view_label = QLabel(self.tr('No'))
        
        self.error_view_label.setMinimumWidth(230)
        self.error_view_label.setMinimumHeight(30)
    
    def combobox_view(self):
        self.combo = QComboBox(self)
        self.combo.setFixedWidth(100)
        self.combo.addItem('MySQL')
        #self.combo.addItem('MSSQL')
        #self.combo.addItem('Oracle')
        
    def view_choice_document(self):
        widget = self.object.tabs.currentWidget()
        self.connect = Connect(widget)
        
    def login(self, *args, options=0):
        self.view_choice_document()
        username, password = self.line_user.text(), self.line_paswd.text()
        host, database = self.line_host.text(), self.line_database.text()
        db = self.combo.currentText()
        self.connect.create_data(username, password, db, host=host, database=database)
        check, error = self.connect.open()
        if check:
            ALL_CONNECTIONS.append((username, db,
                                    datetime.now().time().strftime('%H:%M:%S'), self.connect.parent))
            self.object.func.refresh_code_buttons(self.connect.parent)
            self.close()
            if not database:
                DatabasesList(self.object, self.connect)
        else:
            self.line_user.clear()
            self.line_paswd.clear()
            self.error_view_label.setText(error)


class LogoutSql(QDialog):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setParent(parent, Qt.Dialog)
        self.object = parent
        
        self.list_connections = QListWidget(self)
        self.set_items()
        
        layout = QGridLayout(self)
        layout.addWidget(self.list_connections, 0, 0)
        layout.addWidget(self.set_button_box(), 1, 0)
        
        self.setLayout(layout)
        self.setWindowTitle(self.tr('Open Sql connections'))
        
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        self.close_connect = bbox.addButton(self.tr('End connection'), QDialogButtonBox.ApplyRole)
        self.close_connect.clicked.connect(self.close_connection)
        close = bbox.addButton(self.tr('Close'), QDialogButtonBox.NoRole)
        close.clicked.connect(self.close)
        return bbox
       
    def set_items(self):
        for name, db, time_, widget in ALL_CONNECTIONS:
            item = QListWidgetItem('{0} connect in {1} at {2}'.format(name, db, time_), self.list_connections)
            item.name = name
            item.db = db
            item.time = time_
            item.widget = widget
    
    def close_connection(self, *args):
        item = self.list_connections.currentItem()
        if not item is None:
            widget = item.widget
            widget.sql_connect.close()
            delattr(widget, 'sql_connect')
            ALL_CONNECTIONS.remove((item.name, item.db, item.time, item.widget))
            self.object.func.refresh_sql_code_buttons(widget)
            self.list_connections.clear()
            self.set_items()



class DatabasesList(QDialog):
    def __init__(self, parent, connect,  *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setParent(parent, Qt.Dialog)
        self.connect = connect
        self.set_items()
        
        layout = QGridLayout(self)
        layout.addWidget(self.list_databases, 0, 0)
        layout.addWidget(self.set_button_box(), 1, 0)
        self.setWindowTitle(self.tr('Select database'))
        self.show()
        
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        self.sign = bbox.addButton(self.tr('Select'), QDialogButtonBox.ApplyRole)
        self.sign.clicked.connect(self.set_db)
        return bbox
       
    def set_items(self):
        self.list_databases = QListWidget(self)
        items = self.connect.get_databases_list()
        for name in items:
            item = QListWidgetItem(name, self.list_databases)
            item.name = name
    
    def set_db(self, *args):
        item = self.list_databases.currentItem()
        if not item is None:
            self.connect.set_database(item.name)
            self.close()
            #self.connect.exec_sql_code()
            
""" Temporaly. Only Mysql 5"""      
class Connect(object):
    def __init__(self, widget):
        super().__init__()
        widget.sql_connect = self
        self.parent = widget
        self.all_results = []
    
    def create_data(self, username, password, db, host='localhost', database=''):
        self.username = username
        self.password = password
        self.host = host
        self.database_choice = database 
        if db.lower() == 'mysql':
            self.db_name = 'mysql+pymysql'
        else:
            self.db_name = ''
    
    def open(self, check=True):
        create = '{3}://{0}:{1}@{2}/{4}'.format(self.username, self.password, self.host, self.db_name, 
                                                self.database_choice)
        if check:
            try:
                self.engine = create_engine(create, echo=True)
            except exc.ArgumentError as e:
                #print('Error', e, type(e))
                #return False, 'Неверный агрумент\n(БД, имя пользователя, пароль).'
                return False, self.tr('Invalid argument\n(Database, name, password).')
            try:
                self.database = self.engine.connect()
                return True, None
            except exc.OperationalError as e:
                #print('Error', e, type(e))
                #return False, 'Неверное имя пользователя\nили пароль.'
                return False, self.tr('Invalid name or password.')
        else:
            self.engine = create_engine(create, echo=True)
            self.database = self.engine.connect()
            return True, None
    
    def get_databases_list(self):
        databases = inspect(self.engine)
        return databases.get_schema_names()
    
    def set_database(self, name):
        self.database.close()
        self.engine.dispose()
        self.database_choice = name
        self.open(check=False)
    
    def close(self):
        self.database.close()
        self.engine.dispose()
        del self
        
    def exec_sql_code(self):
        codes = self.parent.toPlainText().rstrip()
        codes = re.sub("\/\*.*\n*.*\*\/", '', codes)
        for code in codes.split(';'):
            if code:
                code.replace('\n', '')
                try:
                    result = self.database.execute(code + str(';'))
                    rows = result.fetchmany(size=500)
                    if rows:
                        arguments = (rows, result.keys())
                        t = TableDialog(self.parent, arguments)
                        t.show()
                    result.close()
                except Exception as e:
                    if isinstance(e, exc.ResourceClosedError):
                        #QMessageBox.about(self.parent, 'Выполнено', 
                        #                'Выполнено. По окончании операции запрос вернул пустой набор.\n'
                        #                '(Команды insert, delete, update, set, etc.)')
                        QMessageBox.about(self.parent, self.tr('Complete'), 
                                        self.tr('Complete. Query return empty set.\n'
                                        '(Commands insert, delete, update, set, etc.)'))
                        continue
                    else:
                        return self.show_exception(e)
        
    def show_exception(self, exception):
        if isinstance(exception, exc.ObjectNotExecutableError):
            #QMessageBox.about(self.parent, 'Ошибка', 
            #                        'Код не может быть обработан как SQL запрос.\n'
            #                        '(' + str(exception) + ')')
            QMessageBox.about(self.parent, self.tr('Error'), 
                                    self.tr('This code not a sql query.\n'
                                    '(' + str(exception) + ')'))
        elif isinstance(exception, exc.IntegrityError):
            #QMessageBox.about(self.parent, 'Ошибка', 
            #                        'Обнаружен дублированный ключ(и).')
            QMessageBox.about(self.parent, self.tr('Error'), 
                                    self.tr('Duplicate key(s).'))
        elif isinstance(exception, exc.InternalError):
            #QMessageBox.about(self.parent, 'Ошибка', 
            #                        'Превышено количество идентификаторов или констант '
            #                        'в одном выраджении запроса.'
            #                        '(' + srt(exception) + ')')
            QMessageBox.about(self.parent, self.tr('Error'), 
                                    self.tr('Number of identifiers or constants ' 
                                    'exceeded in one query expression.'
                                    '(' + srt(exception) + ')'))
        elif isinstance(exception, exc.NoForeignKeysError):
            #QMessageBox.about(self.parent, 'Ошибка', 
            #                        'Внешние ключи не могут быть раположены между '
            #                        'двумя запросами.')
            QMessageBox.about(self.parent, self.tr('Error'), 
                                    self.tr('Foreign keys can not be placed between two requests.'))
        elif isinstance(exception, (exc.NoReferenceError, 
                                     exc.NoReferencedColumnError, exc.NoReferencedTableError)):
            #QMessageBox.about(self.parent, 'Ошибка', 
            #                        'Поднятый внешний ключ для указания ссылки не разрешен для '
            #                        'таблицы, столбца или ссылки.')
            QMessageBox.about(self.parent, self.tr('Error'), 
                                    self.tr('An elevated foreign key for referencing is not allowed'
                                    ' for a table, column, or reference.'))
        elif isinstance(exception, exc.NoSuchTableError):
            #QMessageBox.about(self.parent, 'Ошибка', 
            #                        'Таблица не существует или не отображается для подключения.')
            QMessageBox.about(self.parent, self.tr('Error'), 
                                    self.tr('Table doesn\'t exists for connection.'))
        elif isinstance(exception, (exc.StatementError, exc.ProgrammingError)):
            QMessageBox.about(self.parent, self.tr('Error'), 
                                    str(exception))
        else:
            QMessageBox.about(self.parent, self.tr('Udentified error'), 
                                    str(exception))
        
        
        
    
        
        
        
        








