<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Alerts</name>
    <message>
        <location filename="options/alerts.py" line="8"/>
        <source>Permission error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="8"/>
        <source>To create this directory, you must have administrator rights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="12"/>
        <source>File error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="12"/>
        <source>Unacceptable format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="16"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="16"/>
        <source>Some files were not saved.
To save them in the specified directory, you need administrator rights.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="50"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="37"/>
        <source>To use this option, you need at least one open file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="25"/>
        <source>Path doesn&apos;t exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="29"/>
        <source>Project already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="33"/>
        <source>No file for run.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="41"/>
        <source>To use this option, you need connection on database.(Sql connection)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="45"/>
        <source>To use this parameter, you need a file in which the developmentenvironment is sql.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="50"/>
        <source>Script timeout.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Connect</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="221"/>
        <source>Invalid argument
(Database, name, password).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="228"/>
        <source>Invalid name or password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="268"/>
        <source>Complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="268"/>
        <source>Complete. Query return empty set.
(Commands insert, delete, update, set, etc.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="317"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="280"/>
        <source>This code not a sql query.
(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="286"/>
        <source>Duplicate key(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="293"/>
        <source>Number of identifiers or constantsexceeded in one query expression.(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="301"/>
        <source>Foreign keys can not be placed between two requests.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="308"/>
        <source>An elevated foreign key for referencing is not allowed for a table, column, or reference.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="314"/>
        <source>Table doesn&apos;t exists for connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="320"/>
        <source>Udentified error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <location filename="extraoption/default/run.py" line="92"/>
        <source>Run script</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateFileFolder</name>
    <message>
        <location filename="explorer/dialog.py" line="26"/>
        <source>Parent folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="28"/>
        <source>Selection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="30"/>
        <source>Name file/folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="74"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="75"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DatabasesList</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="170"/>
        <source>Select database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="175"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogEnvs</name>
    <message>
        <location filename="envs/dialog.py" line="36"/>
        <source>Selecting the environ development</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="67"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="41"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="65"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="66"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="93"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="97"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="100"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="104"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="108"/>
        <source>Not selected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EFunc</name>
    <message>
        <location filename="options/base.py" line="32"/>
        <source>Create file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/base.py" line="50"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/base.py" line="90"/>
        <source>Save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/base.py" line="127"/>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/base.py" line="139"/>
        <source>Rename folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/base.py" line="139"/>
        <source>Enter the folder name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/base.py" line="160"/>
        <source>Rename file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/base.py" line="160"/>
        <source>Enter the name file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EMenu</name>
    <message>
        <location filename="menu/menu.py" line="17"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="31"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="39"/>
        <source>&amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="45"/>
        <source>&amp;Special functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="53"/>
        <source>&amp;Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="57"/>
        <source>&amp;Environs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="63"/>
        <source>Create file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="68"/>
        <source>&amp;Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="73"/>
        <source>Save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="78"/>
        <source>&amp;Save file as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="83"/>
        <source>&amp;Rename file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="88"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="93"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="98"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="103"/>
        <source>&amp;Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="108"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="113"/>
        <source>Add project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="118"/>
        <source>&amp;Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="123"/>
        <source>&amp;Save all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="124"/>
        <source>Saves all files in all tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="129"/>
        <source>&amp;Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="134"/>
        <source>&amp;Tabulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="139"/>
        <source>&amp;Environ Dev</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="144"/>
        <source>&amp;Save session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="149"/>
        <source>Saved session when exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="154"/>
        <source>&amp;Save interpreters fo file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="159"/>
        <source>Save interpreters for files when exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="164"/>
        <source>&amp;Database connections(sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="169"/>
        <source>&amp;Run sql script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="174"/>
        <source>&amp;All open sql connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="179"/>
        <source>&amp;Attach interpreter to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="184"/>
        <source>&amp;List of interpreters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="189"/>
        <source>&amp;Detach interpreters from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="194"/>
        <source>&amp;Run script</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditorMenu</name>
    <message>
        <location filename="texteditor/editormenu.py" line="22"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="27"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="32"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="37"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="42"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="47"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="52"/>
        <source>&amp;Select all</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Explorer</name>
    <message>
        <location filename="explorer/explorer.py" line="309"/>
        <source>Confirm action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/explorer.py" line="309"/>
        <source>Do you want to move </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExplorerMenu</name>
    <message>
        <location filename="explorer/utils.py" line="23"/>
        <source>Add project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="28"/>
        <source>Delete project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="34"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="39"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="44"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="46"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="49"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="58"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExplorerWidget</name>
    <message>
        <location filename="explorer/explorer.py" line="75"/>
        <source>&lt;img src=&apos;icon/files/project_tree.png&apos; width=&apos;18&apos; height=&apos;20&apos;&gt; Projects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtraFunc</name>
    <message>
        <location filename="options/extra.py" line="138"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/extra.py" line="138"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/extra.py" line="139"/>
        <source>Enter action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/extra.py" line="139"/>
        <source> with this name already exists.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Interpreters</name>
    <message>
        <location filename="extraoption/default/interpreters.py" line="101"/>
        <source>Attach to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="103"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListInterpreters</name>
    <message>
        <location filename="extraoption/default/interpreters.py" line="25"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="32"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="35"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="70"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginSql</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="43"/>
        <source>Enter the data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="47"/>
        <source>Log in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="48"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="73"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="74"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="75"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="76"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="77"/>
        <source>DBMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="78"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="79"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogoutSql</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="129"/>
        <source>Open Sql connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="133"/>
        <source>End connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="135"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="window.py" line="136"/>
        <source>No document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="157"/>
        <source>Do you want to save before goin exit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="158"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="159"/>
        <source>Not save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="160"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="loadscreen/splashscreen.py" line="21"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="loadscreen/splashscreen.py" line="48"/>
        <source>&lt;font color=&quot;black&quot;&gt;Loading Qt components.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="loadscreen/splashscreen.py" line="58"/>
        <source>&lt;font color=&quot;black&quot;&gt;Loading files.&lt;br&gt;The execution time depends on the file size.&lt;br&gt;Current file: &lt;font color=&quot;red&quot; face=&quot;roboto&quot;&gt;&quot;%s&quot;&lt;/font&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabulationDialog</name>
    <message>
        <location filename="options/tabulation.py" line="22"/>
        <source>Select value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/tabulation.py" line="27"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options/tabulation.py" line="28"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopWidget</name>
    <message>
        <location filename="extrawidgets/widgets.py" line="39"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="103"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="110"/>
        <source>Save all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="117"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="124"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="131"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="138"/>
        <source>Repeat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="144"/>
        <source>Run script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="152"/>
        <source>Run sql script</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
