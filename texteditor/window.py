import os, sys, time
from PyQt5.QtWidgets import QApplication, QWidget, QDesktopWidget, QMainWindow,\
    QMessageBox, QGridLayout, QLabel, QStyleFactory, QHBoxLayout, QVBoxLayout
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import Qt, QTranslator, QLocale, QLibraryInfo, QTimer, QRect

import cProfile

from options.base import EFunc
from texteditor.editor import Editor
from tab.tabs import Tabs
from extrawidgets.widgets import TopWidget
from settings import SettingsFile, RunnersFile
from menu.menu import EMenu
from envs.environments import STATUS_ENVIRONMENT
from explorer.explorer import Explorer, ExplorerWidget
from files.icons import Icons
from loadscreen.splashscreen import SplashScreen
from extraoption.default.run import ConsoleWidget

from session import Session



"""MAIN WIN"""        
class MainWindow(QMainWindow): 
    path_file = []
    file_data = {}
    file_in_tabs = {}
    option = {
        'session': 0,
        'currentsession': '',
        'tabulation': 4,
        'saverunners': 0,
        }
    environs = {}
    projects = []
    default_environ = 'none'
    file_run = {}
    
    def __init__(self, app):
        super().__init__()
        self.option['font'] = QFont().defaultFamily()
        self.session = Session(self)
        self.session.get_session()
        self.init_geometry()
        self.setObjectName('main')
        
        self.app = app
        self.status_init = 'go'
        
    def show(self):
        self.init_functions()
        self.app.processEvents()
        
        self.init_window_option()
        
        self.init_widgets_and_layouts()
        
        self.init_session()
        self.app.processEvents()
        
        self.clear_temps_attrs()
        return super().show()
        
    def init_geometry(self):
        rect = self.session.get_display_settings()
        if not rect:
            q = self.frameGeometry()
            q.moveCenter(QDesktopWidget().availableGeometry().center())
            x,y = q.topLeft().x(), q.topLeft().y()
            self.setGeometry(x, y, 900, 700)
        else:
            self.setGeometry(rect)
    
    def init_functions(self):
        self.icons = Icons()
        self.func = EFunc(self)
        self.tabs = Tabs(self)
        self.consoleWidget = ConsoleWidget(self)
        self.console = self.consoleWidget.text_result
        self.explorerWidget = ExplorerWidget(self)
        self.explorer = self.explorerWidget.explorer
        self.menu = EMenu(self)
        
    def init_window_option(self):
        self.setWindowTitle('chukak Notepad')
        self.setWindowIcon(QIcon('icon/main/roflan.png'))
        
        self.setMenuBar(self.menu.create_menu())
    
    def init_widgets_and_layouts(self):
        self.top_widget = TopWidget(self)
        self.main_widget = QWidget(self)
        self.central_widget = QWidget(self)
        self.status_label = QLabel(self)
        
        self.central_layout = QVBoxLayout(self.central_widget)
        self.central_layout.addWidget(self.tabs)
        self.central_layout.addWidget(self.consoleWidget)
        self.central_widget.setLayout(self.central_layout)
        self.central_widget.setContentsMargins(0, 0, 0, 0)
        self.central_widget.layout().setContentsMargins(0, 0, 0, 0)
        
        self.bottom_layout = QHBoxLayout()
        self.bottom_layout.setSpacing(2)
        self.bottom_layout.addWidget(self.explorerWidget)
        self.bottom_layout.addWidget(self.central_widget)
        self.bottom_layout.setContentsMargins(2, 4, 2, 4)
        
        maingrid = QGridLayout(self.main_widget)
        maingrid.setVerticalSpacing(0)
        maingrid.addWidget(self.top_widget, 0, 0)
        maingrid.addLayout(self.bottom_layout, 1, 0)
        self.main_widget.setLayout(maingrid)
        self.main_widget.setContentsMargins(2, 4, 2, 4)
        self.main_widget.layout().setContentsMargins(2, 4, 2, 4)
        
        
        self.setCentralWidget(self.main_widget)
        self.status_bar()
    
    def init_session(self):
        self.sp = splash_screen
        self.tabs.create_tabs()
        self.explorer.add_project(self.projects)
        delattr(self, 'sp')
    
    def status_bar(self):
        self.statusBar().removeWidget(self.status_label)
        if self.tabs.currentIndex() != -1:
            name = self.file_in_tabs[self.tabs.currentIndex()]
            environ = self.environs.get(name, 'none')
            status = STATUS_ENVIRONMENT[environ]
        else:
            status = self.tr('No document')
        self.status_label.setText(status)
        self.status_label.show()
        self.statusBar().addWidget(self.status_label)
        
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_F1:
            self.func.create_file()
        elif event.key() == Qt.Key_F2:
            self.func.open_file()
        elif event.key() == Qt.Key_F3:
            self.func.save_file()
        elif event.key() == Qt.Key_F4:
            self.func.save_all_file()
    
    def closeEvent(self, event):
        if not self.tabs.count():
            self.session.save_session()
            event.accept()
        else:
            mbox = QMessageBox(self)
            mbox.setText(self.tr('Do you want to save before goin exit?'))
            save = mbox.addButton(self.tr('Save'), QMessageBox.ActionRole)
            qiut = mbox.addButton(self.tr('Not save'), QMessageBox.YesRole)
            cancel = mbox.addButton(self.tr('Cancel'), QMessageBox.NoRole)
            mbox.setDefaultButton(save)
            mbox.exec_()
            if mbox.clickedButton() == qiut:
                if self.option['session']:
                    self.session.save_session(save=True)
                else:
                    self.session.save_session()
                event.accept()
            elif mbox.clickedButton() == cancel:
                event.ignore()
            elif mbox.clickedButton() == save:
                if self.option['session']:
                    self.session.save_session(save=True)
                else:
                    self.session.save_session()
                self.func.save_all_file()
                event.accept()
    
    def clear_temps_attrs(self):
        self.file_run.clear()
        self.file_data.clear()
        
    




if __name__ == "__main__":
    #profile = cProfile.Profile()
    #profile.enable()
    st = time.process_time()
    SettingsFile()
    RunnersFile()
    
    QApplication.setDesktopSettingsAware(False)
    app = QApplication(sys.argv)
    
    translator = QTranslator()
    translator.load('ch_ru') #+ QLocale.system().name(), 
    app.installTranslator(translator)
    
    styleSheet = open('styles.qss', 'r').read()
    app.setStyleSheet(styleSheet)
    
    print(QStyleFactory.keys())
    print(QApplication.style().metaObject().className())
    
    window = MainWindow(app)
    
    splash_screen = SplashScreen(app, window)
    splash_screen.set_geometry(window.geometry())
    splash_screen.set_gui()
    splash_screen.show()
    
    time.sleep(0.2)
    
    window.show()
    splash_screen.finish(window)
    print(time.process_time() - st, 'main')
    #profile.disable()
    #profile.print_stats()
    sys.exit(app.exec_())

