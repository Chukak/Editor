import os

from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPainter, QColor



# Start
class RegexForTabulation(object):
    default = []
    default_end = []
    
    def return_env(self):
        return self.default
        
    def python3(self):
        return [
            r'\b(def|if|else|with|class|elif|try|except|finally|for|while)\b[\s\w\_\S]*:',
            ]
        
    def cpp(self):
        return [
            r'(\{)',
            ]
    
    def javascript(self):
        return [
            r'(\{|\(|\[)',
            ]
    def html5(self):
        return [
            r'<\w+\s*[\/]?\s*>',
            ]
    
    def return_env_end(self):
        return self.default_end
        
    def python3_end(self):
        return [
            r'^[^#]*((\'.*#.*\')|(\".*#.*\")|[^#]*)\b(pass|return|yield)\b',
            ]
        
    def cpp_end(self):
        return [
            r'(\})'
            ]
    
    def javascript_end(self):
        return [
            ]
        
    def html5_end(self):
        return [
            ]
    
    @classmethod
    def change_env(cls, env):
        envs = {
            'python3' : cls().python3,
            'cpp' : cls().cpp,
            'javascript': cls().javascript, 
            'html5': cls().html5,
            }
        func = envs.get(env, None)
        if not func is None:
            cls.default = func()
        else:
            cls.default = None
    
    @classmethod
    def change_env_end(cls, env):
        envs = {
            'python3' : cls().python3_end,
            'cpp' : cls().cpp_end,
            'javascript': cls().javascript_end, 
            'html5': cls().html5_end,
            }
        func = envs.get(env, None)
        if not func is None:
            cls.default_end = func()
        else:
            cls.default_end = None
    
        
ENV_FOR_EDITOR = {
        '.py': 'python3',
        '.cpp': 'cpp',
        '.html': 'html5',
        '.js': 'javascript',
        '.css': 'css3',
    }


def get_env_for_editor(file):
    for key in ENV_FOR_EDITOR:
        if file.endswith(key):
            return ENV_FOR_EDITOR[key]
    else:
        return None
    

"""НОМЕРА СТРОК"""
class LineNum(QWidget):
    def __init__(self, document):
        super().__init__()
        self.document = document
        self.document.blockCountChanged.connect(self.line_num_width_update)
        self.document.updateRequest.connect(self.line_num_update)
        self.setParent(self.document)
        self.setObjectName('linenum')
        
    def line_num_width(self):
        digits = 1
        count = max(1, self.document.blockCount())
        while count >= 10:
            count /= 10
            digits += 1
        space = 3 + self.document.fontMetrics().width('9') * digits
        return space
    
    def line_num_width_update(self, _):
        self.document.setViewportMargins(self.line_num_width(), 0, 0, 0)
    
    def line_num_update(self, rect, dy):
        if dy:
            self.scroll(0, dy)
        else:
            self.update(0, rect.y(), self.width(), rect.height())
        if rect.contains(self.document.viewport().rect()):
            self.line_num_width_update(0)
    
    def line_num_paint_event(self, event):
        painter = QPainter(self)
        painter.fillRect(event.rect(), QColor('#ffffff'))
        block = self.document.firstVisibleBlock()
        block_num = block.blockNumber()
        top = self.document.blockBoundingGeometry(block).translated(self.document.contentOffset()).top() #выравние строки с начала документа
        bottom = top + self.document.blockBoundingRect(block).height()
        height = self.document.fontMetrics().height()
        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = str(block_num + 1)
                painter.setPen(QColor('#7F7E7A'))
                painter.drawText(0, top, self.width(), height, 0x0004, number) # 0x0004 = Qt.AlignCenter
            block = block.next()
            top = bottom
            bottom = top + self.document.blockBoundingRect(block).height()
            block_num += 1
    
    def paintEvent(self, event):
        self.line_num_paint_event(event)
        
    def sizeHint(self):
        return QSize(self.line_num_width(), 0)
    

class EditorFuncForMenu:
    def __init__(self, object):
        super().__init__()
        self.tabs = object.tabs
    
    def check_widget(self):
        widget = self.tabs.currentWidget()
        return widget if not widget is None else False
    
    def copy(self):
        widget = self.check_widget()
        return widget.copy() if widget else None
    
    def cut(self):
        widget = self.check_widget()
        return widget.cut() if widget else None
    
    def paste(self):
        widget = self.check_widget()
        return widget.paste() if widget else None
    
    def undo(self):
        widget = self.check_widget()
        return widget.undo() if widget else None
    
    def redo(self):
        widget = self.check_widget()
        return widget.redo() if widget else None




