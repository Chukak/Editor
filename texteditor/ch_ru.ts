<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Alerts</name>
    <message>
        <location filename="options/alerts.py" line="8"/>
        <source>Permission error</source>
        <translation>Ошибка доступа</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="8"/>
        <source>To create this directory, you must have administrator rights</source>
        <translation>Для создания данной директории вам необходимы права арминистратора</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="12"/>
        <source>File error</source>
        <translation>Ошибка файла</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="12"/>
        <source>Unacceptable format.</source>
        <translation>Неизвестный формат</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="16"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="16"/>
        <source>Some files were not saved.
To save them in the specified directory, you need administrator rights.</source>
        <translation>Некоторые файлы не были сохранены. Для их сохранения требуются права администратора</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="50"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="37"/>
        <source>To use this option, you need at least one open file.</source>
        <translation>Для использования данной опции вам неодходим хотя бы один открытый файл</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="25"/>
        <source>Path doesn&apos;t exists.</source>
        <translation>Путь не найден</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="29"/>
        <source>Project already exists.</source>
        <translation>Проект уже существует</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="33"/>
        <source>No file for run.</source>
        <translation>Нет файла для запуска</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="41"/>
        <source>To use this option, you need connection on database.(Sql connection)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="45"/>
        <source>To use this parameter, you need a file in which the development environment is sql.</source>
        <translation>Для использования данной опции вам необходим файл, который имеет среду разработки SQL</translation>
    </message>
    <message>
        <location filename="options/alerts.py" line="50"/>
        <source>Script timeout.</source>
        <translation>Время выполнения вышло</translation>
    </message>
</context>
<context>
    <name>Connect</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="221"/>
        <source>Invalid argument
(Database, name, password).</source>
        <translation>Неправильный аргумент(Database, name, password)</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="228"/>
        <source>Invalid name or password.</source>
        <translation>Неправильное имя или пароль</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="268"/>
        <source>Complete</source>
        <translation>Выполнено</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="268"/>
        <source>Complete. Query return empty set.
(Commands insert, delete, update, set, etc.)</source>
        <translation>Выполнено. Запрос вернул пустой набор.(Команды insert, delete, update, set, etc)</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="317"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="280"/>
        <source>This code not a sql query.
(</source>
        <translation type="unfinished">Этот код не является sql запросом (</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="286"/>
        <source>Duplicate key(s).</source>
        <translation>Дублированный ключ(и)</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="293"/>
        <source>Number of identifiers or constants exceeded in one query expression.(</source>
        <translation type="unfinished">Превышен номер идентификатора или константы в одном запросе</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="301"/>
        <source>Foreign keys can not be placed between two requests.</source>
        <translation>Внешний ключ не может быть расположен меджу двумя запросами</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="308"/>
        <source>An elevated foreign key for referencing is not allowed for a table, column, or reference.</source>
        <translation>Недопустимый внешний ключ для таблицы, столбца, ссылки</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="314"/>
        <source>Table doesn&apos;t exists for connection.</source>
        <translation>Таблица не может быть найдена для соединения</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="320"/>
        <source>Udentified error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <location filename="extraoption/default/run.py" line="92"/>
        <source>Run script</source>
        <translation>Run script</translation>
    </message>
</context>
<context>
    <name>CreateFileFolder</name>
    <message>
        <location filename="explorer/dialog.py" line="26"/>
        <source>Parent folder:</source>
        <translation>Родительская папка:</translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="28"/>
        <source>Selection:</source>
        <translation>Поле выбора:</translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="30"/>
        <source>Name file/folder:</source>
        <translation>Имя файла:</translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="74"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="explorer/dialog.py" line="75"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>DatabasesList</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="170"/>
        <source>Select database</source>
        <translation>Выбрать базу данных</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="175"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
</context>
<context>
    <name>DialogEnvs</name>
    <message>
        <location filename="envs/dialog.py" line="36"/>
        <source>Selecting the environ development</source>
        <translation>Выбрать среду разработки</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="67"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="41"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="65"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="66"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="93"/>
        <source>Color</source>
        <translation>Цвета</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="97"/>
        <source>Bold</source>
        <translation>Bold</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="100"/>
        <source>Italic</source>
        <translation>Italic</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="104"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="envs/dialog.py" line="108"/>
        <source>Not selected</source>
        <translation>Не выбрано</translation>
    </message>
</context>
<context>
    <name>EFunc</name>
    <message>
        <location filename="options/base.py" line="32"/>
        <source>Create file</source>
        <translation>Создать файл</translation>
    </message>
    <message>
        <location filename="options/base.py" line="50"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="options/base.py" line="90"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="options/base.py" line="127"/>
        <source>Open Directory</source>
        <translation>Открыть директорию></translation>
    </message>
    <message>
        <location filename="options/base.py" line="139"/>
        <source>Rename folder</source>
        <translation>Переименовать папку</translation>
    </message>
    <message>
        <location filename="options/base.py" line="139"/>
        <source>Enter the folder name</source>
        <translation>Введите имя папки</translation>
    </message>
    <message>
        <location filename="options/base.py" line="160"/>
        <source>Rename file</source>
        <translation>Переименовать файл</translation>
    </message>
    <message>
        <location filename="options/base.py" line="160"/>
        <source>Enter the name file</source>
        <translation>ВВедите имя файла</translation>
    </message>
</context>
<context>
    <name>EMenu</name>
    <message>
        <location filename="menu/menu.py" line="17"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="31"/>
        <source>&amp;Edit</source>
        <translation>&amp;Изменить</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="39"/>
        <source>&amp;Options</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="45"/>
        <source>&amp;Special functions</source>
        <translation>&amp;Спец. функции</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="53"/>
        <source>&amp;Run</source>
        <translation>&amp;Запуск</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="57"/>
        <source>&amp;Environs</source>
        <translation>&amp;Среда разработки</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="63"/>
        <source>Create file</source>
        <translation>&amp; Создать файл</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="68"/>
        <source>&amp;Open file</source>
        <translation>&amp;Открыть файл</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="73"/>
        <source>Save file</source>
        <translation>&amp;Сохранить файл</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="78"/>
        <source>&amp;Save file as</source>
        <translation>&amp;Сохранить файл как</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="83"/>
        <source>&amp;Rename file</source>
        <translation>&amp;Переименовать файл</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="88"/>
        <source>&amp;Undo</source>
        <translation>&amp;Отменить</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="93"/>
        <source>&amp;Redo</source>
        <translation>&amp;Повторить</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="98"/>
        <source>Copy</source>
        <translation>&amp;Копировать</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="103"/>
        <source>&amp;Cut</source>
        <translation>&amp;Вырезать</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="108"/>
        <source>&amp;Paste</source>
        <translation>&amp;Вставить</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="113"/>
        <source>Add project</source>
        <translation>&amp;Добавить проект</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="118"/>
        <source>&amp;Exit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="123"/>
        <source>&amp;Save all</source>
        <translation>&amp;Сохранить все</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="124"/>
        <source>Saves all files in all tabs</source>
        <translation>Сохранить все файлы во всех вкладках</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="129"/>
        <source>&amp;Font</source>
        <translation>&amp;Шрифт</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="134"/>
        <source>&amp;Tabulation</source>
        <translation>Табуляция</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="139"/>
        <source>&amp;Environ Dev</source>
        <translation>&amp;Среда разработки</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="144"/>
        <source>&amp;Save session</source>
        <translation>&amp;Сохранять сессию</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="149"/>
        <source>Saved session when exit</source>
        <translation>Сохраняет сессию при выходе</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="154"/>
        <source>&amp;Save interpreters fo file</source>
        <translation>&amp;Сохранять интерпретаторы для файлов</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="159"/>
        <source>Save interpreters for files when exit</source>
        <translation>Сохраняет интерпретаторы для файлов</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="164"/>
        <source>&amp;Database connections(sql)</source>
        <translation>&amp;Соединение с базой данных</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="169"/>
        <source>&amp;Run sql script</source>
        <translation>&amp;Запустить sql скрипт</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="174"/>
        <source>&amp;All open sql connections</source>
        <translation>&amp;Открыть все sql соединения</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="179"/>
        <source>&amp;Attach interpreter to file</source>
        <translation>Прикрепить интерпретатор к файлу</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="184"/>
        <source>&amp;List of interpreters</source>
        <translation>Список интерпретаторов</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="189"/>
        <source>&amp;Detach interpreters from file</source>
        <translation>Открепить интерпретатор от файла</translation>
    </message>
    <message>
        <location filename="menu/menu.py" line="194"/>
        <source>&amp;Run script</source>
        <translation>&amp;Запустить скрипт</translation>
    </message>
</context>
<context>
    <name>EditorMenu</name>
    <message>
        <location filename="texteditor/editormenu.py" line="22"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="27"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="32"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="37"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="42"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="47"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="texteditor/editormenu.py" line="52"/>
        <source>&amp;Select all</source>
        <translation>&amp;Выбрать все</translation>
    </message>
</context>
<context>
    <name>Explorer</name>
    <message>
        <location filename="explorer/explorer.py" line="309"/>
        <source>Confirm action</source>
        <translation>Подтвердите действие</translation>
    </message>
    <message>
        <location filename="explorer/explorer.py" line="309"/>
        <source>Do you want to move </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ExplorerMenu</name>
    <message>
        <location filename="explorer/utils.py" line="23"/>
        <source>Add project</source>
        <translation>Добавить проект</translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="28"/>
        <source>Delete project</source>
        <translation>Удалить проект</translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="34"/>
        <source>Refresh</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="39"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="44"/>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="46"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="49"/>
        <source>Folder</source>
        <translation>Папку</translation>
    </message>
    <message>
        <location filename="explorer/utils.py" line="58"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>ExplorerWidget</name>
    <message>
        <location filename="explorer/explorer.py" line="75"/>
        <source>&lt;img src=&apos;icon/files/project_tree.png&apos; width=&apos;18&apos; height=&apos;20&apos;&gt; Projects</source>
        <translation>&lt;img src=&apos;icon/files/project_tree.png&apos; width=&apos;18&apos; height=&apos;20&apos;&gt; Проекты</translation>
    </message>
</context>
<context>
    <name>ExtraFunc</name>
    <message>
        <location filename="options/extra.py" line="138"/>
        <source>Folder</source>
        <translation>Папка</translation>
    </message>
    <message>
        <location filename="options/extra.py" line="138"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="options/extra.py" line="139"/>
        <source>Enter action</source>
        <translation>Подтвердите действие</translation>
    </message>
    <message>
        <location filename="options/extra.py" line="139"/>
        <source> with this name already exists.
Do you want to continue?</source>
        <translation>с таким именем уже существует.Вы хотите продолжить?</translation>
    </message>
</context>
<context>
    <name>Interpreters</name>
    <message>
        <location filename="extraoption/default/interpreters.py" line="101"/>
        <source>Attach to file</source>
        <translation>Прикрепить к файлу</translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="103"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>ListInterpreters</name>
    <message>
        <location filename="extraoption/default/interpreters.py" line="25"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="32"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="35"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="extraoption/default/interpreters.py" line="70"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
</context>
<context>
    <name>LoginSql</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="43"/>
        <source>Enter the data</source>
        <translation>Введите данные</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="47"/>
        <source>Log in</source>
        <translation>Войти</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="48"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="73"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="74"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="75"/>
        <source>Host</source>
        <translation>Имя хоста</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="76"/>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="77"/>
        <source>DBMS</source>
        <translation>DBMS</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="78"/>
        <source>Error:</source>
        <translation>Ошибка:</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="79"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>LogoutSql</name>
    <message>
        <location filename="extraoption/sql/runsql.py" line="129"/>
        <source>Open Sql connections</source>
        <translation>Открыть Sql соединения</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="133"/>
        <source>End connection</source>
        <translation>Закрыть соединение</translation>
    </message>
    <message>
        <location filename="extraoption/sql/runsql.py" line="135"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="window.py" line="136"/>
        <source>No document</source>
        <translation>Нет документа</translation>
    </message>
    <message>
        <location filename="window.py" line="157"/>
        <source>Do you want to save before goin exit?</source>
        <translation>Вы хотите сохранить перед тем как выйти</translation>
    </message>
    <message>
        <location filename="window.py" line="158"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="window.py" line="159"/>
        <source>Not save</source>
        <translation>Не сохранять</translation>
    </message>
    <message>
        <location filename="window.py" line="160"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="loadscreen/splashscreen.py" line="21"/>
        <source>Run</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="loadscreen/splashscreen.py" line="48"/>
        <source>&lt;font color=&quot;black&quot;&gt;Loading Qt components.&lt;/font&gt;</source>
        <translation>&lt;font color=&quot;black&quot;&gt;Загрузка Qt компонентов.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="loadscreen/splashscreen.py" line="58"/>
        <source>&lt;font color=&quot;black&quot;&gt;Loading files.&lt;br&gt;The execution time depends on the file size.&lt;br&gt;Current file: &lt;font color=&quot;red&quot; face=&quot;roboto&quot;&gt;&quot;%s&quot;&lt;/font&gt;&lt;/font&gt;</source>
        <translation>&lt;font color=&quot;black&quot;&gt;Загрузка файлов.&lt;br&gt;Время выполнения зависит от размера файла.&lt;br&gt;Текущий файл: &lt;font color=&quot;red&quot; face=&quot;roboto&quot;&gt;&quot;%s&quot;&lt;/font&gt;&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>TabulationDialog</name>
    <message>
        <location filename="options/tabulation.py" line="22"/>
        <source>Select value</source>
        <translation>Выберите значение</translation>
    </message>
    <message>
        <location filename="options/tabulation.py" line="27"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="options/tabulation.py" line="28"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>TopWidget</name>
    <message>
        <location filename="extrawidgets/widgets.py" line="39"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="103"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="110"/>
        <source>Save all</source>
        <translation>Сохранить все</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="117"/>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="124"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="131"/>
        <source>Undo</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="138"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="144"/>
        <source>Run script</source>
        <translation>Запустить скрипт</translation>
    </message>
    <message>
        <location filename="extrawidgets/widgets.py" line="152"/>
        <source>Run sql script</source>
        <translation>Запустить sql скрипт</translation>
    </message>
</context>
</TS>
