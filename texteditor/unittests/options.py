import os, sys, time, shutil, unittest
from functools import partial
from PyQt5.QtTest import QTest

from PyQt5.QtWidgets import QTabWidget, QWidget,QLineEdit, QMainWindow,\
    QPlainTextEdit
from PyQt5.QtCore import Qt
from options.base import EFunc
from files.icons import Icons
import options


options.base.DEFAULT_PATH = os.getcwd() +  os.path.sep + 'unittests' + os.path.sep + 'TEST'


class EFuncWithoutExtra(EFunc):
    def search_next_prev(self, *args, step):
        pass
            
    def close_tab_with_search(self, index=-1):
        pass
            
    def clear_text_line(self, *args, index=-1):
            pass
            
    def check_indexs(self):
        pass
            
    def reset_indexs(self):
        pass
            
    def perm_alert_error(self):
        pass
            
    def error_from_save_all(self):
        pass
            
    def environ_development(self, *args):
        pass
            
    def set_tabulation_num(self):
        pass
            
    def get_relative_path(self, path):
        pass
            
    def select_created_item_explorer(self, path):
        pass
            
    def select_exiting_item_explorer(self, path, rename=None):
        pass
            
class TestBaseOptions(unittest.TestCase):
    def setUp(self):
        self.set_main()
        
    
    def set_main(self):
        self.main.func = EFuncWithoutExtra(self.main)      
        self.path = os.getcwd() +  os.path.sep + 'unittests' + os.path.sep + 'TEST'
    
    def create_temp_project(self):
        path = self.path
        #pathlib.Path(path).mkdir(parents=True, exist_ok=True)
        if not os.path.exists(path):
            os.makedirs(path)
        return path   
    
    def clear_attrs(self):
        shutil.rmtree(self.path, ignore_errors=True)
        self.main.projects.clear()
        self.main.file_in_tabs.clear()
        self.main.path_file.clear()
    
    #def test_1_create_file(self):
    #    path = self.create_temp_project()
    #    self.application.focusChanged.connect(partial(self.get_focus_dialog, text='file'))
    #    result = self.main.func.create_file()
    #    self.application.disconnect()
    #    file = path + os.path.sep + 'file'
    #    self.assertTrue(os.path.exists(file))
    #    self.assertEqual(result, file)
    #    self.assertEqual(self.main.path_file.count(file), 1)
        
    """def test_2_open_file(self):
        path = self.create_temp_project()
        fname = path + os.path.sep + 'file'
        with open(fname, 'tw', encoding='utf-8') as f:
                    f.seek(0)
                    f.truncate()
        self.main.func.open_file(path=fname)
        self.assertEqual(self.main.path_file.count(fname), 1) """
        
    #def test_2_rename_file(self):
    #    path = self.create_temp_project()
    #    fname = path + os.path.sep + 'file'
    #    with open(fname, 'tw', encoding='utf-8') as f:
    #                f.seek(0)
    #                f.truncate()
    #    self.main.path_file.append(fname)
    #    self.application.focusChanged.connect(partial(self.get_focus_dialog, text='file2'))
    #    self.main.func.rename_file(path=fname)
    #    self.application.disconnect()
    #    new_fname = path + os.path.sep + 'file2'
    #    self.assertTrue(os.path.exists(new_fname))
    #    self.assertNotEqual(fname, new_fname)
        
    def test_3_find_text(self):
        path = self.create_temp_project()
        fname = path + os.path.sep + 'file'
        plain_text = QPlainTextEdit()
        linedit = QLineEdit()
        linedit.setText('def')
        index = self.main.tabs.addTab(plain_text, os.path.basename(fname))
        self.main.tabs.currentWidget().insertPlainText('   def   defef   def ') # 3,5 16,18
        
        self.main.func.find_text(line=linedit)
        for n in self.main.func.search_index:
            self.assertIn(n, [6, 12, 20])
        
    def tearDown(self):
        self.clear_attrs()
        time.sleep(0.5)
    
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        print('Success! All base options test are finished.')
    
    def get_focus_dialog(self, old, new, text):
        if isinstance(new, QLineEdit):
            new.setText(text)
            QTest.mouseClick(new, Qt.LeftButton)
            QTest.keyClick(new, Qt.Key_Enter)
        if isinstance(new, type(None)):
            QTest.keyClick(None, Qt.Key_Enter)
    
    
        
        
        








