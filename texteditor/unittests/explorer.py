import unittest, os, pathlib, time, sys
from explorer.explorer import Explorer
from PyQt5.QtWidgets import QWidget, QLineEdit, QPushButton , QMainWindow
from PyQt5.QtCore import Qt
from files.icons import Icons
from options.base import EFunc
from texteditor.tabs import Tabs 
import shutil
from PyQt5.QtTest import QTest
from _ast import Attribute




class TestExplorer(unittest.TestCase):
    def setUp(self):
        self.set_explorer()
       
    def set_explorer(self):
        self.explorer = self.main.explorer = Explorer(self.main)
        self.explorer.setVisible(False)
        self.path = path = os.getcwd() +  os.path.sep + 'unittests' + os.path.sep + 'TEST'
        
    def create_temp_project(self):
        path = self.path
        #pathlib.Path(path).mkdir(parents=True, exist_ok=True)
        if not os.path.exists(path):
            os.makedirs(path)
        for i in ('1', '2'):
            with open(path + os.path.sep + 'file%s' %i, 'tw', encoding='utf-8') as f:
                            f.seek(0)
                            f.truncate()
                            f.close()
        self.main.projects.append(path)
        self.explorer.add_project(self.main.projects)
        return path
    
    def clear_attrs(self, path=None):
        if path is None:
            shutil.rmtree(self.path, ignore_errors=True)
        else:
            shutil.rmtree(path)
        self.main.projects.clear()
        self.main.file_in_tabs.clear()
        self.main.path_file.clear()
        
    def test_1_add_project(self):
        path = self.create_temp_project()
        top_item = self.explorer.topLevelItem(0)
        self.assertIsNot(top_item, None, msg='Error: return 0 ')
        
        self.assertEqual(top_item.path, path, msg='Error: folder not in tree')
        files = [path + os.path.sep + 'file%s' %i for i in ('1', '2')]
        for child in top_item.takeChildren():
            self.assertIn(child.path, files, msg='Error: file not in tree') 
    
    def test_2_remove_project(self):
        path = self.create_temp_project()
        top_item = self.explorer.topLevelItem(0)
        self.explorer.setCurrentItem(top_item)
        self.explorer.remove_project()
        top_item = self.explorer.topLevelItem(0)
        self.assertIs(top_item, None)
    
    def test_3_refresh_item(self):
        path = self.create_temp_project()
        top_item = self.explorer.topLevelItem(0)
        self.explorer.setCurrentItem(top_item)
        index = self.explorer.refresh_item()
        self.assertEqual(index.row(), 0)
        
        count = self.explorer.currentItem().childCount()
        item = self.explorer.currentItem().child(0)
        self.explorer.refresh_item(current=item)
        self.assertEqual(count, self.explorer.currentItem().childCount())
        
        items = self.explorer.findItems('file2', Qt.MatchContains | Qt.MatchRecursive)
        old_index = self.explorer.indexFromItem(items[0])
        index = self.explorer.refresh_item(current=items[0], new_path=path + os.path.sep + 'new_file')
        self.assertNotEqual(item.path, self.explorer.itemFromIndex(index).path)
        self.assertEqual(old_index.row(), index.row())
        
    #def test_4_rename_item(self):
    #    path = self.create_temp_project()
    #    top_item = self.explorer.topLevelItem(0)
    #    self.explorer.setCurrentItem(top_item)
    #    name = self.explorer.currentItem().text(0)
    #    self.application.focusChanged.connect(self.get_focus_input_dialog)
    #    self.explorer.rename_item()
    #    self.application.disconnect()
    #    new_name = self.explorer.currentItem().text(0)
    #    self.assertNotEqual(name, new_name)
    #    path = self.explorer.currentItem().path
    #    self.clear_attrs(path)
        
    def test_5_create_new_item(self):
        path = self.create_temp_project()
        top_item = self.explorer.topLevelItem(0)
        self.explorer.setCurrentItem(top_item)
        dialog = self.explorer.create_new_item(file=True)
        dialog.setVisible(False)
        dialog.create.setText('file3')
        dialog.create_file_folder()
        find = self.explorer.findItems('file3', Qt.MatchContains | Qt.MatchRecursive | Qt.MatchCaseSensitive)
        if len(find) > 0:
            self.assertEqual(find[0].path, path + os.path.sep + 'file3')
    
    def test_6_delete_item(self):
        path = self.create_temp_project()
        item = self.explorer.findItems('file1', Qt.MatchContains | Qt.MatchRecursive | Qt.MatchCaseSensitive)
        if len(item) > 0:
            self.explorer.setCurrentItem(item[0])
            self.explorer.delete_item()
            item = self.explorer.findItems('file1', Qt.MatchContains | Qt.MatchRecursive | Qt.MatchCaseSensitive)
            self.assertEqual(0, len(item))
    
    #def get_focus_input_dialog(self, old, new):
    #    if isinstance(new, QLineEdit):
    #        new.setText('TEST2')
    #        QTest.mouseClick(new, Qt.LeftButton)
    #        QTest.keyClick(new, Qt.Key_Enter)
    #    if isinstance(new, type(None)):
    #        QTest.keyClick(None, Qt.Key_Enter)
        
    def tearDown(self):
        self.clear_attrs()
        time.sleep(0.5)
        
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        print('Success! All explorer tests are finished.')
        
    
    
    
        
        
