import unittest
import codecs
import time
from envs.syntax.highlights import SyntaxHighlighter
from PyQt5.QtWidgets import QPlainTextEdit
from envs.environments import ENVIRONMENT, STYLES_ENVS, DEFAULT_STYLES_ENVS
from envs.syntax.styles.python import CUSTOM_STYLES




class TestHighlights(unittest.TestCase):
    def setUp(self):
        for key in DEFAULT_STYLES_ENVS.keys():
            if key != 'none':
                dist = STYLES_ENVS[key]
                dist.update(DEFAULT_STYLES_ENVS[key])
                
    def create_text_edit(self, insert, highlight):
        textedit = QPlainTextEdit()
        textedit.highlighter = SyntaxHighlighter(textedit.document(), highlight)
        textedit.insertPlainText(insert) 
        return textedit         
                
    def test_1_highlight_string(self):
        for env, highlight in ENVIRONMENT.items():
            if not highlight is None:
                string = "'  {0} '  {0}  '  \" {0} ".format('"')
                result = [(0, 5), (8, 14), (16, -1)]
                textedit = self.create_text_edit(string, highlight)
                self.assertEqual(result, textedit.highlighter.string, msg='Error: ' + str(env) + ' does not work')
                del textedit
    
    def test_2_highlights_comment(self):
        for env, highlight in ENVIRONMENT.items():
            if not highlight is None:
                textedit = self.create_text_edit('', highlight)
                comment = textedit.highlighter.comment[0]
                string = ' " {0} "  {0} '.format(comment)
                textedit.insertPlainText(string)
                result = 8 + len(comment) -1
                self.assertEqual(result, textedit.highlighter.block_comments, msg='Error: ' + str(env) + ' does not work')
                del textedit
    
    def test_3_highlights_multiline(self):
        def highlights_multiline(textedit, insert, check):
            textedit.insertPlainText(insert)
            result = textedit.highlighter.multi
            for key, val in result.items():
                val_two = check[key]
                self.assertEqual(val, val_two)
            
        for env, highlight in ENVIRONMENT.items():
            if not highlight is None:
                textedit = self.create_text_edit('', highlight)
                multi = textedit.highlighter.multiline
                if isinstance(multi[0], tuple):
                    string =  '{0} \n  {1}'.format(s.replace('\\', ''), e.replace('\\', ''))
                    check = {
                        0: [(0, -1)],
                        1: [(0, 2 + len(e.replace('\\', '')))]
                        }
                    highlights_multiline(textedit, string, check)
                elif isinstance(multi[0], list):
                    for s, e, state in multi[0]:
                        textedit.clear()
                        string = '{0} \n  {1}'.format(s.replace('\\', ''), e.replace('\\', ''))
                        check = {
                            0: [(0, -1)],
                            1: [(0, 2 + len(e.replace('\\', '')))]
                            }
                        highlights_multiline(textedit, string, check)
                del textedit
    
    def test_4_check_str_or_multi_in_multi(self):
        
        def check_str_in_multi(textedit, string, check):
            textedit.insertPlainText(string)
            result = textedit.highlighter.check_str_in_multi(check)
            self.assertEqual(1, result, msg=string + '   ')
        
        def check_multi_in_multi(textedit, string, check):
            textedit.insertPlainText(string)
            result = textedit.highlighter.check_multi_in_multi(check, 0)
            self.assertEqual(1, result, msg=string + '   ')
        
        for env, highlight in ENVIRONMENT.items():
            if not highlight is None:
                textedit = self.create_text_edit('', highlight)
                multi = textedit.highlighter.multiline
                if isinstance(multi[0], tuple):
                    s,e = multi[0]
                    string = '{0}                {1}'.format(s.replace('\\', ''), e.replace('\\', ''))
                    check = 9 + len(s) -1
                    check_str_in_multi(textedit, string, check)
                    check_multi_in_multi(textedit, string, check)
                elif isinstance(multi[0], list):
                    for s, e, state in multi[0]:
                        textedit.clear()
                        string = '{0}                 {1}'.format(s.replace('\\', ''), e.replace('\\', ''))
                        check = 9 + len(s) -1
                        check_str_in_multi(textedit, string, check)
                        check_multi_in_multi(textedit, string, check)
                del textedit
    
    def tearDown(self):
        time.sleep(0.5)
        
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        print('Success! All highlights tests are finished.')
        
        
    



 