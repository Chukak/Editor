import unittest, time

from files.icons import Icons, ALL_ICONS
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
import sys

ENDS_FILES = {
    'python' : ['.py'],
    'python_bytecode' : ['.pyc'],
    'cpp' : ['.cpp'],
    'image' : ['.jpg', '.jpeg', '.png', '.ico',]
    }



class TestFileIcons(unittest.TestCase):
    def setUp(self):
        pass
    
    def test_1_icons_files(self):
        icons = self.main.icons
        for key, val in ALL_ICONS.items():
            try:
                one = getattr(icons, key).pixmap(QSize(32, 32)).toImage()
                two = QIcon(val).pixmap(QSize(32, 32)).toImage()
                self.assertIs(one==two, True, 'image one != image two')
            except AttributeError:
                self.assertRaises(AttributeError, msg='Error: icons has not this attr.')
    
    def test_2_get_icon_file(self):
        icons = self.main.icons
        for key, val in ENDS_FILES.items():
            try:
                one = getattr(icons, key).pixmap(QSize(32, 32)).toImage()
                for end in val:
                    two = icons.get_icon_file('file' + end).pixmap(QSize(32, 32)).toImage()
                    self.assertIs(one==two, True, msg=end)
            except AttributeError:
                self.assertRaises(AttributeError, msg='Error: icons has not this attr.')
    
    def tearDown(self):
        time.sleep(0.5)
    
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        print('Success! All fileicons tests are finished.')
        
        
        
    
    

