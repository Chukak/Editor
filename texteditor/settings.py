from configparser import ConfigParser

class SettingsFile(ConfigParser):
    def __init__(self):
        super().__init__()
        try: 
            data = self.read('settings.ini')
            if not data:
                raise FileNotFoundError
        except FileNotFoundError:
            with open('settings.ini', 'tw', encoding='utf-8') as f:
                self['DEFAULT'] = {
                    'font': 'DejaVu Sans 10',
                    'session': str(0),
                    'currentsession': '',
                    'defaultenv': 'none',
                    'tabulation' : str(4),
                    'saverunners': 0
                    }
                self['FILES'] = {
                    }
                self['PROJECTS'] = {
                    'opens': '',
                    }
                self.write(f)
                
class RunnersFile(ConfigParser):
    def __init__(self):
        super().__init__()
        try:
            data = self.read('runners.ini')
            if not data:
                raise FileNotFoundError
        except FileNotFoundError:
            with open('runners.ini', 'tw', encoding='utf-8') as f:
                self['RUNNERS'] = {}
                self['FILES_WITH_RUNNERS'] = {}
                self.write(f)
                
                