from functools import partial
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMenuBar

from utils.utils import EditorFuncForMenu

"""Menu"""        
class EMenu(QMenuBar): #EditorMenu
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        self.func = self.object.func
        self.extra = EditorFuncForMenu(parent)
        self.setParent(parent)
        
    def create_menu(self):
        file_menu = self.addMenu(self.tr('&File'))
        file_menu.addAction(self.create_file_menu())
        file_menu.addAction(self.open_file_menu())
        file_menu.addAction(self.open_project_menu())
        file_menu.addSeparator()
        file_menu.addAction(self.save_file_menu())
        file_menu.addAction(self.save_file_as_menu())
        file_menu.addSeparator()
        file_menu.addAction(self.save_all_file_menu())
        file_menu.addSeparator()
        file_menu.addAction(self.rename_menu())
        file_menu.addSeparator()
        file_menu.addAction(self.exit_menu())
            
        refactor_menu = self.addMenu(self.tr('&Edit'))
        refactor_menu.addAction(self.undo())
        refactor_menu.addAction(self.redo())
        file_menu.addSeparator()
        refactor_menu.addAction(self.copy_menu())
        refactor_menu.addAction(self.cut_menu())
        refactor_menu.addAction(self.paste_menu())
        
        option_menu = self.addMenu(self.tr('&Options'))
        option_menu.addAction(self.set_font_menu())
        option_menu.addAction(self.set_tabulation_menu())
        option_menu.addAction(self.set_session_save_menu())
        option_menu.addAction(self.set_runners_save_menu())
        
        special_menu = self.addMenu(self.tr('&Special functions'))
        special_menu.addAction(self.sql_connection_menu())
        special_menu.addAction(self.all_sql_connection_menu())
        special_menu.addSeparator()
        special_menu.addAction(self.list_runner_menu())
        special_menu.addAction(self.set_runner_menu())
        special_menu.addAction(self.delete_runner_menu())
        
        run_menu = self.addMenu(self.tr('&Run'))
        run_menu.addAction(self.run_script_menu())
        run_menu.addAction(self.run_sql_code_menu())
        
        env_menu = self.addMenu(self.tr('&Environs'))
        env_menu.addAction(self.environ_list_menu())
        
        return self
    
    def create_file_menu(self):
        create = QAction(QIcon('icon/menu/create.png'), self.tr('Create file'), self)
        create.triggered.connect(self.func.create_file)
        return create
        
    def open_file_menu(self):
        fopen = QAction(self.tr('&Open file'), self)
        fopen.triggered.connect(self.func.open_file)
        return fopen
        
    def save_file_menu(self):
        save = QAction(QIcon('icon/menu/save.png'), self.tr('Save file'), self)
        save.triggered.connect(self.func.save_file)
        return save
        
    def save_file_as_menu(self):
        save_as = QAction(self.tr('&Save file as'), self)
        save_as.triggered.connect(self.func.save_file_as)
        return save_as
        
    def rename_menu(self):
        rename = QAction(self.tr('&Rename file'), self)
        rename.triggered.connect(self.func.rename_file)
        return rename
    
    def undo(self):
        undo = QAction(QIcon('icon/editor/menu/undo.png'), self.tr('&Undo'), self)
        undo.triggered.connect(self.extra.undo)
        return undo
    
    def redo(self):
        redo = QAction(QIcon('icon/editor/menu/redo.png'), self.tr('&Redo'), self)
        redo.triggered.connect(self.extra.redo)
        return redo
    
    def copy_menu(self):
        copy = QAction(QIcon('icon/editor/menu/copy.png'), self.tr('Copy'), self)
        copy.triggered.connect(self.extra.copy)
        return copy
        
    def cut_menu(self):
        cut = QAction(QIcon('icon/editor/menu/cut.png'), self.tr('&Cut'), self)
        cut.triggered.connect(self.extra.cut)
        return cut
        
    def paste_menu(self):
        paste = QAction(QIcon('icon/editor/menu/paste.png'), self.tr('&Paste'), self)
        paste.triggered.connect(self.extra.paste)
        return paste
    
    def open_project_menu(self):
        project = QAction(QIcon('icon/menu/add_project.png'), self.tr('Add project'), self)
        project.triggered.connect(self.func.add_folder_project)
        return project
        
    def exit_menu(self):
        exit = QAction(self.tr('&Exit'), self)
        exit.triggered.connect(self.object.close)
        return exit
    
    def save_all_file_menu(self):
        all = QAction(QIcon('icon/menu/saveall.png'), self.tr('&Save all'), self)
        all.setToolTip(self.tr('Saves all files in all tabs'))
        all.triggered.connect(self.func.save_all_file)
        return all
    
    def set_font_menu(self):
        font = QAction(self.tr('&Font'), self)
        font.triggered.connect(self.func.set_font)
        return font
    
    def set_tabulation_menu(self):
        tabulation = QAction(self.tr('&Tabulation'), self)
        tabulation.triggered.connect(self.func.set_tabulation_num)
        return tabulation
    
    def environ_list_menu(self):
        runwork = QAction(self.tr('&Environ Dev'), self)
        runwork.triggered.connect(self.func.environ_development)
        return runwork
    
    def set_session_save_menu(self):
        session = QAction(self.tr('&Save session'), self, checkable=True)
        if self.object.option['session']:
            session.setChecked(True)
        else:
            session.setChecked(False)
        session.setToolTip(self.tr('Saved session when exit'))
        session.triggered.connect(partial(self.func.set_session_save, action=session))
        return session
    
    def set_runners_save_menu(self):
        runners = QAction(self.tr('&Save interpreters fo file'), self, checkable=True)
        if self.object.option['saverunners']:
            runners.setChecked(True)
        else:
            runners.setChecked(False)
        runners.setToolTip(self.tr('Save interpreters for files when exit'))
        runners.triggered.connect(partial(self.func.set_runners_save, action=runners))
        return runners
    
    def sql_connection_menu(self):
        connect = QAction(self.tr('&Database connections(sql)'), self)
        connect.triggered.connect(self.func.open_sql_login_console)
        return connect
    
    def run_sql_code_menu(self):
        code = QAction(self.tr('&Run sql script'), self)
        code.triggered.connect(self.func.run_sql_code)
        return code
    
    def all_sql_connection_menu(self):
        all = QAction(self.tr('&All open sql connections'), self)
        all.triggered.connect(self.func.open_sql_logout_console)
        return all
    
    def set_runner_menu(self):
        code = QAction(self.tr('&Attach interpreter to file'), self)
        code.triggered.connect(self.func.set_interpreter)
        return code
    
    def list_runner_menu(self):
        lst = QAction(self.tr('&List of interpreters'), self)
        lst.triggered.connect(self.func.show_list_interpreters)
        return lst
    
    def delete_runner_menu(self):
        delete = QAction(self.tr('&Detach interpreters from file'), self)
        delete.triggered.connect(self.func.delete_runner_from_file)
        return delete
    
    def run_script_menu(self):
        run = QAction(self.tr('&Run script'), self)
        run.triggered.connect(self.func.run_code)
        return run

    
    
    
   