from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QLabel, QSlider, QGridLayout


class TabulationDialog(QDialog):
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        self.value = self.object.option['tabulation']
        self.setParent(parent, Qt.Dialog)
        self.setMaximumWidth(130)
        self.set_slider()
        self.set_label()
        bbox = self.set_button_box()
        
        layout = QGridLayout(self)
        layout.addWidget(self.slider, 0, 0)
        layout.addWidget(self.label, 0, 1)
        layout.addWidget(bbox, 1, 0)
        
        self.setLayout(layout)
        self.setWindowTitle(self.tr('Select value'))
        
    
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        self.save = bbox.addButton(self.tr('Save'), QDialogButtonBox.YesRole)
        cancel = bbox.addButton(self.tr('Cancel'), QDialogButtonBox.NoRole)
        self.save.clicked.connect(self.save_value)
        cancel.clicked.connect(self.close)
        return bbox  
    
    def set_slider(self):
        self.slider = QSlider(Qt.Horizontal, self)
        self.slider.setMaximum(32)
        self.slider.setValue(self.value)
        self.slider.valueChanged.connect(self.get_value)
    
    def set_label(self):
        self.label = QLabel(self)
        self.label.setText(str(self.value))
        self.label.setFixedWidth(15)
    
    def get_value(self, num):
        self.label.setText(str(num))
        self.value = num
    
    def save_value(self, *args):
        self.object.option['tabulation'] = self.value
        for n in self.object.file_in_tabs:
            widget = self.object.tabs.widget(n)
            widget.set_tabulation(self.value)
        self.close()
        
