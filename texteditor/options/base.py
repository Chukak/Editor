import os
from PyQt5.QtWidgets import QWidget, QFileDialog, QInputDialog, QFontDialog
from PyQt5.QtGui import QTextCursor, QTextCharFormat, QColor

from pathlib import Path
from options.extra import ExtraFunc
from options.alerts import Alerts 
from options.more import MoreFunc

# LINUX PATHS
DEFAULT_PATH = str(Path.home())
 

"""КЛАСС-ВИДЖЕТ С ФУНКЦИЯМИ РЕДАКТОРА"""
class EFunc(QWidget, ExtraFunc, MoreFunc): # EditorFunction 
    search_index = []
    step = 0
    len_line = 0
    search_toggle = False
    color_found = QColor('#E0E04F')
    color_next_prev = QColor('#FF8A24')
    format_color = QTextCharFormat()
    
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        self.setParent(parent)
        self.icons = self.object.icons
        self.alert = Alerts(self)
        
    def create_file(self, *args):
        fname, get = QFileDialog.getSaveFileName(self.object, self.tr('Create file'), DEFAULT_PATH)
        if fname:
            try:
                with open(fname, 'tw', encoding='utf-8') as f:
                    f.seek(0)
                    f.truncate()
                self.object.path_file.append(fname)
                self.object.tabs.create_new_tab(path=fname)
                if self.object.explorer.topLevelItemCount():
                    self.select_created_item_explorer(fname)
                return fname
            except (PermissionError, UnicodeDecodeError) as e:
                if isinstance(e, PermissionError):
                    self.alert.perm_alert_error()
        return None
            
    def open_file(self, *args, path=None):
        if path is None:
            fname = QFileDialog.getOpenFileName(self.object, self.tr('Open file'), DEFAULT_PATH,
                                                'all files(*)')[0]
        else:
            fname = path
        if not fname in self.object.file_in_tabs.values():
            if os.path.exists(fname):
                try:
                    with open(fname, 'r+', encoding='utf-8') as f:
                        data = f.read()
                    self.object.path_file.append(fname)
                    self.object.tabs.create_new_tab(path=fname, data=data)
                    if not path is None and self.object.explorer.topLevelItemCount():
                        self.select_exiting_item_explorer(fname)
                    
                except (PermissionError, UnicodeDecodeError) as e:
                    if isinstance(e, PermissionError):
                        self.alert.perm_alert_error()
                    elif isinstance(e, UnicodeDecodeError):
                        self.alert.unicode_alert_error()
        else:
            index = tuple(index for index, val in self.object.file_in_tabs.items() if val==fname)
            self.object.tabs.setCurrentIndex(index[0])
            
    def save_file(self, *args):
        if self.object.tabs.count():
            fname = self.object.file_in_tabs[self.object.tabs.currentIndex()]
            if os.path.exists(fname):
                try:
                    with open(fname, 'w', encoding='utf-8') as f:
                        data = self.object.tabs.currentWidget().toPlainText()
                        f.write(data)
                    self.object.tabs.currentWidget().saved_file_or_not(save=True)
                except (PermissionError, UnicodeDecodeError) as e:
                    if isinstance(e, PermissionError):
                        self.alert.perm_alert_error()
                    elif isinstance(e, UnicodeDecodeError):
                        self.alert.unicode_alert_error()
        
    def save_file_as(self, *args):
        if self.object.tabs.count():
            fname, get = QFileDialog.getSaveFileName(self.object, self.tr('Save file'), DEFAULT_PATH,
                                           'all files(*);;text files(*.txt);;pyfiles(*.py)')
            if fname:
                try:
                    widget = self.object.tabs.currentWidget()
                    with open(fname, 'w', encoding='utf-8') as f:
                        data = widget.toPlainText()
                        f.write(data)
                    widget.saved_file_or_not(save=True)
                except (PermissionError, UnicodeDecodeError) as e:
                    if isinstance(e, PermissionError):
                        self.alert.perm_alert_error()
                    elif isinstance(e, UnicodeDecodeError):
                        self.alert.unicode_alert_error()
        
    def save_all_file(self, *args):
        if self.object.tabs.count():
            tabs = self.object.tabs
            errors = 0
            for num, val in self.object.file_in_tabs.items():
                fname = val
                if os.path.exists(fname):
                    try:
                        with open(fname, 'w', encoding='utf-8') as f:
                            data = tabs.widget(num).toPlainText()
                            f.write(data)
                        tabs.setTabText(num, os.path.basename(fname))
                    except (PermissionError, UnicodeDecodeError) as e:
                        errors +=1
                        continue
            if errors:
                self.alert.error_from_save_all()
            
    def add_folder_project(self, *args):
        folder = QFileDialog(self.object)
        folder.setFileMode(QFileDialog.Directory)
        folder.setOptions(QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)
        fname = folder.getExistingDirectory(self.object, self.tr("Open Directory"), DEFAULT_PATH)
        if not fname in self.object.projects:
            if os.path.exists(fname):
                self.object.explorer.add_project([fname])
                self.object.projects.append(fname)
            else:
                self.alert.perm_alert_exists_path()
        else:
            self.alert.project_add_error()
    
    def rename_folder(self, path):
        dir, prefix = os.path.dirname(path), os.path.basename(path)
        text, get = QInputDialog.getText(self.object, self.tr('Rename folder'),
                                         self.tr('Enter the folder name'), text=prefix)
        if os.path.exists(path):
            if get:
                new_path = dir + os.sep + text
                if os.path.isdir(new_path):
                    bool_ = self.check_file_if_rename('dir')
                    if not bool_:
                        return self.rename_folder(path)
                    os.rename(path, new_path)
        else:
            self.alert.perm_alert_exists_path()
        
    def rename_file(self, *args, path=None, intabs=True):
        if path is None and self.object.tabs.count():
            fname = self.object.file_in_tabs[self.object.tabs.currentIndex()]
        else:
            fname = path
        dir, name = os.path.dirname(fname),  os.path.basename(fname)
        #prefix = '.' + name[::-1].split('.')[0][::-1] if '.' in name else ''
        prefix = name
        text, get = QInputDialog.getText(self.object, self.tr('Rename file'),
                                         self.tr('Enter the name file'), text=prefix)
        if os.path.exists(fname):
            if get:
                new_fname = dir + os.sep + text
                if os.path.isfile(new_fname):
                    bool_ = self.check_file_if_rename('file')
                    if not bool_:
                        return self.rename_file(*args, path=path, intabs=intabs)
                print(fname, new_fname)
                os.rename(fname, new_fname)
                i = -1
                if not intabs:
                    for key, val in self.object.file_in_tabs.items():
                        if val == fname:
                            i = key
                            intabs = True
                            break
                else:
                    i = self.object.tabs.currentIndex() 
                if intabs:
                    self.object.file_in_tabs[i] = new_fname
                    basename = os.path.basename(new_fname)
                    self.object.tabs.setTabText(i, basename)
                    self.object.tabs.setTabIcon(i, self.icons.get_icon_file(basename))
                    self.object.setWindowTitle(new_fname)
                    self.object.path_file.remove(fname)
                    self.object.path_file.append(new_fname)
                    self.object.environs[new_fname] = self.object.environs[fname]
                    del self.object.environs[fname]
                    if self.object.explorer.topLevelItemCount():
                        self.select_exiting_item_explorer(fname, rename=new_fname)
                return new_fname
        else:
            self.alert.perm_alert_exists_path()
        
    def find_text(self, line):
        if self.object.tabs.count():
            self.clear_index_position()
            text = line.text()
            if text:
                len_text = self.len_line = len(text)
                widget = self.object.tabs.currentWidget()
                format_ = self.format_color
                format_.clearBackground()
                cursor = widget.textCursor() 
                cursor.movePosition(QTextCursor.Start, QTextCursor.MoveAnchor)
                cursor.movePosition(QTextCursor.End, QTextCursor.KeepAnchor)
                cursor.setCharFormat(format_)
                format_.setBackground(self.color_found)
                index = 0
                data = widget.document()
                while index >= 0:
                    pos = data.find(text, position=index)
                    index = pos.position()
                    if index >= 0:
                        cursor.setPosition(index-len_text, QTextCursor.MoveAnchor)
                        cursor.setPosition(index, QTextCursor.KeepAnchor)
                        cursor.setCharFormat(format_)
                        self.search_index.append(index)
                if self.search_index:
                    format_.setBackground(self.color_next_prev)
                    cursor.setPosition(self.search_index[0] - len_text, QTextCursor.MoveAnchor)
                    cursor.setPosition(self.search_index[0], QTextCursor.KeepAnchor)
                    cursor.setCharFormat(format_)
                    cursor.setPosition(self.search_index[0] - len_text, QTextCursor.MoveAnchor)
                    widget.setTextCursor(cursor)
                    self.object.top_widget.prev_next_button_update(True)
                widget.setReadOnly(False)
                self.search_toggle = True
                
    def set_font(self, *args):
        font, get = QFontDialog.getFont()
        if get:
            self.object.option['font'] = font.toString()
            if self.object.tabs.count():
                for index in range(self.object.tabs.count()):
                    self.object.tabs.widget(index).setFont(font)
    
    def set_session_save(self, action):
        self.object.option['session'] = 1 if action.isChecked() else 0
    
    def set_runners_save(self, action):
        self.object.option['saverunners'] = 1 if action.isChecked() else 0
    
    
        


        