from extraoption.sql.runsql import LoginSql, LogoutSql
from extraoption.default.run import Result
from extraoption.default.interpreters import Interpreters
import os


class MoreFunc(object):
    def open_sql_login_console(self, *args):
        widget = self.object.tabs.currentWidget()
        if not widget is None:
            if widget.check_sql_connection_try():
                login = LoginSql(self.object)
                login.show()
        else:
            self.alert.login_sql_connection_error()
    
    def open_sql_logout_console(self, *args):
        widget = self.object.tabs.currentWidget()
        if not widget is None:
            if widget.check_sql_disconnection_try():
                logout = LogoutSql(self.object)
                logout.show()
        else:
            self.alert.logout_sql_connection_error()
    
    def run_sql_code(self, *args):
        widget = self.object.tabs.currentWidget()
        if not widget is None:
            widget.set_sql_connection()
        else:
            self.alert.run_code_error()
        
    def set_interpreter(self, *args):
        if self.object.tabs.count():
            self.object.tabs.setEnabled(False)
            dialog = Interpreters(self.object)
            dialog.show()
        else:
            self.alert.set_interpreter_error()
    
    def run_code(self, *args):
        widget = self.object.tabs.currentWidget()
        if not widget is None:
            widget.run_code()
        else:
            self.alert.run_code_error()
    
    def delete_runner_from_file(self, *args):
        widget = self.object.tabs.currentWidget()
        if not widget is None:
            print(widget.code_runner.file)
            delattr(widget, 'code_runner')
            self.refresh_code_buttons(widget)
            self.refresh_sql_code_buttons(widget)
        else:
            self.alert.widget_error()
        
    def check_file_with_runner(self, file, widget):
        if file in self.object.file_run:
            print(widget)
            Result(widget, self.object.file_run[file], file=file, session=True)
