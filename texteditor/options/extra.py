from envs.dialog import DialogEnvs 
from options.tabulation import TabulationDialog
#from options.alerts import Alerts as err
from extraoption.default.interpreters import ListInterpreters
import os

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtGui import QTextCursor

class ExtraFunc(object):
    def search_next_prev(self, *args, step):
        self.step += step
        widget = self.object.tabs.currentWidget()
        cursor = widget.textCursor()
        format_= self.format_color
        if self.search_index:
            if self.step < 0:
                index = self.search_index[0]
                self.step = 0
            elif self.step > len(self.search_index) -1:
                index = self.search_index[-1]
                self.step = len(self.search_index) -1
            else:
                index_ = -1
                if step == 1:
                    if self.step -1 > -1:
                        index_ = self.search_index[self.step -1]
                elif step == -1:
                    index_ = self.search_index[self.step +1]
                if index_ > -1:
                    format_.setBackground(self.color_found)
                    cursor.setPosition(index_ - self.len_line, QTextCursor.MoveAnchor)
                    cursor.setPosition(index_, QTextCursor.KeepAnchor)
                    cursor.setCharFormat(format_)
                
                index = self.search_index[self.step]
                format_.setBackground(self.color_next_prev)
                cursor.setPosition(index - self.len_line, QTextCursor.MoveAnchor)
                cursor.setPosition(index, QTextCursor.KeepAnchor)
                cursor.setCharFormat(format_)
                cursor.setPosition(index - self.len_line, QTextCursor.MoveAnchor)
                widget.setTextCursor(cursor) 
    
    def close_tab_with_search(self, index=-1):
        if self.search_toggle:
            self.reset_indexs()
            self.step = self.len_line = 0
            self.object.top_widget.prev_next_button_update(False)
            self.search_toggle = False
                    
    def clear_text_line(self, *args, index=-1):
        if self.search_toggle:
            self.clear_index_position(index=index)
            self.object.top_widget.prev_next_button_update(False)
            #self.object.top_widget.line_text.clear()
            self.search_toggle = False
    
    def refresh_sql_code_buttons(self, widget):
        attr = getattr(widget, 'sql_connect', False)
        if not attr:
            self.object.top_widget.bcodesql.setEnabled(False)
        else:
            self.object.top_widget.bcodesql.setEnabled(True)
    
    def refresh_code_buttons(self, widget):
        attr = getattr(widget, 'code_runner', False)
        if not attr:
            self.object.top_widget.bcode.setEnabled(False)
        else:
            self.object.top_widget.bcode.setEnabled(True)
    
    def clear_index_position(self, index=-1):
        tabs = self.object.tabs
        widget = tabs.widget(index) if index >=0 else tabs.currentWidget()
        format_ = self.format_color
        format_.clearBackground()
        cursor_ = widget.textCursor() 
        for i in self.search_index: 
            cursor_.setPosition(i - self.len_line, QTextCursor.MoveAnchor)
            cursor_.setPosition(i, QTextCursor.KeepAnchor)
            cursor_.setCharFormat(format_)
        self.search_index.clear()
        self.step = self.len_line = 0
        
    def environ_development(self, *args):
        dialog = DialogEnvs(self.object)
        self.object.tabs.setEnabled(False)
        dialog.show()
    
    def set_tabulation_num(self):
        dialog = TabulationDialog(self.object)
        dialog.show()
    
    def get_relative_path(self, path):
        if self.object.explorer.currentItem() is None:
            subpath, result = os.path.realpath(path), None
            for p in self.object.projects:
                    ppath = os.path.realpath(p)
                    if ppath in subpath:
                        result = p
                        break
            if not result is None:
                items = self.object.explorer.findItems(os.path.basename(result), Qt.MatchContains | 
                                                       Qt.MatchCaseSensitive | Qt.MatchRecursive)
                for item in items:
                    if self.object.explorer.indexOfTopLevelItem(item) != -1:
                        self.object.explorer.setCurrentItem(item)
                        break
        
    def select_exiting_item_explorer(self, path, rename=None):
        self.get_relative_path(path)
        self.object.explorer.refresh_item()
        items = self.object.explorer.findItems(os.path.basename(path), Qt.MatchContains 
                                               | Qt.MatchRecursive | Qt.MatchCaseSensitive)
        ppath = os.path.realpath(self.object.explorer.currentItem().path)
        for item in items:
            if ppath in os.path.realpath(item.path):
                index = self.object.explorer.refresh_item(current=item, new_path=rename)
                item = self.object.explorer.itemFromIndex(index)
                self.object.explorer.setCurrentItem(item)
                self.object.explorer.currentItem().setSelected(True)
                break
    
    def select_created_item_explorer(self, path):
        self.get_relative_path(path)
        self.object.explorer.refresh_item()
        items = self.object.explorer.findItems(os.path.basename(path), Qt.MatchContains 
                                                | Qt.MatchRecursive | Qt.MatchCaseSensitive)
        ppath = os.path.realpath(self.object.explorer.currentItem().path)
        for item in items:
            if ppath in os.path.realpath(item.path):
                self.object.explorer.setCurrentItem(item)
                self.object.explorer.currentItem().setSelected(True)
                break
    
    def check_file_if_rename(self, who):
        fix = self.tr('Folder') if who == 'dir' else self.tr('File')
        quest = QMessageBox.question(self, self.tr('Enter action'), fix + self.tr(' with this name already exists.\n'
                                     'Do you want to continue?'))
        return True if quest == QMessageBox.Yes else False
           
    def show_list_interpreters(self):
        dialog = ListInterpreters(self.object)
        dialog.show()
    
    
        
    
    
