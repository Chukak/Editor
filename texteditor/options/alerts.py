from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QObject

class Alerts(QObject):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.obj = parent
    
    def perm_alert_error(self):
        QMessageBox.about(self.obj, self.tr('Permission error'), 
                                    self.tr('To create this directory, you must have administrator rights'))
    
    def unicode_alert_error(self):
        QMessageBox.about(self.obj, self.tr('File error'), 
                                    self.tr('Unacceptable format.'))
    
    def error_from_save_all(self):
        QMessageBox.about(self.obj, self.tr('Warning'), 
                                    self.tr('Some files were not saved.\n' 
                                    'To save them in the specified directory, you need administrator rights.'))
    
    def widget_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('To use this option, you need at least one open file.'))
    
    def exists_path_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('Path doesn\'t exists.'))
    
    def project_add_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('Project already exists.'))
    
    def run_code_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('No file for run.'))
    
    def set_interpreter_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('To use this option, you need at least one open file.'))
    
    def logout_sql_connection_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('To use this option, you need connection on database.(Sql connection)'))
    
    def login_sql_connection_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('To use this parameter, you need a file in which the development ' 
                                            'environment is sql.'))
    
    def timeout_code_error(self):
        QMessageBox.about(self.obj, self.tr('Error'), 
                                    self.tr('Script timeout.'))
    
    
    

