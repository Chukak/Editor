from PyQt5.QtGui import QIcon


FOLDER = 'icon/files/folder.png'
FILE = 'icon/files/file.png'
PROJECT = 'icon/files/project.png'
PYTHON_BYTECODE = 'icon/files/bytecode.png'
PYTHON = 'icon/files/python.png'
CPP = 'icon/files/cpp.png'
IMAGE = 'icon/files/image.png'


ALL_ICONS = {
    'python': PYTHON,
    'py_bytecode': PYTHON_BYTECODE,
    'cpp': CPP,
    'project': PROJECT,
    'image': IMAGE,
    'file': FILE,
    'folder': FOLDER,
    }



ALL_PATH_ICON = [FOLDER, FILE, PROJECT, PYTHON_BYTECODE, PYTHON, CPP, IMAGE]
   
class Icons(object):
    def __init__(self):
        super().__init__()
        self.project = QIcon(PROJECT)
        self.folder = QIcon(FOLDER)
        self.file = QIcon(FILE)
        self.py_bytecode= QIcon(PYTHON_BYTECODE)
        self.python = QIcon(PYTHON)
        self.cpp = QIcon(CPP)
        self.image = QIcon(IMAGE)
        
        self.all_icons = {
            '.py': self.python, 
            '.pyc': self.py_bytecode,
            '.cpp': self.cpp, 
            '.jpg': self.image,
            '.jpeg': self.image,
            '.ico': self.image,
            }
    
    def get_icon_folder(self):
        return self.folder
    
    def get_icon_file(self, file):
        for key, val in self.all_icons.items():
            if file.endswith(key):
                return val
        else:
            return self.file
    
           
            
