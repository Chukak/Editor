from PyQt5.QtWidgets import QScrollBar, QTreeWidget, QTreeWidgetItem,\
    QHeaderView,  QAbstractItemView, QWidget,\
    QLabel, QVBoxLayout, QPushButton, QHBoxLayout, QMessageBox 
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
import os
from _functools import partial

from explorer.dialog import CreateFileFolder
from explorer.utils import ExplorerMenu, EventsOptions

import shutil


class ExplorerWidget(QWidget):
    def __init__(self, parent):
        super().__init__()
        layout = QVBoxLayout(self)
        self.header = self.set_header()
        self.return_widget()
        self.turn_toggle = False
        self.explorer = Explorer(parent)
        self.setMaximumWidth(250)
        #self.setFixedWidth(0)
        layout.setSpacing(0)
        layout.addWidget(self.rturn)
        layout.addWidget(self.header)
        layout.addWidget(self.explorer)
        
        self.setLayout(layout)
        self.setContentsMargins(0, 0, 2, 0)
        self.layout().setContentsMargins(0, 0, 2, 0)
    
    def return_widget(self, *args):
        self.rturn = QWidget(self)
        self.rturn.setFixedWidth(40)
        self.rturn.setFixedHeight(30)
        
        button = QPushButton(self.rturn)
        #button.setIcon(QIcon('icon/menu/turn_project_open.png'))
        button.setFixedWidth(30)
        button.setMaximumHeight(30)
        button.setObjectName('projects_rturn')
        button.clicked.connect(self.turn)
        
        layout = QVBoxLayout(self.rturn)
        layout.addWidget(button)
        
        self.rturn.setLayout(layout)
        self.rturn.setContentsMargins(0, 0, 0, 0)
        self.rturn.layout().setContentsMargins(0, 0, 0, 0)
        self.rturn.setVisible(False)
        
    def turn(self):
        if not self.turn_toggle:
            self.header.setVisible(False)
            self.explorer.setVisible(False)
            self.return_widget()
            self.setFixedWidth(50)
            self.rturn.setVisible(True)
            self.turn_toggle = True
        else:
            self.rturn.setVisible(False)
            self.header.setVisible(True)
            self.explorer.setVisible(True)
            self.setMaximumWidth(250)
            self.turn_toggle = False
            
    def set_header(self):
        header = QWidget(self)
        header.setFixedHeight(25)
        header.setMaximumWidth(220)
        
        label = QLabel(header)
        label.setText(self.tr("<img src='icon/files/project_tree.png' width='18' height='20'> Projects")) #####!
        label.setMaximumHeight(24)
        label.setMinimumHeight(24)
        label.setFixedWidth(220)
        label.setObjectName('projects')
        label.setAlignment(Qt.AlignLeft)
        
        button = QPushButton(header)
        #button.setIcon(QIcon('icon/menu/turn_project.png'))
        button.setFixedWidth(22)
        button.setMaximumHeight(22)
        button.setObjectName('projects_turn')
        button.clicked.connect(self.turn)
        
        layout = QHBoxLayout(header)
        layout.addWidget(label)
        layout.addWidget(button)
        
        header.setLayout(layout)
        header.setContentsMargins(0, 0, 0, 0)
        header.layout().setContentsMargins(0, 0, 0, 0)
        return header
    

class Explorer(QTreeWidget):
    def __init__(self, parent):
        super().__init__()
        self.object = parent
        self.projects = {}
        self.indexes_projects = []
        self.icons = self.object.icons
        self.menu = ExplorerMenu(self)
        self.events_options = EventsOptions(self)
        self.setParent(parent)
        self.setMaximumWidth(220)
        self.set_header()
        self.setHorizontalScrollBar(self.set_horizontal_scrollbar())
        self.setVerticalScrollBar(self.set_vertical_scrollbar())
        self.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.horizontalScrollBar().setSingleStep(1)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.setDragDropMode(3)
        self.customContextMenuRequested.connect(self.menu.showMenu)
        self.itemCollapsed.connect(self.collapsed_item)
        self.itemSelectionChanged.connect(self.change_item)
        self.setObjectName('explorer')
        self.show()
    
    def set_vertical_scrollbar(self):
        scrollbar = QScrollBar(Qt.Vertical, self)
        return scrollbar
    
    def set_horizontal_scrollbar(self):
        scrollbar = QScrollBar(Qt.Horizontal, self)
        return scrollbar
    
    def set_header(self):
        header = QHeaderView(Qt.Horizontal, self)
        header.setStretchLastSection(False)
        header.setSectionResizeMode(QHeaderView.ResizeToContents)
        header.setMinimumSectionSize(220)
        header.setMaximumHeight(0)
        self.setHeader(header)
    
    def add_project(self, paths):
        if isinstance(paths, list):
            for path in paths:
                project_folder = self.create_item(self, path)
                project_folder.setIcon(0, self.icons.project)
                project_folder.path = path
                self.set_widget_items(path, project_folder)
        
    def collapsed_item(self, item):
        self.setCurrentItem(item)
        self.currentItem().setSelected(True)
    
    def change_item(self):
        item = self.currentItem()
        topindex = self.indexOfTopLevelItem(item)
        if topindex >=0:
            self.menu.close.setDisabled(False)
            self.menu.delete_ff.setDisabled(True)
        else:
            self.menu.close.setDisabled(True)
            self.menu.delete_ff.setDisabled(False)
        
    def remove_project(self, *args):
        item = self.currentItem()
        index, top_index = self.indexFromItem(item).row(), self.indexOfTopLevelItem(item)
        if index == top_index:
            if item in self.selectedItems():
                self.object.projects.remove(item.path)
                self.takeTopLevelItem(index)
    
    def set_widget_items(self, path, parent):
        for elem in os.listdir(path):
            try:
                if not elem.startswith('.'):
                    s = os.path.join(path, elem)
                    item = self.create_item(parent, s)
                    if not os.path.isdir(s):
                        item.setIcon(0, self.icons.get_icon_file(elem))
                        continue
                    item.setIcon(0, self.icons.get_icon_folder())
                    self.set_widget_items(s, item)
            except PermissionError:
                continue
    
    def create_item(self, parent, path, index=-1):
        if index >0:
            item = QTreeWidgetItem([os.path.basename(path)])
            parent.insertChild(index, item)
        else:
            item = QTreeWidgetItem(parent, [os.path.basename(path)])
        item.setFlags((Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsDragEnabled |
                       Qt.ItemIsDropEnabled))
        item.path = path
        return item

    def refresh_item(self, *args, current=None, new_path=None):
        if self.object.explorer.topLevelItemCount():
            item = current if not current is None else self.currentItem()
            path = new_path if not new_path is None else item.path 
            item.takeChildren()
            if not item is None:
                if os.path.isdir(path):
                    self.set_widget_items(path, item)
                else:
                    parent = item.parent()
                    index = parent.indexOfChild(item)
                    parent.removeChild(item)
                    item = self.create_item(parent, path, index)
                    item.setIcon(0, self.icons.get_icon_file(os.path.basename(path)))
                return self.indexFromItem(item)
    
    def rename_item(self, *args):
        if self.object.explorer.topLevelItemCount():
            item = self.currentItem()
            index, path = self.indexOfTopLevelItem(item), item.path
            if os.path.isdir(path):
                return self.object.func.rename_folder(path)
            else:
                new_path = self.object.func.rename_file(path=path, intabs=False)
                if not new_path is None:
                    item.path = new_path
                    item.setText(0, os.path.basename(new_path))
                    if index >0:
                        self.object.projects.remove(path)
                        self.object.projects.append(new_path)
        
    def create_new_item(self, *args, file=True):
        if self.object.explorer.topLevelItemCount():
            item = self.currentItem() 
            item = item if os.path.isdir(item.path) else item.parent()
            self.setCurrentItem(item)
            self.currentItem().setSelected(True)
            return CreateFileFolder(self, item.path, file)
        
    def delete_item(self, *args):
        if self.object.explorer.topLevelItemCount():
            item, parent = self.currentItem(), self.currentItem().parent()
            path = item.path
            index = parent.indexOfChild(item) if not parent is None else -1
            sibling = index -1 if index -1 >=0 else -1
            
            for key, val in self.object.file_in_tabs.items():
                if path == val:
                    self.object.tabs.close_tab(key)
                    break
            
            if os.path.exists(path):
                if os.path.isdir(path):
                    shutil.rmtree(path)
                elif os.path.isfile(path):
                    os.remove(path)
                    
            self.refresh_item(current=parent)
            if sibling >=0:
                item = parent.child(sibling)
                self.setCurrentItem(item)
                self.currentItem().setSelected(True)
        
    """EVENTS """
    def mouseDoubleClickEvent(self, event):
        item = self.currentItem()
        if os.path.isdir(item.path):
            if item.isExpanded():
                self.collapseItem(item)
            else:
                self.expandItem(item)
        elif os.path.isfile(item.path):
            self.object.func.open_file(path=item.path)
    
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            #print('sdf')
            pass
        super().mousePressEvent(event)
    
    
    def dragEnterEvent(self, event):
        super().dragEnterEvent(event)
        item = self.currentItem()
        if not item is None:
            path = item.path
            self.events_options.set_old_item(item=item, path=path)  
            print('enter') 
    
    def dragLeaveEvent(self, event):
        super().dragLeaveEvent(event)
        #print('leave')
        #print(self.currentItem().parent().path)
    
    def dragMoveEvent(self, event):
        super().dragMoveEvent(event)
        #print('move')
        #print(self.currentItem())
    
    def dropEvent(self, event):
        super().dropEvent(event)
        item = self.itemAt(event.pos())
        if not item is None:
            new_item, new_path = None, None
            if os.path.isdir(item.path):
                new_item = item
                new_path = item.path
            elif os.path.isfile(item.path):
                new_item = item.parent()
                new_path = new_item.path
            if not new_item is None and not new_path is None:
                old_path, old_item = self.events_options.old_path, self.events_options.old_item
                quest = QMessageBox.question(self, self.tr('Confirm action'), self.tr('Do you want to move ' + 
                                             os.path.basename(old_path) + ' in ' +
                                             new_path))
                if quest == QMessageBox.Yes:
                    if not old_path is None and not new_path is None:
                        if os.path.isfile(old_path): 
                            full_new_path = new_item.path + os.sep + os.path.basename(old_path)
                            for key, val in self.object.file_in_tabs.items():
                                if val == old_path:
                                    self.object.file_in_tabs[key] = full_new_path
                                    basename = os.path.basename(full_new_path)
                                    self.object.tabs.setTabText(key, basename)
                                    self.object.tabs.setTabIcon(key, self.icons.get_icon_file(basename))
                                    
                                    self.object.path_file.remove(old_path)
                                    self.object.path_file.append(full_new_path)
                                    self.object.environs[full_new_path] = self.object.environs[old_path]
                                    del self.object.environs[old_path]
                                    if self.object.tabs.currentIndex() == key:
                                        self.object.setWindowTitle(full_new_path)
                                    break
                        shutil.move(old_path, new_path)
                        
                    if not old_item is None and not new_item is None:
                        self.refresh_item(current=old_item.parent())
                        self.refresh_item(current=new_item)
        print('drop')
        
        
    

    
    
    
        
