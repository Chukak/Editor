from PyQt5.QtWidgets import QMenu, QAction
from PyQt5.QtGui import QIcon
from functools import partial

class ExplorerMenu(QMenu):
    def __init__(self, parent):
        super().__init__()
        self.explorer = parent
        self.create_menu()
    
    def create_menu(self):
        self.addAction(self.add_project_menu())
        self.addAction(self.close_project_menu())
        self.addSeparator()
        self.addMenu(self.create_project_menu())
        self.addAction(self.delete_file_folder())
        self.addSeparator()
        self.addAction(self.refresh_project_menu())
        self.addAction(self.rename_project_menu())
        
        
    def add_project_menu(self):
        add = QAction(QIcon('icon/menu/add_project.png'), self.tr('Add project'), self)
        add.triggered.connect(self.explorer.object.func.add_folder_project)
        return add
    
    def close_project_menu(self):
        self.close = QAction(QIcon('icon/menu/delete_project.png'), self.tr('Delete project'), self)
        self.close.triggered.connect(self.explorer.remove_project)
        self.close.setDisabled(True)
        return self.close
    
    def refresh_project_menu(self):
        refresh = QAction(self.tr('Refresh'), self)
        refresh.triggered.connect(self.explorer.refresh_item)
        return refresh
    
    def rename_project_menu(self):
        rename = QAction(self.tr('Rename'), self)
        rename.triggered.connect(self.explorer.rename_item)
        return rename
    
    def create_project_menu(self):
        self.create = QMenu(self.tr('Create'), self)
        
        file = QAction(self.explorer.icons.file, self.tr('File'), self.create)
        file.triggered.connect(partial(self.explorer.create_new_item, file=True))
        
        folder = QAction(self.explorer.icons.folder, self.tr('Folder'), self.create)
        folder.triggered.connect(partial(self.explorer.create_new_item, file=False))
            
        self.create.addAction(file)
        self.create.addAction(folder)
        #self.create.setDisabled(True)
        return self.create
    
    def delete_file_folder(self):
        self.delete_ff = QAction(QIcon('icon/menu/delete.png'), self.tr('Delete'), self)
        self.delete_ff.triggered.connect(self.explorer.delete_item)
        self.delete_ff.setDisabled(True)
        return self.delete_ff
    
    def showMenu(self, point):
        self.exec(self.explorer.viewport().mapToGlobal(point))
        
    
class EventsOptions(object):
    def __init__(self, obj, *args, **kwargs):
        super().__init__()
        self.obj = obj
        self.old_item = None
        self.old_path = None
        self.new_item = None
        self.new_path = None
    
    def set_old_item(self, item=None, path=None):
        self.old_item = item
        self.old_path = path
    
    def set_new_item(self, item=None, path=None):
        self.new_item = item
        self.new_path = path