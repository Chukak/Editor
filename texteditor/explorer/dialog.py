from PyQt5.QtWidgets import QDialog, QGridLayout, QTreeWidgetItem, QTreeWidget,\
    QLineEdit, QDialogButtonBox, QLabel
from PyQt5.QtCore import Qt

import os
import pathlib



class CreateFileFolder(QDialog):
    def __init__(self, explorer, path, cfile=True):
        super().__init__()
        self.file= cfile
        self.path = path
        self.explorer = explorer
        self.icons = self.explorer.icons
        self.setParent(explorer, Qt.Dialog)
        layout = QGridLayout(self)
        self.set_line_path()
        self.set_tree_widget()
        self.set_line_creating()
        bbox = self.set_button_box()
        
        self.setMinimumWidth(300)
        self.setMinimumHeight(300)
        layout.addWidget(QLabel(self.tr('Parent folder:'), self), 0, 0)
        layout.addWidget(self.line_path, 1, 0)
        layout.addWidget(QLabel(self.tr('Selection:'), self), 2, 0)
        layout.addWidget(self.tree, 3, 0)
        layout.addWidget(QLabel(self.tr('Name file/folder:'), self), 4, 0)
        layout.addWidget(self.create, 5, 0)
        layout.addWidget(bbox, 6, 0)
        self.setLayout(layout)
        self.show()
        
    def set_line_path(self):
        self.line_path = QLineEdit(self)
        self.line_path.setText(self.path)
        self.line_path.setReadOnly(True)
    
    def set_text_line_path(self):
        item = self.tree.currentItem()
        self.line_path.setText(item.path)   
            
    def set_tree_widget(self):
        def set_widget_items(path, parent):
            for elem in os.listdir(path):
                try:
                    if not elem.startswith('.'):
                        s = os.path.join(path, elem)
                        if os.path.isdir(s):
                            item = QTreeWidgetItem(parent, [os.path.basename(s)])
                            item.setFlags((Qt.ItemIsSelectable | Qt.ItemIsEnabled))
                            item.path = s
                            item.setIcon(0, self.icons.get_icon_folder())
                            set_widget_items(s, item)
                except PermissionError:
                    continue
            return
                
        self.tree = QTreeWidget(self)
        self.tree.header().setMaximumHeight(0)
        self.tree.itemSelectionChanged.connect(self.set_text_line_path)
        folder = QTreeWidgetItem(self.tree, [os.path.basename(self.path)])
        folder.setIcon(0, self.icons.project)
        folder.path = self.path
        return set_widget_items(self.path, folder)
    
    def set_line_creating(self):
        self.create = QLineEdit(self)
        
    def set_button_box(self):
        bbox = QDialogButtonBox(self)
        self.save = bbox.addButton(self.tr('Save'), QDialogButtonBox.YesRole)
        cancel = bbox.addButton(self.tr('Cancel'), QDialogButtonBox.NoRole)
        self.save.clicked.connect(self.create_file_folder)
        cancel.clicked.connect(self.close)
        return bbox    
        
    def create_file_folder(self):    
        if os.path.exists(self.line_path.text()):
            path = self.line_path.text() + os.path.sep + self.create.text()
            if self.file:
                try:
                    with open(path, 'tw', encoding='utf-8') as f:
                        f.seek(0)
                        f.truncate()
                    self.explorer.object.path_file.append(path)
                    self.explorer.object.tabs.create_new_tab(path)
                except PermissionError:
                    self.perm_alert_error()
                
            else:
                if not os.path.exists(path):
                    #pathlib.Path(path).mkdir(parents=True, exist_ok=True)
                    os.makedirs(path)
            self.explorer.object.func.select_created_item_explorer(path)
        self.close()
        
        