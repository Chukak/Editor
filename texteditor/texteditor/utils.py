from PyQt5.QtCore import Qt
import re

from PyQt5.QtGui import QTextCursor


class KeyEvents(object):
    def __init__(self, obj, *args, **kwargs):
        super().__init__()
        self.obj = obj
        self.cursor = None
        self.env_for_enter_default = ('python3', 'javascript', 'cpp')
        self.env_for_enter_html = ('html5')
    
    def get_cursor(self):
        return self.obj.textCursor()
    
    def get_point(self, event):
        if self.obj.environ in self.env_for_enter_default:
            return self.key_enter(event)
        elif self.obj.environ in self.env_for_enter_html:
            return self.key_enter_for_html(event)
    
    def search_word_end(self, text):
        for pattern in self.obj.word_tab_end:
            match_end = re.search(pattern, text)
            if not match_end is None:
                return True
        return False
    
    def search_word_start(self, text):
        if not self.obj.word_tab_start is None:
            for pattern in self.obj.word_tab_start:
                match = re.search(pattern, text)
                if not match is None:
                    return True
        return False
        
    def key_enter(self, event):
        cursor = self.get_cursor()
        textblock = cursor.block()
        findall = re.findall('\t', textblock.previous().text())
        times = len(findall) if not findall is None else 0
        if not self.obj.word_tab_end is None:
            if self.search_word_end(textblock.previous().text()):
                return times -1 if times > 0 else 0
            else:
                if self.search_word_start(textblock.previous().text()):
                    return times +1
                else:
                    return times if times > 0 else 0
                        
    def key_enter_for_html(self, event):
        pass               
    
    def key_tab(self, event):
        cursor = self.get_cursor()
        if cursor.hasSelection():
            text = cursor.selection().toPlainText()
            result = '\n'.join([ '\t' + x for x in text.split('\n')])
            cursor.insertText(result)
            return True
        return False
                   


class AutocompleteChars(object):
    def __init__(self, obj, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.obj = obj
        self.braces = {'(' : ')', '{' : '}', '[' : ']',}
        self.strings = {'"' : '"', "'" : "'"}
    
    def get_cursor(self):
        return self.obj.textCursor()
    
    def get_text(self, pos, num):
        cursor = self.get_cursor()
        cursor.setPosition(pos, QTextCursor.MoveAnchor)
        cursor.setPosition(pos + num, QTextCursor.KeepAnchor)
        return cursor, cursor.selectedText()
    
    def check_removed(self, id, pos, removed):
        cursor, text = self.get_text(pos, removed +1)
        checks = ['""', "''"] if id else ['()', '{}', '[]'] # braces - 0 ; strings - 1
        if text in checks:
            cursor.removeSelectedText()
    
    def check_added(self, pos, num):
        cursor = self.get_cursor()
        cursor.setPosition(pos, QTextCursor.MoveAnchor)
        cursor.setPosition(pos + num, QTextCursor.KeepAnchor)
        default = cursor.selectedText()
        cursor.setPosition(pos + num, QTextCursor.MoveAnchor)
        cursor.setPosition(pos + num + 1, QTextCursor.KeepAnchor)
        text = cursor.selectedText()
        if default == text:
            if text in self.braces.values() or text in self.strings.values():
                return False
        return True
    
    def insert_symbol(self, pos, removed, added):
        if pos < self.obj.document().characterCount() -1:
            if added == 1:
                bool_ = self.added(pos, added)
                return True if bool_ else False
            elif removed == 1:
                bool_ = self.removed(pos, removed)
                return True if bool_ else False  
    
    def added(self, pos, added):
        cursor, text = self.get_text(pos, added)
        if self.check_added(pos, added):
            if text in self.braces:
                self.obj.insertPlainText(self.braces.get(text))
                cursor.setPosition(pos + added, QTextCursor.MoveAnchor)
                self.obj.setTextCursor(cursor)
            elif text in self.strings:
                strings = self.obj.highlights.string
                pos_in_block = cursor.positionInBlock() - 1 # 1-inf
                in_string = False
                for s, e in strings:
                    if (e == -1 and pos_in_block > s) or (s < pos_in_block <= e +1):
                        in_string = True
                if not in_string:
                    self.obj.insertPlainText(self.strings.get(text))
                    cursor.setPosition(pos + added, QTextCursor.MoveAnchor)
                    self.obj.setTextCursor(cursor)
        else:
            cursor.removeSelectedText()
            cursor.movePosition(QTextCursor.NextCharacter)
            self.obj.setTextCursor(cursor)
        return True
    
    def removed(self, pos, removed):
        self.obj.undo()
        cursor, text = self.get_text(pos, removed)
        if text in self.braces:
            self.check_removed(0, pos, removed)
        elif text in self.strings:
            self.check_removed(1, pos, removed)
        self.obj.redo() 
        return True
    