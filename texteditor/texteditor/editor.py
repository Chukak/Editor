import time, re, os
from PyQt5.QtWidgets import QPlainTextEdit, QScrollBar 
from PyQt5.QtCore import QRect, Qt, QEvent, QCoreApplication
from PyQt5.QtTest import QTest
from utils.utils import LineNum, RegexForTabulation
from texteditor.editormenu import EditorMenu
from texteditor.utils import KeyEvents, AutocompleteChars
from envs.syntax.highlights import SyntaxHighlighter
from envs.environments import ENVIRONMENT
from PyQt5.QtGui import QFont, QPalette, QTextCursor, QFontMetrics, QKeyEvent

"""EDITOR"""
class Editor(QPlainTextEdit):
    def __init__(self, parent, font, insert, env='none'):
        start = time.process_time()
        super().__init__()
        self.setParent(parent)
        self.object = parent.object
        self.setVerticalScrollBar(self.set_vertical_scrollbar())
        self.setHorizontalScrollBar(self.set_horizontal_scrollbar())
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.set_palette()
        self.set_font(font)
        self.set_tabulation(self.object.option['tabulation'])
        self.setTabChangesFocus(False)
        self.setMouseTracking(True)
        self.insertPlainText(insert)
        self.setReadOnly(False)
        self.setLineWrapMode(QPlainTextEdit.NoWrap)
        self.setObjectName('editor')
        self.line_area = LineNum(self)
        self.menu = EditorMenu(self)
        self.event = KeyEvents(self)
        self.autocomplete = AutocompleteChars(self)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.menu.showMenu)
        self.document().contentsChange.connect(self.insert_symbol)
        self.set_highlighter(env)
        self.set_keywords_tab(env)
        self.save_toggle = True
        print(time.process_time() - start)
        
    def set_font(self, font):
        t_ = QFont()
        t_.fromString(font)
        self.setFont(t_)
    
    def set_tabulation(self, num):
        metrics = QFontMetrics(self.font())
        self.setTabStopWidth(num * metrics.width(' '))  
        
    def set_vertical_scrollbar(self):
        scrollbar = QScrollBar(self)
        scrollbar.setMinimumHeight(10)
        return scrollbar
    
    def set_horizontal_scrollbar(self):
        scrollbar = QScrollBar(self)
        scrollbar.setMinimumWidth(10)
        return scrollbar
    
    def set_palette(self):
        p = QPalette()
        self.setPalette(p)
        
    """переопределенный метод для номеров строк""" 
    def resizeEvent(self, event):
        super().resizeEvent(event)
        cr = self.contentsRect()
        self.line_area.setGeometry(QRect(cr.left(), cr.top(),
                    self.line_area.line_num_width(), cr.height()))  
        
    def insert_symbol(self, pos, removed, added):
        complete = self.autocomplete.insert_symbol(pos, removed, added)
        self.saved_file_or_not()
            
    def set_highlighter(self, key):
        env = ENVIRONMENT[key]
        self.environ = str(key)
        if env is None:
            self.processing = 'none'
            self.document().blockSignals(True)
            self.highlights = SyntaxHighlighter(self.document(), None)
        else:
            processing = env.get_highlight_processing()
            self.processing = processing
            self.document().blockSignals(True)
            self.highlights = SyntaxHighlighter(self.document(), env, processing)
            QCoreApplication.processEvents()
            self.document().blockSignals(False)
            
    def set_keywords_tab(self, env):
        RegexForTabulation.change_env(env)
        self.word_tab_start = RegexForTabulation().return_env()
        RegexForTabulation.change_env_end(env)
        self.word_tab_end = RegexForTabulation().return_env_end()
        
    def check_sql_connection_try(self):
        return True if self.processing == 'sql' else False
    
    def check_sql_disconnection_try(self):
        if self.processing == 'sql':
            return True if getattr(self, 'sql_connect', False) else False
    
    def set_sql_connection(self):
        if self.processing == 'sql':
            if getattr(self, 'sql_connect', False):
                self.sql_connect.exec_sql_code()
    
    def run_code(self):
        if not self.processing in ('sql', 'none'):
            if getattr(self, 'code_runner', False):
                self.code_runner.exec_code()
    
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Enter or event.key() == Qt.Key_Return:
            super().keyPressEvent(event)
            times = self.event.get_point(event)
            if not times is None:
                for _ in range(times):
                    new_event = QKeyEvent(QEvent.KeyPress, Qt.Key_Tab, Qt.KeypadModifier)
                    QTest.keyClick(self, Qt.Key_Tab)
        
        elif event.key() == Qt.Key_Tab:
            tab = self.event.key_tab(event)
            if not tab:
                super().keyPressEvent(event)
        else:
            super().keyPressEvent(event)
           
    def saved_file_or_not(self, save=False):
        index = self.object.tabs.currentIndex()
        if not save:
            name = '*' + os.path.basename(self.object.file_in_tabs[index])
        else:
            name = os.path.basename(self.object.file_in_tabs[index])
        self.object.tabs.setTabText(index, name)
    





