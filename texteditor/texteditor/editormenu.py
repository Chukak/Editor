from PyQt5.QtWidgets import QMenu, QAction
from PyQt5.QtGui import QIcon

class EditorMenu(QMenu):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.create_menu()   
        
    def create_menu(self):
        self.addAction(self.undo())
        self.addAction(self.redo())
        self.addSeparator() 
        self.addAction(self.copy()) 
        self.addAction(self.cut()) 
        self.addAction(self.paste())
        self.addAction(self.delete())
        self.addSeparator() 
        self.addAction(self.select_all())    
        
    def undo(self, *args):
        undo = QAction(QIcon('icon/editor/menu/undo.png'), self.tr('Undo'), self)
        undo.triggered.connect(self.parent.undo)
        return undo 
    
    def redo(self, *args):
        redo = QAction(QIcon('icon/editor/menu/redo.png'), self.tr('Redo'), self)
        redo.triggered.connect(self.parent.redo)
        return redo 
    
    def copy(self, *args):
        copy = QAction(QIcon('icon/editor/menu/copy.png'), self.tr('Copy'), self)
        copy.triggered.connect(self.parent.copy)
        return copy
    
    def cut(self, *args):
        cut = QAction(QIcon('icon/editor/menu/cut.png'), self.tr('Cut'), self)
        cut.triggered.connect(self.parent.cut)
        return cut 
    
    def paste(self, *args):
        paste = QAction(QIcon('icon/editor/menu/paste.png'), self.tr('Paste'), self)
        paste.triggered.connect(self.parent.paste)
        return paste 
    
    def delete(self, *args):
        delete = QAction(QIcon('icon/editor/menu/delete.png'), self.tr('Delete'), self)
        delete.triggered.connect(self.parent.clear)
        return delete  
    
    def select_all(self, *args):
        select_all = QAction(self.tr('&Select all'), self)
        select_all.triggered.connect(self.parent.selectAll)
        return select_all     
    
    def showMenu(self, point):
        self.exec(self.parent.viewport().mapToGlobal(point))
